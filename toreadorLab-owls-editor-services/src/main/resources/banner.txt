
  ${AnsiColor.GREEN}████████╗${AnsiColor.RED}  ██████ ║ ███████╗  ████████╗   ██████    ███████╗ ║  ██████  ║ ███████╗
  ${AnsiColor.GREEN}╚══██╔══╝${AnsiColor.RED} ██║   ██║ ██╔═══██  ██╔═════╝  █║     █║  ██    ██ ║ ██║   ██ ║ ██╔═══██ 
  ${AnsiColor.GREEN}   ██║   ${AnsiColor.RED} ██║   ██║ ███████╔  ██████╗    ████████║  ██    ██ ║ ██║   ██ ║ ███████╔
  ${AnsiColor.GREEN}   ██║   ${AnsiColor.RED} ██║   ██║ ██╔══██║  ██╔═══╝    █║===══█║  ██    ██ ║ ██║   ██ ║ ██╔══██║
  ${AnsiColor.GREEN}   ██║   ${AnsiColor.RED}  ██████ ║ ██║  ╚██  ████████╗  █║     █║  ███████╗ ║  ██████  ║ ██║  ╚██
  ${AnsiColor.GREEN}   ╚═╝   ${AnsiColor.RED} ╚══════╝ ╚═══════╝ ╚════════╝ ╚════════╝ ╚════════╝ ╚════════╝ ╚════════╝

${AnsiColor.BRIGHT_BLUE}:: Toreador Annotation Services 🤓  :: Running Spring Boot ${spring-boot.version} ::
:: http://www.toreador-project.eu/ ::${AnsiColor.DEFAULT}
