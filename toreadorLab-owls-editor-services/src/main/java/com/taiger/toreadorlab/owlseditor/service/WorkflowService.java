package com.taiger.toreadorlab.owlseditor.service;

import static org.mindswap.owls.vocabulary.OWLS_1_2.Process.ControlConstructBag;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.tools.ant.filters.StringInputStream;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.model.vocabulary.XMLSchema;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.GraphQueryResult;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFHandler;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.eclipse.rdf4j.rio.rdfxml.util.RDFXMLPrettyWriterFactory;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.elasticsearch.common.base.Strings;
import org.mindswap.owl.OWLFactory;
import org.mindswap.owl.OWLKnowledgeBase;
import org.mindswap.owl.OWLOntology;
import org.mindswap.owls.expression.Condition;
import org.mindswap.owls.grounding.Grounding;
import org.mindswap.owls.process.Choice;
import org.mindswap.owls.process.CompositeProcess;
import org.mindswap.owls.process.ControlConstruct;
import org.mindswap.owls.process.IfThenElse;
import org.mindswap.owls.process.Perform;
import org.mindswap.owls.process.Process;
import org.mindswap.owls.process.RepeatWhile;
import org.mindswap.owls.process.Sequence;
import org.mindswap.owls.process.Split;
import org.mindswap.owls.process.SplitJoin;
import org.mindswap.owls.profile.Profile;
import org.mindswap.utils.URIUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.taiger.toreadorlab.owlseditor.domain.workflow.ToreadorConstantStrings;
import com.taiger.toreadorlab.owlseditor.domain.workflow.ToreadorEdge;
import com.taiger.toreadorlab.owlseditor.domain.workflow.ToreadorNode;
import com.taiger.toreadorlab.owlseditor.domain.workflow.ToreadorReferenceModeEnum;
import com.taiger.toreadorlab.owlseditor.domain.workflow.ToreadorWorkflow;
import com.taiger.toreadorlab.owlseditor.web.rest.errors.ToreadorOntoEditorException;

import it.eng.toreador.catalogue.ApplicationProperty;
import it.eng.toreador.catalogue.Property;
import it.eng.toreador.catalogue.StaticProperty;

@Service
public class WorkflowService {

	private List<ToreadorNode> alreadyProcessed = new ArrayList<>();

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	// Sesame
	private ValueFactory valueFactory = SimpleValueFactory.getInstance();
	private RepositoryConnection repositoryConnection;
	private Repository repository;
	private ModelBuilder rdfGraphModelBuilder;
	private IRI groundingIRI;
	private String processType;

	@Autowired
	private OntologyService ontologyService;

	@Value("${iconverse.ontology.baseURI}")
	private String baseURI;

	private OWLKnowledgeBase kb;
	private OWLOntology ont;

	private org.mindswap.owls.service.Service service;
	private CompositeProcess process;
	private Profile profile;
	private Grounding grounding;

	private Sequence sequence = null;
	private CompositeProcess compositeProcess = null;

	@Value("${iconverse.resources}")
	private String resourcesPath;

	@Value("${iconverse.ontology.mode}")
	private String ontologyMode;

	@Value("${iconverse.ontology.baseUriOnly}")
	private boolean baseUriOnly;

	@Value("${iconverse.ontology.sparqlendpoint.select}")
	private String sparqlSelectEndpoint;

	@Value("${iconverse.ontology.sparqlendpoint.update}")
	private String sparqlUpdateEndpoint;

	@Value("${iconverse.ontology.servicesURI}")
	private String servicesBaseURI;

	@Value("${iconverse.ontology.serviceReferenceRequestTemplate}")
	private String serviceReferenceRequestTemplate;

	@Value("${iconverse.ontology.serviceReferenceRequestQuery}")
	private String serviceReferenceRequestQuery;

	// @Value("${iconverse.ontology.referenceAsGetRequest}")
	private ToreadorReferenceModeEnum referenceAsGetRequest;

	private Boolean sequenceFirst = true;

	@PostConstruct
	public void init() throws UnknownHostException {
		// init Sesame repository
		try {
			if ("memory".equals(ontologyMode)) {
				initMemoryRepository();
			} else if ("sparql".equals(ontologyMode)) {
				initSparqlRepository();
			}
		} catch (RepositoryException | RDFParseException | IOException e) {
			log.error("Error initializing ontology repository", e);
		}
	}

	private void initMemoryRepository() throws RepositoryException, RDFParseException, IOException {
		repository = new SailRepository(new MemoryStore());
		repository.initialize();
		repositoryConnection = repository.getConnection();
		// load ontology
		// File ontologyFile = new File(resourcesPath + ontologyPath);
		// repositoryConnection.add(ontologyFile, baseURI, RDFFormat.TURTLE);
		// repositoryConnection.commit();
	}

	private void initSparqlRepository() throws RepositoryException {
		repository = new SPARQLRepository(sparqlSelectEndpoint);
		repository.initialize();
		repositoryConnection = repository.getConnection();
	}

	public void reinit() throws UnknownHostException {
		try {
			repositoryConnection.close();
			repository.shutDown();
		} catch (RepositoryException e) {
			log.error("Error closing ontology repository", e);
		}
		init();
	}

	public String getServiceRequestReference(String graph, String resource) {
		String query = serviceReferenceRequestQuery.replaceAll("##GRAPH_URI##", graph).replaceAll("##RESOURCE_URI##",
				resource);
		String toReturn = null;
		try {
			toReturn = serviceReferenceRequestTemplate + URLEncoder.encode(query, StandardCharsets.UTF_8.name());
			return toReturn;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return toReturn;

	}

	public String transformWorkflowToOWLS(ToreadorWorkflow workflow, ToreadorReferenceModeEnum explicitReferences)
			throws ToreadorOntoEditorException {
		kb = OWLFactory.createKB();
		ont = kb.createOntology(URI.create(baseURI));

		service = ont.createService(URIUtils.createURI(URI.create(baseURI), "DataCleaningService"));
		process = ont.createCompositeProcess(URIUtils.createURI(URI.create(baseURI), "DataCleaningProcess"));
		profile = ont.createProfile(URIUtils.createURI(URI.create(baseURI), "DataCleaningProfile"));
		grounding = ont.createWSDLGrounding(URIUtils.createURI(URI.create(baseURI), "DataCleaningGrounding"));
		sequence = null;
		compositeProcess = null;

		this.alreadyProcessed = new ArrayList<>();
		this.referenceAsGetRequest = explicitReferences;
		this.sequenceFirst = true;
		String graphUri = workflow.getId();
		String serviceUri = graphUri + "#";
		String label = "MainWorkflow";
		groundingIRI = (valueFactory.createIRI(serviceUri + label + "Grounding"));
		IRI processIRI = (valueFactory.createIRI(serviceUri + label + "Process"));
		IRI profileIRI = (valueFactory.createIRI(serviceUri + label + "Profile"));
		IRI serviceIRI = (valueFactory.createIRI(serviceUri + label + "Service"));
		// create basic model
		rdfGraphModelBuilder = new ModelBuilder();
		rdfGraphModelBuilder.namedGraph(graphUri)
				.setNamespace("process", "http://www.daml.org/services/owl-s/1.2/Process.owl#")
				.setNamespace("service", "http://www.daml.org/services/owl-s/1.2/Service.owl#")
				.setNamespace("grounding", "http://www.daml.org/services/owl-s/1.2/Grounding.owl#")
				.setNamespace("profile", "http://www.daml.org/services/owl-s/1.2/Profile.owl#")
				.setNamespace("rdfs", "http://www.w3.org/2000/01/rdf-schema#")
				.setNamespace("list", "http://www.daml.org/services/owl-s/1.2/generic/ObjectList.owl#")
				.setNamespace("owl", "http://www.w3.org/2002/07/owl#");
		// imports the owl-s ontologies required
		rdfGraphModelBuilder
				.add(OWL.ONTOLOGY, OWL.IMPORTS,
						valueFactory.createIRI("http://www.daml.org/services/owl-s/1.2/Grounding.owl"))
				.add(OWL.ONTOLOGY, OWL.IMPORTS,
						valueFactory.createIRI("http://www.daml.org/services/owl-s/1.2/Profile.owl"))
				.add(OWL.ONTOLOGY, OWL.IMPORTS,
						valueFactory.createIRI("http://www.daml.org/services/owl-s/1.2/Process.owl"))
				.add(OWL.ONTOLOGY, OWL.IMPORTS,
						valueFactory.createIRI("http://www.daml.org/services/owl-s/1.2/Service.owl"))
				// create instances of main components and relate them whit
				// main service
				.add(groundingIRI, RDF.TYPE, "grounding:WsdlGrounding").add(profileIRI, RDF.TYPE, "profile:Profile")
				.add(serviceIRI, RDF.TYPE, "service:Service").subject(serviceIRI).add("service:supports", groundingIRI)
				.add("service:describedBy", processIRI).add(RDFS.LABEL, label + "Service")
				.add("service:presents", profileIRI).subject(groundingIRI).add(RDFS.LABEL, label + "Grounding")
				.add("service:supportedBy", serviceIRI).subject(processIRI).add(RDF.TYPE, "process:CompositeProcess")
				.add(RDFS.LABEL, label + "Process").add("service:describes", serviceIRI).subject(profileIRI)
				.add(RDF.TYPE, "profile:Profile").add("service:presentedBy", serviceIRI)
				.add("profile:serviceName", label).add(RDFS.LABEL, label + "Profile");
		List<ToreadorNode> startingNodes = workflow.getStartingNodes();
		IRI controlConstructIRI = createControlConstructTaiger(processIRI, workflow, startingNodes, false);
		rdfGraphModelBuilder.subject(processIRI).add("process:composedOf", controlConstructIRI);
		// serialization of the model as RDF/XML
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			RDFHandler rdfWriter = new RDFXMLPrettyWriterFactory().getWriter(baos);// Rio.createWriter(RDFFormat.RDFXML,
																					// sw);
			Rio.write(rdfGraphModelBuilder.build(), rdfWriter);
		} finally {
			try {
				String string = baos.toString(StandardCharsets.UTF_8.name());
				String result = string.replaceAll("[\\t\\n\\r]+", " ");
				log.info(printWorkflow(string));
				return result;
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}




	
	private IRI createControlConstructTaiger(IRI processIRI, ToreadorWorkflow workflow,
			List<ToreadorNode> startingNodes, boolean skipAlreadyAdded) {
		// removes the nodes that has been already processed.
		// this is necessary for the iterations
		if (!skipAlreadyAdded)
			startingNodes.removeAll(this.alreadyProcessed);
		if (startingNodes == null || startingNodes.isEmpty()
				|| (startingNodes.get(0).getData().getService() == null && startingNodes.get(0).getNodeType()
						.equalsIgnoreCase(ToreadorConstantStrings.WORKFLOW_SERVICE_TYPE_END))) {
			// IRI controlConstructIRI =
			// valueFactory.createIRI(String.valueOf(processIRI));
			// rdfGraphModelBuilder.subject(controlConstructIRI).add("list:rest",
			// "http://www.daml.org/services/owl-s/1.2/generic/ObjectList.owl#nil");
			return valueFactory.createIRI("http://www.daml.org/services/owl-s/1.2/generic/ObjectList.owl#nil");
		}
		if (startingNodes.size() == 1) {
			ToreadorNode node = startingNodes.get(0);
			// add node to already processed nodes
			this.alreadyProcessed.add(node);
			System.out.println("alreadyProcessed:: " + alreadyProcessed.toString());
			// get in the
			String cycleNodeType = workflow.getCycleNodeType(node);
			// this is the case where we have an iteration
			if (!Strings.isNullOrEmpty(cycleNodeType)) {
				IRI controlConstructIRI = valueFactory.createIRI(processIRI + cycleNodeType);
				if (ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_REPEATUNTIL.equals(cycleNodeType)) {
					rdfGraphModelBuilder.subject(controlConstructIRI).add(RDF.TYPE, "process:Repeat-Until");
					rdfGraphModelBuilder.subject(controlConstructIRI).add("process:untilCondition", valueFactory
							.createLiteral(node.getData().getNodeType().getConf().getCondition(), XMLSchema.STRING));
					List<ToreadorNode> nextNodes = workflow.getNextNodes(node);

					IRI toAdd = createControlConstructTaiger(controlConstructIRI, workflow, nextNodes, true);
					rdfGraphModelBuilder.subject(controlConstructIRI).add("process:untilProcess", toAdd);
				} else {
					rdfGraphModelBuilder.subject(controlConstructIRI).add(RDF.TYPE, "process:Repeat-While");
					rdfGraphModelBuilder.subject(controlConstructIRI).add("process:whileCondition", valueFactory
							.createLiteral(node.getData().getNodeType().getConf().getCondition(), XMLSchema.STRING));
					List<ToreadorNode> nextNodes = workflow.getNextNodes(node);

					IRI toAdd = createControlConstructTaiger(controlConstructIRI, workflow, nextNodes, true);
					rdfGraphModelBuilder.subject(controlConstructIRI).add("process:whileProcess", toAdd);
				}
				return controlConstructIRI;
			} else {
				// this is sequence or operator type.
				IRI controlConstructIRI = valueFactory
						.createIRI(processIRI + ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_SEQUENCE);
				if (node.getNodeType().equals(ToreadorConstantStrings.WORKFLOW_SERVICE_TYPE_SERVICE)) {

					if (sequenceFirst) {
						rdfGraphModelBuilder.subject(controlConstructIRI).add(RDF.TYPE, "process:Sequence");
						sequenceFirst = false;
					}

					List<ToreadorNode> nextNodes = workflow.getNextNodes(node);
					IRI toAdd = createControlConstructBagTaiger(controlConstructIRI, workflow, node, nextNodes);
					rdfGraphModelBuilder.subject(controlConstructIRI).add("process:components", toAdd);
					return controlConstructIRI;
				} else {
					switch (node.getOperatorType()) {
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_SPLIT: {
						rdfGraphModelBuilder.subject(controlConstructIRI).add(RDF.TYPE, "process:Sequence");
						processType = "process:Split";
						
						List<ToreadorNode> nextNodes = workflow.getNextNodes(node);

						IRI componentIRI = valueFactory.createIRI(controlConstructIRI.stringValue() + "Component");
						rdfGraphModelBuilder.subject(componentIRI).add(RDF.TYPE, "process:ControlConstructBag");

						IRI ccElem = createControlConstructTaiger(controlConstructIRI, workflow, nextNodes, false);
						ToreadorNode firstJoinFromHere = workflow.getFirstJoinFromHere(nextNodes);
						List<ToreadorNode> firstJoinFromHereList = new ArrayList();
						if (firstJoinFromHere == null && !nextNodes.isEmpty()) {
							List<ToreadorNode> nextNodes2 = workflow.getNextNodes(nextNodes.get(0));
							for (ToreadorNode toreadorNode : nextNodes2) {
								List<ToreadorEdge> inEdges = workflow.getInEdges(toreadorNode);
								if(inEdges.size() == 2){
									firstJoinFromHere = toreadorNode;
									sequenceFirst = true;
								}
							}
						}
						firstJoinFromHereList.add(firstJoinFromHere);
						IRI rest = createControlConstructTaiger(componentIRI, workflow,
								firstJoinFromHereList, true);
						
						rdfGraphModelBuilder.subject(componentIRI).add("list:first", ccElem);
						
						rdfGraphModelBuilder.subject(componentIRI).add("list:rest", rest);

						rdfGraphModelBuilder.subject(controlConstructIRI).add("process:components", componentIRI);
						return controlConstructIRI;
					}
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_SPLITJOIN: {
						rdfGraphModelBuilder.subject(controlConstructIRI).add(RDF.TYPE, "process:Sequence");
						processType = "process:Split-Join";
						List<ToreadorNode> nextNodes = workflow.getNextNodes(node);

						IRI componentIRI = valueFactory.createIRI(controlConstructIRI.stringValue() + "Component");
						rdfGraphModelBuilder.subject(componentIRI).add(RDF.TYPE, "process:ControlConstructBag");
						IRI ccElem = createControlConstructTaiger(controlConstructIRI, workflow, nextNodes, false);

						rdfGraphModelBuilder.subject(componentIRI).add("list:first", ccElem);
//						if(nextNodes.size() == 1){
//							List<ToreadorNode> nextNodes2 = workflow.getNextNodes(nextNodes.get(0));
////							if(nextNodes2.get(0).getNodeType()
////							.equalsIgnoreCase(ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_JOIN)){
//								sequenceFirst = true;
//								IRI rest = createControlConstructTaiger(componentIRI, workflow,
//										nextNodes2, true);
//								
//								rdfGraphModelBuilder.subject(componentIRI).add("list:rest", rest);
//			
//								rdfGraphModelBuilder.subject(controlConstructIRI).add("process:components", componentIRI);
//								return controlConstructIRI;
////							}
//						}
						
						ToreadorNode firstJoinFromHere = workflow.getFirstJoinFromHere(nextNodes);
						sequenceFirst = true;
						IRI rest = createControlConstructTaiger(componentIRI, workflow,
								workflow.getNextNodes(firstJoinFromHere), true);
						
						rdfGraphModelBuilder.subject(componentIRI).add("list:rest", rest);
	
						rdfGraphModelBuilder.subject(controlConstructIRI).add("process:components", componentIRI);
						return controlConstructIRI;
					}
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_CHOICE: {
						rdfGraphModelBuilder.subject(controlConstructIRI).add(RDF.TYPE, "process:Choice");
						List<ToreadorNode> nextNodes = workflow.getNextNodes(node);
						ToreadorNode nextNode = nextNodes.get(0);
						nextNodes.remove(nextNode);
						IRI toAdd = createControlConstructBag(controlConstructIRI, workflow, nextNode, nextNodes);
						rdfGraphModelBuilder.subject(controlConstructIRI).add("process:components", toAdd);
						return controlConstructIRI;

					}
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_IF: {
						rdfGraphModelBuilder.subject(controlConstructIRI).add(RDF.TYPE, "process:If-Then-Else");
						List<ToreadorNode> nextNodes = workflow.getNextNodes(node);
						if (!nextNodes.isEmpty()) {
							ToreadorNode innerNode = nextNodes.get(0);
							nextNodes.remove(innerNode);
							// IRI componentIRI =
							// valueFactory.createIRI(controlConstructIRI.stringValue()
							// + "Component");
							IRI thenIRI = valueFactory.createIRI(controlConstructIRI.stringValue() + "Then");
							IRI elseIRI = valueFactory.createIRI(controlConstructIRI.stringValue() + "Else");
							rdfGraphModelBuilder.subject(controlConstructIRI).add("process:ifCondition",
									valueFactory.createLiteral(node.getData().getNodeType().getConf().getCondition(),
											XMLSchema.STRING));
							ToreadorNode nodeThen = workflow.getNodeFromEdge(
									workflow.getElements().getEdge(node.getData().getNodeType().getConf().getIfTrue()));
							ToreadorNode nodeElse = workflow.getNodeFromEdge(workflow.getElements()
									.getEdge(node.getData().getNodeType().getConf().getIfFalse()));
							IRI toAddThen = createControlConstruct(thenIRI, workflow,
									new ArrayList<ToreadorNode>(Arrays.asList(nodeThen)), true);
							rdfGraphModelBuilder.subject(controlConstructIRI).add("process:then", toAddThen);
							IRI toAddElse = createControlConstruct(elseIRI, workflow,
									new ArrayList<ToreadorNode>(Arrays.asList(nodeElse)), true);
							rdfGraphModelBuilder.subject(controlConstructIRI).add("process:else", toAddElse);
						}
						return controlConstructIRI;
					}
					// TODO ask CINI if is this correct, it should be ok
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_REPEATUNTIL:
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_REPEATWHILE:
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_JOIN: {
						processType = null;
						return valueFactory
								.createIRI("http://www.daml.org/services/owl-s/1.2/generic/ObjectList.owl#nil");
					}
					}

				}
				return controlConstructIRI;
			}
		} else {
			// this is sequence or operator type.
			IRI controlConstructIRI = valueFactory
					.createIRI(processIRI + ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_ANYORDER);
			rdfGraphModelBuilder.subject(controlConstructIRI).add(RDF.TYPE,
					processType == null ? "process:Any-Order" : processType);
			if (processType != null)
				processType = null;

			ToreadorNode startNode = startingNodes.get(0);
			ToreadorNode startNode1 = startingNodes.get(1);
//			if(this.alreadyProcessed.contains(startNode)){
//				startNode = startingNodes.get(1);
//				// add node to already processed nodes
//				//this.alreadyProcessed.add(startNode);
//			}
//			//startingNodes.remove(startNode);

			String cycleNodeType = workflow.getCycleNodeType(startNode);
			if ((ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_REPEATUNTIL.equals(cycleNodeType))
					|| (ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_REPEATWHILE.equals(cycleNodeType))) {
				controlConstructIRI = valueFactory.createIRI(processIRI + cycleNodeType);
			}
			
			sequenceFirst = true;
			
//			IRI componentIRI = valueFactory.createIRI(controlConstructIRI.stringValue() + "Component");
//			rdfGraphModelBuilder.subject(componentIRI).add(RDF.TYPE, "process:ControlConstructBag");
//			IRI ccElem = createControlConstructTaiger(controlConstructIRI, workflow, nextNodes, false);
//
//			rdfGraphModelBuilder.subject(componentIRI).add("list:first", ccElem);
			
			List<ToreadorNode> t1 = new ArrayList();
			t1.add(startNode);
			List<ToreadorNode> t2 = new ArrayList();
			t2.add(startNode1);
			IRI componentIRI = valueFactory.createIRI(controlConstructIRI.stringValue() + "Component");
			rdfGraphModelBuilder.subject(componentIRI).add(RDF.TYPE, "process:ControlConstructBag");
			IRI ccElem = createControlConstructTaiger(controlConstructIRI, workflow, t1, false);
			rdfGraphModelBuilder.subject(componentIRI).add("list:first", ccElem);
			IRI rest = createControlConstructTaiger(componentIRI, workflow,
					t2, true);
			
			rdfGraphModelBuilder.subject(componentIRI).add("list:first", ccElem);
			
			rdfGraphModelBuilder.subject(componentIRI).add("list:rest", rest);

			rdfGraphModelBuilder.subject(controlConstructIRI).add("process:components", componentIRI);
			return controlConstructIRI;		
			
//			IRI componentIRI = valueFactory.createIRI(controlConstructIRI.stringValue() + "Component");
//			rdfGraphModelBuilder.subject(componentIRI).add(RDF.TYPE, "process:ControlConstructBag");
//			List<ToreadorNode> t1 = new ArrayList();
//			t1.add(startNode);
//			List<ToreadorNode> t2 = new ArrayList();
//			t2.add(startNode1);
//			IRI ccElem = createControlConstructTaiger(controlConstructIRI, workflow, t1, false);
//			rdfGraphModelBuilder.subject(componentIRI).add("list:first", ccElem);
//			sequenceFirst = true;
//			IRI rest = createControlConstructTaiger(componentIRI, workflow, t2, true);
//			rdfGraphModelBuilder.subject(componentIRI).add("list:rest", rest);
//			rdfGraphModelBuilder.subject(controlConstructIRI).add("process:components", componentIRI);
//			return controlConstructIRI;
		}

	}

	private IRI createControlConstructListTaiger(IRI controlConstructIRI, ToreadorWorkflow workflow, ToreadorNode first,
			List<ToreadorNode> rest) {
		IRI componentIRI = valueFactory.createIRI(controlConstructIRI.stringValue() + "Component");
		rdfGraphModelBuilder.subject(componentIRI).add(RDF.TYPE, "process:ControlConstructList");
		IRI ccElem = createControlComponentTaiger(controlConstructIRI, first, workflow);
		rdfGraphModelBuilder.subject(componentIRI).add("list:first", ccElem);
		IRI toAdd = createControlConstructTaiger(componentIRI, workflow, rest, false);
		rdfGraphModelBuilder.subject(componentIRI).add("list:rest", toAdd);
		return componentIRI;
	}

	private IRI createControlConstructBagTaiger(IRI controlConstructIRI, ToreadorWorkflow workflow, ToreadorNode first,
			List<ToreadorNode> rest) {
		IRI componentIRI = valueFactory.createIRI(controlConstructIRI.stringValue() + "Component");
		rdfGraphModelBuilder.subject(componentIRI).add(RDF.TYPE, "process:ControlConstructBag");
		IRI ccElem = createControlComponentTaiger(controlConstructIRI, first, workflow);
		rdfGraphModelBuilder.subject(componentIRI).add("list:first", ccElem);
		IRI toAdd = createControlConstructTaiger(componentIRI, workflow, rest, false);
		rdfGraphModelBuilder.subject(componentIRI).add("list:rest", toAdd);
		return componentIRI;
	}

	private IRI createControlComponentTaiger(IRI controlConstructIRI, ToreadorNode node, ToreadorWorkflow workflow) {
		// This should check the node type if it is of type service then
		// return the Perform instance otherwise need to return an instance of
		// CompositeProcess
		// max = (a > b) ? a : b;
		String auxFrameworkName = (node.getData().getService().getFramework() != null)
				? node.getData().getService().getFramework().getName().replaceAll(" ", "") : "nullFramework";
		String auxFrameworkVersion = (node.getData().getService().getFramework() != null)
				? node.getData().getService().getFramework().getVersion() : "nullFramework";
		String aux = servicesBaseURI + auxFrameworkName + node.getData().getService().getArea()
				+ node.getData().getService().getId() + node.getData().getService().getName().replaceAll(" ", "") + "-"
				+ auxFrameworkVersion + ".owl";

		String auxName = auxFrameworkName + node.getData().getService().getArea() + node.getData().getService().getId()
				+ node.getData().getService().getName().replaceAll(" ", "") + "-" + auxFrameworkVersion + "Profile";

		if (node.getNodeType().equals(ToreadorConstantStrings.WORKFLOW_SERVICE_TYPE_OPERATOR)) {
			return createControlConstructTaiger(controlConstructIRI, workflow,
					new ArrayList<ToreadorNode>(Arrays.asList(node)), false);
		} else {
			// add node to already processed nodes
			this.alreadyProcessed.add(node);
			// create the atomic process and grounding

			IRI performIri = valueFactory.createIRI(controlConstructIRI + auxName + "Perform");
			switch (referenceAsGetRequest) {
			case GET_REFERENCES: {
				rdfGraphModelBuilder.subject(performIri).add(RDF.TYPE, "process:Perform").add("process:process",
						valueFactory.createIRI(getServiceRequestReference(aux, aux + "#" + auxName)));
				// Add process:hasDataFrom
				IRI processGrounding;
				try {
					processGrounding = ontologyService.getGroundingForService(valueFactory.createIRI(aux),
							valueFactory.createIRI(aux + "#" + node.getData().getService().getName()));
					rdfGraphModelBuilder.subject(groundingIRI).add("grounding:hasAtomicProcessGrounding",
							getServiceRequestReference(aux, processGrounding.stringValue()));
				} catch (ToreadorOntoEditorException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			}
			case EXPLICIT_REFERENCES: {
				rdfGraphModelBuilder.subject(performIri).add(RDF.TYPE, "process:Perform").add("process:process",
						valueFactory.createIRI(aux + "#" + auxName));
				IRI processGrounding;
				try {
					processGrounding = ontologyService.getGroundingForService(valueFactory.createIRI(aux),
							valueFactory.createIRI(aux + "#" + auxName));
					rdfGraphModelBuilder.subject(groundingIRI).add("grounding:hasAtomicProcessGrounding",
							processGrounding);
				} catch (ToreadorOntoEditorException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			}
			case INSERT_OWLS_TRIPLES: {
				rdfGraphModelBuilder.subject(performIri).add(RDF.TYPE, "process:Perform").add("process:process",
						valueFactory.createIRI(aux + "#" + auxName));
				IRI processGrounding;
				// extract the graph that refers to the service
				// important is to check that the triples are added only once.
				// It should not be a problem cause the RDF graph guarantees
				// unique triples.
				GraphQueryResult serviceOWLS;
				try {
					processGrounding = ontologyService.getGroundingForService(valueFactory.createIRI(aux),
							valueFactory.createIRI(aux + "#" + auxName));
					rdfGraphModelBuilder.subject(groundingIRI).add("grounding:hasAtomicProcessGrounding",
							processGrounding);
					serviceOWLS = ontologyService.getServiceOWLS(aux);
					while (serviceOWLS.hasNext()) {
						Statement s = serviceOWLS.next();
						// log.info(s.getSubject() + " " + s.getPredicate() + "
						// " + s.getObject());
						rdfGraphModelBuilder.add(s.getSubject(), s.getPredicate(), s.getObject());
					}
				} catch (ToreadorOntoEditorException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			}
			}
			// add binding
			List<IRI> bindings = addParemeterBinding(node, performIri, aux);
			for (IRI binding : bindings) {
				rdfGraphModelBuilder.subject(performIri).add("process:hasDataFrom", binding);
			}
			return performIri;
		}
	}

//	public String transformWorkflowToOWLS(ToreadorWorkflow workflow, ToreadorReferenceModeEnum explicitReferences)
//			throws ToreadorOntoEditorException {
//		// String graphUri = SparqlUtils.idFromLabel("", servicesBaseURI) +
//		// ".owl";
//
//		kb = OWLFactory.createKB();
//		ont = kb.createOntology(URI.create(baseURI));
//
//		service = ont.createService(URIUtils.createURI(URI.create(baseURI), "DataCleaningService"));
//		process = ont.createCompositeProcess(URIUtils.createURI(URI.create(baseURI), "DataCleaningProcess"));
//		profile = ont.createProfile(URIUtils.createURI(URI.create(baseURI), "DataCleaningProfile"));
//		grounding = ont.createWSDLGrounding(URIUtils.createURI(URI.create(baseURI), "DataCleaningGrounding"));
//		sequence = null;
//		compositeProcess = null;
//
//		this.alreadyProcessed = new ArrayList<>();
//		this.referenceAsGetRequest = explicitReferences;
//		this.sequenceFirst = true;
//		String graphUri = workflow.getId();
//		String serviceUri = graphUri + "#";
//		String label = "MainWorkflow";
//		groundingIRI = (valueFactory.createIRI(serviceUri + label + "Grounding"));
//		IRI processIRI = (valueFactory.createIRI(serviceUri + label + "Process"));
//		IRI profileIRI = (valueFactory.createIRI(serviceUri + label + "Profile"));
//		IRI serviceIRI = (valueFactory.createIRI(serviceUri + label + "Service"));
//		// create basic model
//		rdfGraphModelBuilder = new ModelBuilder();
//		rdfGraphModelBuilder.namedGraph(graphUri)
//				.setNamespace("process", "http://www.daml.org/services/owl-s/1.2/Process.owl#")
//				.setNamespace("service", "http://www.daml.org/services/owl-s/1.2/Service.owl#")
//				.setNamespace("grounding", "http://www.daml.org/services/owl-s/1.2/Grounding.owl#")
//				.setNamespace("profile", "http://www.daml.org/services/owl-s/1.2/Profile.owl#")
//				.setNamespace("rdfs", "http://www.w3.org/2000/01/rdf-schema#")
//				.setNamespace("list", "http://www.daml.org/services/owl-s/1.2/generic/ObjectList.owl#")
//				.setNamespace("owl", "http://www.w3.org/2002/07/owl#");
//		// imports the owl-s ontologies required
//		rdfGraphModelBuilder
//				.add(OWL.ONTOLOGY, OWL.IMPORTS,
//						valueFactory.createIRI("http://www.daml.org/services/owl-s/1.2/Grounding.owl"))
//				.add(OWL.ONTOLOGY, OWL.IMPORTS,
//						valueFactory.createIRI("http://www.daml.org/services/owl-s/1.2/Profile.owl"))
//				.add(OWL.ONTOLOGY, OWL.IMPORTS,
//						valueFactory.createIRI("http://www.daml.org/services/owl-s/1.2/Process.owl"))
//				.add(OWL.ONTOLOGY, OWL.IMPORTS,
//						valueFactory.createIRI("http://www.daml.org/services/owl-s/1.2/Service.owl"))
//				// create instances of main components and relate them whit
//				// main service
//				.add(groundingIRI, RDF.TYPE, "grounding:WsdlGrounding").add(profileIRI, RDF.TYPE, "profile:Profile")
//				.add(serviceIRI, RDF.TYPE, "service:Service").subject(serviceIRI).add("service:supports", groundingIRI)
//				.add("service:describedBy", processIRI).add(RDFS.LABEL, label + "Service")
//				.add("service:presents", profileIRI).subject(groundingIRI).add(RDFS.LABEL, label + "Grounding")
//				.add("service:supportedBy", serviceIRI).subject(processIRI).add(RDF.TYPE, "process:CompositeProcess")
//				.add(RDFS.LABEL, label + "Process").add("service:describes", serviceIRI).subject(profileIRI)
//				.add(RDF.TYPE, "profile:Profile").add("service:presentedBy", serviceIRI)
//				.add("profile:serviceName", label).add(RDFS.LABEL, label + "Profile");
//		// start conversion of the ToreadorWorkflow model
//		List<ToreadorNode> startingNodes = workflow.getStartingNodes();
//		// add inputs and outputs from the starting and ending nodes to the
//		// process
//		// TODO ho rimosso questo, aggiungere dopo quello che ci sta dentro
//		// createCompositeProcess(workflow, processIRI, startingNodes,
//		// rdfGraphModelBuilder);
//
//		/*
//		 * service.addProfile(profile); service.setProcess(process);
//		 * service.addGrounding(grounding); sequence = ont.createSequence(null);
//		 */
//		IRI controlConstructIRI = createControlConstruct(processIRI, workflow, startingNodes, false);
//
//		/*
//		 * then loop over your Workflow to add Sequence, Choice, While etc
//		 * using: Sequence sequence = ont.createSequence(null); SplitJoin
//		 * splitJoin = ont.createSplitJoin(null);
//		 * compositeProcess.setComposedOf(sequence); Choice choice =
//		 * ont.createChoice(null); RepeatWhile repeat =
//		 * ont.createRepeatWhile(.....); Condition cond =
//		 * ont.createSWRLCondition(....);
//		 */
//		// compositeProcess=ont.createCompositeProcess(URI.create(("MasterProcess")));
//		// compositeProcess.setComposedOf(sequence);
//
//		rdfGraphModelBuilder.subject(processIRI).add("process:composedOf", controlConstructIRI);
//		// serialization of the model as RDF/XML
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		try {
//			RDFHandler rdfWriter = new RDFXMLPrettyWriterFactory().getWriter(baos);// Rio.createWriter(RDFFormat.RDFXML,
//																					// sw);
//			Rio.write(rdfGraphModelBuilder.build(), rdfWriter);
//		} finally {
//			try {
//				String string = baos.toString(StandardCharsets.UTF_8.name());
//				String result = string.replaceAll("[\\t\\n\\r]+", " ");
//				log.info(printWorkflow(string));
//				return result;
//			} catch (UnsupportedEncodingException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		return null;
//	}

	private IRI prueba(IRI processIRI, ToreadorWorkflow workflow, List<ToreadorNode> startingNodes,
			boolean skipAlreadyAdded) {

		if (!skipAlreadyAdded)
			startingNodes.removeAll(this.alreadyProcessed);
		if (startingNodes == null || startingNodes.isEmpty()
				|| (startingNodes.get(0).getData().getService() == null && startingNodes.get(0).getNodeType()
						.equalsIgnoreCase(ToreadorConstantStrings.WORKFLOW_SERVICE_TYPE_END))) {
			IRI controlConstructIRI = valueFactory.createIRI(String.valueOf(processIRI));
			// rdfGraphModelBuilder.subject(controlConstructIRI).add("list:rest",
			// "http://www.daml.org/services/owl-s/1.2/generic/ObjectList.owl#nil");
			return valueFactory.createIRI("http://www.daml.org/services/owl-s/1.2/generic/ObjectList.owl#nil");
		}
		if (startingNodes.size() == 1) {
			ToreadorNode node = startingNodes.get(0);
			// add node to already processed nodes
			this.alreadyProcessed.add(node);
			// get in the
			String cycleNodeType = workflow.getCycleNodeType(node);
			// this is the case where we have an iteration
			if (!Strings.isNullOrEmpty(cycleNodeType)) {

				return null;
			} else {
				// this is sequence or operator type.
				IRI controlConstructIRI = valueFactory
						.createIRI(processIRI + ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_SEQUENCE);
				if (node.getNodeType().equals(ToreadorConstantStrings.WORKFLOW_SERVICE_TYPE_SERVICE)) {

					List<ToreadorNode> nextNodes = workflow.getNextNodes(node);
					IRI toAdd = createControlConstructBagPrueba(controlConstructIRI, workflow, node, nextNodes);
					rdfGraphModelBuilder.subject(controlConstructIRI).add("process:components", toAdd);

					process.setComposedOf(sequence);

					return controlConstructIRI;

				} else {
					switch (node.getOperatorType()) {
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_SPLIT: {
						Split split = ont.createSplit(null);
						List<ToreadorNode> nextNodes = workflow.getNextNodes(node);

						return controlConstructIRI;
					}
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_SPLITJOIN: {
						SplitJoin splitJoin = ont.createSplitJoin(null);
						return controlConstructIRI;

					}
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_CHOICE: {
						// Choice choice = ont.createChoice(null);

						List<ToreadorNode> nextNodes = workflow.getNextNodes(node);
						ToreadorNode nextNode = nextNodes.get(0);
						nextNodes.remove(nextNode);
						IRI toAdd = createControlConstructBagPrueba(controlConstructIRI, workflow, nextNode, nextNodes);
						// rdfGraphModelBuilder.subject(controlConstructIRI).add("process:components",
						// toAdd);
						Choice choice = ont.createChoice(URI.create(String.valueOf(toAdd)));
						sequence.addComponent(choice);

						return controlConstructIRI;

					}
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_IF: {

						IRI thenIRI = valueFactory.createIRI(controlConstructIRI.stringValue() + "Then");
						IRI elseIRI = valueFactory.createIRI(controlConstructIRI.stringValue() + "Else");
						IfThenElse ifThenElse = ont.createIfThenElse(null);
						compositeProcess.setComposedOf(ifThenElse);
						Condition condition = ont.createSPARQLCondition(URI.create("process:If-Then-Else"));
						condition.setBody(node.getData().getNodeType().getConf().getCondition());

						// set the condition
						ifThenElse.setCondition(condition);

						// create two produce constructs to generate the results
						ifThenElse.setThen((ControlConstruct) thenIRI);
						ifThenElse.setElse((ControlConstruct) elseIRI);

						sequence.addComponent(ifThenElse);
						List<ToreadorNode> nextNodes = workflow.getNextNodes(node);
						if (!nextNodes.isEmpty()) {

						}
						return controlConstructIRI;

					}
					// TODO ask CINI if is this correct, it should be ok
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_REPEATUNTIL:
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_REPEATWHILE:
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_JOIN: {
						RepeatWhile repeat = ont.createRepeatWhile(null);
						Condition cond = ont.createSWRLCondition(null);
						processType = null;
						return valueFactory
								.createIRI("http://www.daml.org/services/owl-s/1.2/generic/ObjectList.owl#nil");
					}
					}

				}
				return controlConstructIRI;
			}
		} else {
			// this is sequence or operator type.
			IRI controlConstructIRI = valueFactory
					.createIRI(processIRI + ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_ANYORDER);
			rdfGraphModelBuilder.subject(controlConstructIRI).add(RDF.TYPE,
					processType == null ? "process:Any-Order" : processType);
			if (processType != null)
				processType = null;
			ToreadorNode startNode = startingNodes.get(0);
			startingNodes.remove(startNode);
			// add node to already processed nodes
			this.alreadyProcessed.add(startNode);
			String cycleNodeType = workflow.getCycleNodeType(startNode);
			if ((ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_REPEATUNTIL.equals(cycleNodeType))
					|| (ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_REPEATWHILE.equals(cycleNodeType))) {
				controlConstructIRI = valueFactory.createIRI(processIRI + cycleNodeType);
			}
			IRI createControlConstructBag = createControlConstructBag(controlConstructIRI, workflow, startNode,
					startingNodes);
			rdfGraphModelBuilder.subject(controlConstructIRI).add("process:components", createControlConstructBag);
			return controlConstructIRI;
		}

	}

	private IRI createControlConstructBagPrueba(IRI controlConstructIRI, ToreadorWorkflow workflow, ToreadorNode first,
			List<ToreadorNode> rest) {
		IRI componentIRI = valueFactory.createIRI(controlConstructIRI.stringValue() + "Component");

		sequence.addComponent((ControlConstruct) ControlConstructBag);
		IRI ccElem = createControlComponent(controlConstructIRI, first, workflow);
		sequence.addComponent((ControlConstruct) ccElem);
		IRI toAdd = createControlConstruct(componentIRI, workflow, rest, false);
		sequence.addComponent((ControlConstruct) toAdd);

		return componentIRI;
	}

	private IRI createControlComponentPrueba(IRI controlConstructIRI, ToreadorNode node, ToreadorWorkflow workflow) {
		// This should check the node type if it is of type service then
		// return the Perform instance otherwise need to return an instance of
		// CompositeProcess

		String aux = servicesBaseURI + node.getData().getService().getFramework().getName().replaceAll(" ", "")
				+ node.getData().getService().getArea() + node.getData().getService().getId()
				+ node.getData().getService().getName().replaceAll(" ", "") + "-"
				+ node.getData().getService().getFramework().getVersion() + ".owl";

		String auxName = node.getData().getService().getFramework().getName().replaceAll(" ", "")
				+ node.getData().getService().getArea() + node.getData().getService().getId()
				+ node.getData().getService().getName().replaceAll(" ", "") + "-"
				+ node.getData().getService().getFramework().getVersion() + "Profile";

		if (node.getNodeType().equals(ToreadorConstantStrings.WORKFLOW_SERVICE_TYPE_OPERATOR)) {
			return createControlConstruct(controlConstructIRI, workflow,
					new ArrayList<ToreadorNode>(Arrays.asList(node)), false);
		} else {
			// add node to already processed nodes
			this.alreadyProcessed.add(node);
			// create the atomic process and grounding

			IRI performIri = valueFactory.createIRI(controlConstructIRI + auxName + "Perform");

			switch (referenceAsGetRequest) {
			case INSERT_OWLS_TRIPLES: {

				rdfGraphModelBuilder.subject(performIri).add(RDF.TYPE, "process:Perform").add("process:process",
						valueFactory.createIRI(aux + "#" + auxName));

				IRI processGrounding;
				// extract the graph that refers to the service
				// important is to check that the triples are added only once.
				// It should not be a problem cause the RDF graph guarantees
				// unique triples.
				GraphQueryResult serviceOWLS;
				try {
					processGrounding = ontologyService.getGroundingForService(valueFactory.createIRI(aux),
							valueFactory.createIRI(aux + "#" + auxName));
					rdfGraphModelBuilder.subject(groundingIRI).add("grounding:hasAtomicProcessGrounding",
							processGrounding);
					service.addGrounding(grounding);
					serviceOWLS = ontologyService.getServiceOWLS(aux);

					while (serviceOWLS.hasNext()) {
						Statement s = serviceOWLS.next();

						Perform perform = ont.createPerform(URI.create(auxName));
						perform.setProcess((Process) s);
						sequence.addComponent(perform);
					}
				} catch (ToreadorOntoEditorException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			}
			}
			// add binding
			List<IRI> bindings = addParemeterBinding(node, performIri, aux);
			for (IRI binding : bindings) {
				rdfGraphModelBuilder.subject(performIri).add("process:hasDataFrom", binding);
			}
			return performIri;
		}
	}

	private IRI createControlConstruct(IRI processIRI, ToreadorWorkflow workflow, List<ToreadorNode> startingNodes,
			boolean skipAlreadyAdded) {
		// removes the nodes that has been already processed.
		// this is necessary for the iterations
		System.out.println("----------------------");
		System.out.println("createControlConstruct");
		System.out.println("----------------------");
		System.out.println("processIRI:: " + processIRI);
		System.out.println("startingNodes:::" + startingNodes.toString());

		if (!skipAlreadyAdded)
			startingNodes.removeAll(this.alreadyProcessed);
		if (startingNodes == null || startingNodes.isEmpty()
				|| (startingNodes.get(0).getData().getService() == null && startingNodes.get(0).getNodeType()
						.equalsIgnoreCase(ToreadorConstantStrings.WORKFLOW_SERVICE_TYPE_END))) {
			IRI controlConstructIRI = valueFactory.createIRI(String.valueOf(processIRI));
			// rdfGraphModelBuilder.subject(controlConstructIRI).add("list:rest",
			// "http://www.daml.org/services/owl-s/1.2/generic/ObjectList.owl#nil");
			return valueFactory.createIRI("http://www.daml.org/services/owl-s/1.2/generic/ObjectList.owl#nil");
		}
		if (startingNodes.size() == 1) {
			ToreadorNode node = startingNodes.get(0);
			// add node to already processed nodes
			this.alreadyProcessed.add(node);
			System.out.println("alreadyProcessed:: " + alreadyProcessed.toString());
			// get in the
			String cycleNodeType = workflow.getCycleNodeType(node);
			// this is the case where we have an iteration
			if (!Strings.isNullOrEmpty(cycleNodeType)) {
				IRI controlConstructIRI = valueFactory.createIRI(processIRI + cycleNodeType);
				if (ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_REPEATUNTIL.equals(cycleNodeType)) {
					rdfGraphModelBuilder.subject(controlConstructIRI).add(RDF.TYPE, "process:Repeat-Until");
					rdfGraphModelBuilder.subject(controlConstructIRI).add("process:untilCondition", valueFactory
							.createLiteral(node.getData().getNodeType().getConf().getCondition(), XMLSchema.STRING));
					List<ToreadorNode> nextNodes = workflow.getNextNodes(node);

					IRI toAdd = createControlConstruct(controlConstructIRI, workflow, nextNodes, true);
					rdfGraphModelBuilder.subject(controlConstructIRI).add("process:untilProcess", toAdd);
				} else {
					rdfGraphModelBuilder.subject(controlConstructIRI).add(RDF.TYPE, "process:Repeat-While");
					rdfGraphModelBuilder.subject(controlConstructIRI).add("process:whileCondition", valueFactory
							.createLiteral(node.getData().getNodeType().getConf().getCondition(), XMLSchema.STRING));
					List<ToreadorNode> nextNodes = workflow.getNextNodes(node);

					IRI toAdd = createControlConstruct(controlConstructIRI, workflow, nextNodes, true);
					rdfGraphModelBuilder.subject(controlConstructIRI).add("process:whileProcess", toAdd);
				}
				return controlConstructIRI;
			} else {
				// this is sequence or operator type.
				System.out.println("SEQUENCE");
				IRI controlConstructIRI = valueFactory
						.createIRI(processIRI + ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_SEQUENCE);
				if (node.getNodeType().equals(ToreadorConstantStrings.WORKFLOW_SERVICE_TYPE_SERVICE)) {

					if (sequenceFirst) {
						rdfGraphModelBuilder.subject(controlConstructIRI).add(RDF.TYPE, "process:Sequence");
						sequenceFirst = false;
					}

					List<ToreadorNode> nextNodes = workflow.getNextNodes(node);
					System.out.println("Node::" + node.toString());
					System.out.println("nextNodes::" + nextNodes.toString());
					IRI toAdd = createControlConstructBag(controlConstructIRI, workflow, node, nextNodes);
					rdfGraphModelBuilder.subject(controlConstructIRI).add("process:components", toAdd);
					System.out.println("controlConstructIRI:" + controlConstructIRI.toString());
					System.out.println("toAdd:" + toAdd.toString());
					return controlConstructIRI;
				} else {
					switch (node.getOperatorType()) {
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_SPLIT: {
						System.out.println("SPLIT");
						rdfGraphModelBuilder.subject(controlConstructIRI).add(RDF.TYPE, "process:Sequence");
						processType = "process:Split";
						List<ToreadorNode> nextNodes = workflow.getNextNodes(node);

						IRI componentIRI = valueFactory.createIRI(controlConstructIRI.stringValue() + "Component");
						rdfGraphModelBuilder.subject(componentIRI).add(RDF.TYPE, "process:ControlConstructBag");
						System.out.println("node::" + node.toString());
						System.out.println("nextNodes::" + nextNodes.toString());
						IRI ccElem = createControlConstruct(controlConstructIRI, workflow, nextNodes, false);
						rdfGraphModelBuilder.subject(componentIRI).add("list:first", ccElem);
						ToreadorNode firstJoinFromHere = workflow.getFirstJoinFromHere(nextNodes);
						System.out.println("nextNodes::" + nextNodes.toString());
						System.out.println("firstJoinFromHere::" + firstJoinFromHere.toString());
						IRI rest = createControlConstruct(componentIRI, workflow,
								workflow.getNextNodes(firstJoinFromHere), false);
						rdfGraphModelBuilder.subject(componentIRI).add("list:rest", rest);
						System.out.println("componentIRI:" + componentIRI.toString());
						System.out.println("rest:" + rest.toString());
						rdfGraphModelBuilder.subject(controlConstructIRI).add("process:components", componentIRI);
						System.out.println("controlConstructIRI:" + controlConstructIRI.toString());
						System.out.println("componentIRI:" + componentIRI.toString());
						return controlConstructIRI;
					}
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_SPLITJOIN: {
						System.out.println("SPLIT JOIN");
						rdfGraphModelBuilder.subject(controlConstructIRI).add(RDF.TYPE, "process:Sequence");
						processType = "process:Split-Join";
						List<ToreadorNode> nextNodes = workflow.getNextNodes(node);
						// ToreadorNode nextNode = nextNodes.get(0);
						// nextNodes.remove(nextNode);
						// IRI toAdd =
						// createControlConstructBag(controlConstructIRI,
						// workflow,
						// nextNode, nextNodes);

						IRI componentIRI = valueFactory.createIRI(controlConstructIRI.stringValue() + "Component");
						rdfGraphModelBuilder.subject(componentIRI).add(RDF.TYPE, "process:ControlConstructBag");
						System.out.println("node::" + node.toString());
						System.out.println("nextNodes::" + nextNodes.toString());
						IRI ccElem = createControlConstruct(controlConstructIRI, workflow, nextNodes, false);
						rdfGraphModelBuilder.subject(componentIRI).add("list:first", ccElem);
						ToreadorNode firstJoinFromHere = workflow.getFirstJoinFromHere(nextNodes);

						System.out.println("firstJoinFromHere::" + firstJoinFromHere.toString());
						System.out.println("nextNodes::" + nextNodes.toString());
						IRI rest = createControlConstruct(componentIRI, workflow,
								workflow.getNextNodes(firstJoinFromHere), false);
						rdfGraphModelBuilder.subject(componentIRI).add("list:rest", rest);
						System.out.println("componentIRI:" + componentIRI.toString());
						System.out.println("rest:" + rest.toString());
						rdfGraphModelBuilder.subject(controlConstructIRI).add("process:components", componentIRI);
						System.out.println("controlConstructIRI:" + controlConstructIRI.toString());
						System.out.println("componentIRI:" + componentIRI.toString());
						return controlConstructIRI;

					}
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_CHOICE: {
						rdfGraphModelBuilder.subject(controlConstructIRI).add(RDF.TYPE, "process:Choice");
						List<ToreadorNode> nextNodes = workflow.getNextNodes(node);
						ToreadorNode nextNode = nextNodes.get(0);
						nextNodes.remove(nextNode);
						IRI toAdd = createControlConstructBag(controlConstructIRI, workflow, nextNode, nextNodes);
						rdfGraphModelBuilder.subject(controlConstructIRI).add("process:components", toAdd);
						return controlConstructIRI;

					}
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_IF: {
						rdfGraphModelBuilder.subject(controlConstructIRI).add(RDF.TYPE, "process:If-Then-Else");
						List<ToreadorNode> nextNodes = workflow.getNextNodes(node);
						if (!nextNodes.isEmpty()) {
							ToreadorNode innerNode = nextNodes.get(0);
							nextNodes.remove(innerNode);
							// IRI componentIRI =
							// valueFactory.createIRI(controlConstructIRI.stringValue()
							// + "Component");
							IRI thenIRI = valueFactory.createIRI(controlConstructIRI.stringValue() + "Then");
							IRI elseIRI = valueFactory.createIRI(controlConstructIRI.stringValue() + "Else");
							rdfGraphModelBuilder.subject(controlConstructIRI).add("process:ifCondition",
									valueFactory.createLiteral(node.getData().getNodeType().getConf().getCondition(),
											XMLSchema.STRING));
							ToreadorNode nodeThen = workflow.getNodeFromEdge(
									workflow.getElements().getEdge(node.getData().getNodeType().getConf().getIfTrue()));
							ToreadorNode nodeElse = workflow.getNodeFromEdge(workflow.getElements()
									.getEdge(node.getData().getNodeType().getConf().getIfFalse()));
							IRI toAddThen = createControlConstruct(thenIRI, workflow,
									new ArrayList<ToreadorNode>(Arrays.asList(nodeThen)), true);
							rdfGraphModelBuilder.subject(controlConstructIRI).add("process:then", toAddThen);
							IRI toAddElse = createControlConstruct(elseIRI, workflow,
									new ArrayList<ToreadorNode>(Arrays.asList(nodeElse)), true);
							rdfGraphModelBuilder.subject(controlConstructIRI).add("process:else", toAddElse);
						}
						return controlConstructIRI;

					}
					// TODO ask CINI if is this correct, it should be ok
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_REPEATUNTIL:
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_REPEATWHILE:
					case ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_JOIN: {
						processType = null;
						return valueFactory
								.createIRI("http://www.daml.org/services/owl-s/1.2/generic/ObjectList.owl#nil");
					}
					}

				}
				return controlConstructIRI;
			}
		} else {
			// this is sequence or operator type.
			IRI controlConstructIRI = valueFactory
					.createIRI(processIRI + ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_ANYORDER);
			rdfGraphModelBuilder.subject(controlConstructIRI).add(RDF.TYPE,
					processType == null ? "process:Any-Order" : processType);
			if (processType != null)
				processType = null;
			ToreadorNode startNode = startingNodes.get(0);
			startingNodes.remove(startNode);
			// add node to already processed nodes
			this.alreadyProcessed.add(startNode);
			String cycleNodeType = workflow.getCycleNodeType(startNode);
			if ((ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_REPEATUNTIL.equals(cycleNodeType))
					|| (ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_REPEATWHILE.equals(cycleNodeType))) {
				controlConstructIRI = valueFactory.createIRI(processIRI + cycleNodeType);
			}
			IRI createControlConstructBag = createControlConstructBag(controlConstructIRI, workflow, startNode,
					startingNodes);
			rdfGraphModelBuilder.subject(controlConstructIRI).add("process:components", createControlConstructBag);

			return controlConstructIRI;
		}

	}

	private IRI createControlConstructBag(IRI controlConstructIRI, ToreadorWorkflow workflow, ToreadorNode first,
			List<ToreadorNode> rest) {
		System.out.println("-------------------------");
		System.out.println("createControlConstructBag");
		System.out.println("-------------------------");
		System.out.println("controlConstructIRI::" + controlConstructIRI.toString());
		System.out.println("first::" + first.toString());
		System.out.println("rest::" + rest.toString());
		IRI componentIRI = valueFactory.createIRI(controlConstructIRI.stringValue() + "Component");
		rdfGraphModelBuilder.subject(componentIRI).add(RDF.TYPE, "process:ControlConstructBag");
		System.out.println("componentIRI:::" + componentIRI.toString());
		IRI ccElem = createControlComponent(controlConstructIRI, first, workflow);
		rdfGraphModelBuilder.subject(componentIRI).add("list:first", ccElem);
		System.out.println("list:first:::" + ccElem.toString());
		System.out.println("componentIRI:::" + componentIRI.toString());
		System.out.println("rest:::" + rest.toString());
		IRI toAdd = createControlConstruct(componentIRI, workflow, rest, false);
		rdfGraphModelBuilder.subject(componentIRI).add("list:rest", toAdd);
		System.out.println("list:rest:::" + toAdd.toString());
		return componentIRI;
	}

	/*
	 * private IRI createControlComponent(IRI controlConstructIRI, ToreadorNode
	 * node, ToreadorWorkflow workflow) { // This should check the node type if
	 * it is of type service then // return the Perform instance otherwise need
	 * to return an instance of // CompositeProcess if
	 * (node.getNodeType().equals(ToreadorConstantStrings.
	 * WORKFLOW_SERVICE_TYPE_OPERATOR)) { return
	 * createControlConstruct(controlConstructIRI, workflow, new
	 * ArrayList<ToreadorNode>(Arrays.asList(node)), false); } else { // add
	 * node to already processed nodes this.alreadyProcessed.add(node); //
	 * create the atomic process and grounding
	 * 
	 * IRI performIri = valueFactory .createIRI(controlConstructIRI +
	 * node.getData().getService().getName() + "Perform"); switch
	 * (referenceAsGetRequest) { case GET_REFERENCES: {
	 * rdfGraphModelBuilder.subject(performIri).add(RDF.TYPE,
	 * "process:Perform").add("process:process",
	 * valueFactory.createIRI(getServiceRequestReference(node.getData().
	 * getService().getOntoId(), node.getData().getService().getOntoId() + "#" +
	 * node.getData().getService().getName()))); // Add process:hasDataFrom IRI
	 * processGrounding; try { processGrounding =
	 * ontologyService.getGroundingForService(
	 * valueFactory.createIRI(node.getData().getService().getOntoId()),
	 * valueFactory.createIRI(node.getData().getService().getId() + "#" +
	 * node.getData().getService().getName()));
	 * rdfGraphModelBuilder.subject(groundingIRI).add(
	 * "grounding:hasAtomicProcessGrounding",
	 * getServiceRequestReference(node.getData().getService().getOntoId(),
	 * processGrounding.stringValue())); } catch (ToreadorOntoEditorException e)
	 * { // TODO Auto-generated catch block e.printStackTrace(); } break; } case
	 * EXPLICIT_REFERENCES: {
	 * rdfGraphModelBuilder.subject(performIri).add(RDF.TYPE,
	 * "process:Perform").add("process:process", valueFactory.createIRI(
	 * node.getData().getService().getOntoId() + "#" +
	 * node.getData().getService().getName())); IRI processGrounding; try {
	 * processGrounding = ontologyService.getGroundingForService(
	 * valueFactory.createIRI(node.getData().getService().getOntoId()),
	 * valueFactory.createIRI(node.getData().getService().getId() + "#" +
	 * node.getData().getService().getName()));
	 * rdfGraphModelBuilder.subject(groundingIRI).add(
	 * "grounding:hasAtomicProcessGrounding", processGrounding); } catch
	 * (ToreadorOntoEditorException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } break; } case INSERT_OWLS_TRIPLES: {
	 * 
	 * rdfGraphModelBuilder.subject(performIri).add(RDF.TYPE,
	 * "process:Perform").add("process:process", valueFactory.createIRI(
	 * node.getData().getService().getOntoId() + "#" +
	 * node.getData().getService().getName())); IRI processGrounding; // extract
	 * the graph that refers to the service // important is to check that the
	 * triples are added only once. // It should not be a problem cause the RDF
	 * graph guarantees // unique triples. GraphQueryResult serviceOWLS; try {
	 * processGrounding = ontologyService.getGroundingForService(
	 * valueFactory.createIRI(node.getData().getService().getOntoId()),
	 * valueFactory.createIRI(node.getData().getService().getId() + "#" +
	 * node.getData().getService().getName()));
	 * rdfGraphModelBuilder.subject(groundingIRI).add(
	 * "grounding:hasAtomicProcessGrounding", processGrounding); serviceOWLS =
	 * ontologyService.getServiceOWLS(node.getData().getService().getOntoId());
	 * while (serviceOWLS.hasNext()) { Statement s = serviceOWLS.next();
	 * rdfGraphModelBuilder.add(s.getSubject(), s.getPredicate(),
	 * s.getObject()); } } catch (ToreadorOntoEditorException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); } break; } } // add
	 * binding List<IRI> bindings = addParemeterBinding(node, performIri); for
	 * (IRI binding : bindings) {
	 * rdfGraphModelBuilder.subject(performIri).add("process:hasDataFrom",
	 * binding); } return performIri; } }
	 */

	private IRI createControlComponent(IRI controlConstructIRI, ToreadorNode node, ToreadorWorkflow workflow) {
		// This should check the node type if it is of type service then
		// return the Perform instance otherwise need to return an instance of
		// CompositeProcess
		// max = (a > b) ? a : b;
		System.out.println("----------------------");
		System.out.println("createControlComponent");
		System.out.println("----------------------");
		System.out.println("controlConstructIRI::" + controlConstructIRI.toString());
		System.out.println("node::" + node.toString());
		String auxFrameworkName = (node.getData().getService().getFramework() != null)
				? node.getData().getService().getFramework().getName().replaceAll(" ", "") : "nullFramework";
		String auxFrameworkVersion = (node.getData().getService().getFramework() != null)
				? node.getData().getService().getFramework().getVersion() : "nullFramework";
		String aux = servicesBaseURI + auxFrameworkName + node.getData().getService().getArea()
				+ node.getData().getService().getId() + node.getData().getService().getName().replaceAll(" ", "") + "-"
				+ auxFrameworkVersion + ".owl";

		String auxName = auxFrameworkName + node.getData().getService().getArea() + node.getData().getService().getId()
				+ node.getData().getService().getName().replaceAll(" ", "") + "-" + auxFrameworkVersion + "Profile";

		if (node.getNodeType().equals(ToreadorConstantStrings.WORKFLOW_SERVICE_TYPE_OPERATOR)) {
			return createControlConstruct(controlConstructIRI, workflow,
					new ArrayList<ToreadorNode>(Arrays.asList(node)), false);
		} else {
			// add node to already processed nodes
			this.alreadyProcessed.add(node);
			// create the atomic process and grounding

			IRI performIri = valueFactory.createIRI(controlConstructIRI + auxName + "Perform");
			switch (referenceAsGetRequest) {
			case INSERT_OWLS_TRIPLES: {
				// http://54.229.142.100:8081/BDMOntologies/SparkANALYTICS26spark-batch-decisiontree-classification-model-2.1.1.owl
				// ontoid y id
				// http://54.229.142.100:8081/BDMOntologies/SparkANALYTICS39spark-batch-naivebayes-predict-2.1.1.owl
				// name SparkANALYTICS39spark-batch-naivebayes-predict-2.1.1
				// //sin el servicesbaseuri
				// http://www.w3.org/2001/XMLSchema#string

				rdfGraphModelBuilder.subject(performIri).add(RDF.TYPE, "process:Perform").add("process:process",
						valueFactory.createIRI(aux + "#" + auxName));
				IRI processGrounding;
				// extract the graph that refers to the service
				// important is to check that the triples are added only once.
				// It should not be a problem cause the RDF graph guarantees
				// unique triples.
				GraphQueryResult serviceOWLS;
				try {
					processGrounding = ontologyService.getGroundingForService(valueFactory.createIRI(aux),
							valueFactory.createIRI(aux + "#" + auxName));
					rdfGraphModelBuilder.subject(groundingIRI).add("grounding:hasAtomicProcessGrounding",
							processGrounding);
					serviceOWLS = ontologyService.getServiceOWLS(aux);
					while (serviceOWLS.hasNext()) {
						Statement s = serviceOWLS.next();
						// log.info(s.getSubject() + " " + s.getPredicate() + "
						// " + s.getObject());
						rdfGraphModelBuilder.add(s.getSubject(), s.getPredicate(), s.getObject());
					}
				} catch (ToreadorOntoEditorException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			}
			}
			// add binding
			List<IRI> bindings = addParemeterBinding(node, performIri, aux);
			for (IRI binding : bindings) {
				rdfGraphModelBuilder.subject(performIri).add("process:hasDataFrom", binding);
			}
			return performIri;
		}
	}

	// TODO here we need to add parameters binding: see
	// https://www.w3.org/Submission/OWL-S/#5 for examples
	/**
	 * EXAMPLE OF BINDING:
	 * <process:Perform rdf:ID="Step1"> <process:process rdf:resource=
	 * "&aux;#S1"/> <process:hasDataFrom> <process:InputBinding>
	 * <process:theParam rdf:resource="&aux;#I12"/> <process:valueData
	 * xsd:datatype="&xsd;#string" >Academic</process:valueData>
	 * </process:InputBinding>
	 * <process:OutputBinding> <process:toParam rdf:resource=
	 * "#ConfirmationNum"/> <process:valueData xsd:datatype="&xsd;#string" >
	 * 00000000</process:valueData> </process:OutputBinding>
	 * </process:hasDataFrom> </process:Perform>
	 */

	/*
	 * private List<IRI> addParemeterBinding(ToreadorNode node, IRI rootNode) {
	 * List<IRI> bindings = new ArrayList<>(); Map<String, Parameter> parameters
	 * = node.getData().getService().getParameters();
	 * 
	 * for (String paramName : parameters.keySet()) { Parameter parameter =
	 * parameters.get(paramName);
	 * 
	 * IRI binding = valueFactory.createIRI(rootNode.toString() + "Binding" +
	 * paramName); if (isInput(parameter)) {
	 * rdfGraphModelBuilder.subject(binding).add(RDF.TYPE,
	 * "process:InputBinding"); } else {
	 * rdfGraphModelBuilder.subject(binding).add(RDF.TYPE,
	 * "process:OutputBinding"); }
	 * 
	 * rdfGraphModelBuilder.subject(binding).add("process:theParam",
	 * parameter.getId());
	 * rdfGraphModelBuilder.subject(binding).add("process:valueData",
	 * valueFactory.createLiteral( Strings.isNullOrEmpty(parameter.getValue()) ?
	 * parameter.getDefaultValue() : parameter.getValue(),
	 * parameter.getType())); bindings.add(binding); } return bindings; }
	 */

	/**
	 * EXAMPLE OF BINDING:
	 * <process:Perform rdf:ID="Step1"> <process:process rdf:resource=
	 * "&aux;#S1"/> <process:hasDataFrom> <process:InputBinding>
	 * <process:theParam rdf:resource="&aux;#I12"/> <process:valueData
	 * xsd:datatype="&xsd;#string" >Academic</process:valueData>
	 * </process:InputBinding>
	 * <process:OutputBinding> <process:toParam rdf:resource=
	 * "#ConfirmationNum"/> <process:valueData xsd:datatype="&xsd;#string" >
	 * 00000000</process:valueData> </process:OutputBinding>
	 * </process:hasDataFrom> </process:Perform>
	 */
	private List<IRI> addParemeterBinding(ToreadorNode node, IRI rootNode, String namespace) {
		List<IRI> bindings = new ArrayList<>();

		// Map<String, Parameter> parameters =
		// node.getData().getService().getParameters();
		// Map<String, it.eng.toreador.catalogue.ApplicationProperty> parameters
		// = (HashMap<String, ApplicationProperty>)
		// node.getData().getService().getProperties();

		Set<it.eng.toreador.catalogue.Property> otra = node.getData().getService().getProperties();

		for (Property paramName : otra) {
			if (paramName instanceof it.eng.toreador.catalogue.ApplicationProperty) {
				it.eng.toreador.catalogue.ApplicationProperty parameter = (ApplicationProperty) paramName;

				IRI binding = valueFactory.createIRI(rootNode.toString() + "Binding" + parameter.getKey());
				/*
				 * if (isInput(parameter)) {
				 * rdfGraphModelBuilder.subject(binding).add(RDF.TYPE,
				 * "process:InputBinding"); } else {
				 * rdfGraphModelBuilder.subject(binding).add(RDF.TYPE,
				 * "process:OutputBinding"); }
				 */

				if (parameter.isInputData()) {
					rdfGraphModelBuilder.subject(binding).add(RDF.TYPE, "process:InputBinding");
				} else if (parameter.isOutputData()) {
					rdfGraphModelBuilder.subject(binding).add(RDF.TYPE, "process:OutputBinding");
				}
				rdfGraphModelBuilder.subject(binding).add("process:mandatory", namespace + "#" + parameter.isMandatory());
				rdfGraphModelBuilder.subject(binding).add("process:theParam", namespace + "#" + parameter.getKey());
				// rdfGraphModelBuilder.subject(binding).add("process:valueData",
				// valueFactory.createLiteral(Strings.isNullOrEmpty(parameter.getValue())
				// ? parameter.getDefaultValue() : parameter.getValue(),
				// "http://www.w3.org/2001/XMLSchema#" + parameter.getType()));

				if (parameter.getValue() != null) {
					rdfGraphModelBuilder.subject(binding).add("process:valueData", valueFactory.createLiteral(
							parameter.getValue(), "http://www.w3.org/2001/XMLSchema#" + parameter.getType()));
				} else {
					rdfGraphModelBuilder.subject(binding).add("process:valueData", valueFactory.createLiteral(
							parameter.getDefaultValue(), "http://www.w3.org/2001/XMLSchema#" + parameter.getType()));
				}

				bindings.add(binding);
			}else if (paramName instanceof it.eng.toreador.catalogue.StaticProperty) {
				it.eng.toreador.catalogue.StaticProperty parameter = (StaticProperty) paramName;
				IRI binding = valueFactory.createIRI(rootNode.toString() + "Binding" + parameter.getKey());
				
				rdfGraphModelBuilder.subject(binding).add("process:mandatory", namespace + "#" + parameter.isMandatory());
				if (parameter.getUri() != null){
					rdfGraphModelBuilder.subject(binding).add("process:refURI", valueFactory.createLiteral(
							parameter.getUri(), "http://www.w3.org/2001/XMLSchema#string"));
				}
				rdfGraphModelBuilder.subject(binding).add("process:theParam", namespace + "#" + parameter.getKey());
				if (parameter.getValue() != null) {
					rdfGraphModelBuilder.subject(binding).add("process:valueData", valueFactory.createLiteral(
							parameter.getValue(), "http://www.w3.org/2001/XMLSchema#" + parameter.getType()));
				} else {
					rdfGraphModelBuilder.subject(binding).add("process:valueData", valueFactory.createLiteral(
							parameter.getDefaultValue(), "http://www.w3.org/2001/XMLSchema#" + parameter.getType()));
				}

			}
		}

		return bindings;
	}

	/*
	 * private boolean isInput(Parameter parameter) { // FIXME this check should
	 * change once from Service description they // indicate which parameters
	 * are input and which ones are output if
	 * (parameter.getKey().toLowerCase().startsWith("output") ||
	 * (parameter.getDescription().toLowerCase().contains("output"))) return
	 * false; else return true; }
	 */

	public String printWorkflow(String owls) throws ToreadorOntoEditorException {
		StringBuffer sb = new StringBuffer();
		InputStream owlsIs = new StringInputStream(owls);
		// Rio also accepts a java.io.Reader as input for the parser.
		try {
			Model model = Rio.parse(owlsIs, "", RDFFormat.RDFXML);
			// Create a new Repository. Here, we choose a database
			// implementation
			// that simply stores everything in main memory.
			Repository db = new SailRepository(new MemoryStore());
			db.initialize();

			// Open a connection to the database
			try (RepositoryConnection conn = db.getConnection()) {
				// add the model
				conn.add(model);
				// here we can perform queries. Probably iteration is required.
				TupleQuery tupleQuery = conn
						.prepareTupleQuery("prefix rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
								+ "prefix process:<http://www.daml.org/services/owl-s/1.2/Process.owl#> "
								+ "SELECT ?main ?typemain ?component WHERE " + "{ ?main process:composedOf ?component. "
								+ " ?main rdf:type ?typemain. }");
				TupleQueryResult result = tupleQuery.evaluate();
				while (result.hasNext()) {
					BindingSet bs = result.next();
					String x = bs.getValue("main").stringValue();
					String typex = bs.getValue("typemain").stringValue();
					String component = bs.getValue("component").stringValue();
					sb.append(x + " of type " + typex + " composedOf " + component + "\n");
					subPrinting(component, sb, -1, conn);
				}
			} finally {
				// before our program exits, make sure the database is properly
				// shut down.
				db.shutDown();
			}
		} catch (RDFParseException | UnsupportedRDFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sb.toString();
	}

	private void subPrinting(String mainElement, StringBuffer sb, int recursions, RepositoryConnection conn) {
		// here we can perform queries. Probably iteration is required.
		TupleQuery tupleQuery = conn
				.prepareTupleQuery("prefix process:<http://www.daml.org/services/owl-s/1.2/Process.owl#> "
						+ "prefix rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
						+ "SELECT ?typex ?component WHERE " + "{<" + mainElement + "> process:components ?component."
						+ " <" + mainElement + "> rdf:type ?typex. }");
		TupleQueryResult result = tupleQuery.evaluate();
		while (result.hasNext()) {
			BindingSet bs = result.next();
			String typex = bs.getValue("typex").stringValue();
			String component = bs.getValue("component").stringValue();
			sb.append(createIndentation(++recursions) + mainElement + " of type " + typex + " composedOf " + component
					+ "\n");
			processControlConstructBag(component, sb, recursions, conn);
		}
	}

	private void processControlConstructBag(String constructBag, StringBuffer sb, int recursions,
			RepositoryConnection conn) {
		// TODO Auto-generated method stub
		// here we can perform queries. Probably iteration is required.
		TupleQuery tupleQuery = conn
				.prepareTupleQuery("prefix list:<http://www.daml.org/services/owl-s/1.2/generic/ObjectList.owl#> "
						+ "prefix process:<http://www.daml.org/services/owl-s/1.2/Process.owl#> "
						+ "prefix rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> " + "SELECT ?first ?rest WHERE "
						+ "{<" + constructBag + "> list:first ?first." + " <" + constructBag + "> list:rest ?rest. }");
		TupleQueryResult result = tupleQuery.evaluate();
		while (result.hasNext()) {
			BindingSet bs = result.next();
			String first = bs.getValue("first").stringValue();
			String rest = bs.getValue("rest").stringValue();
			recursions++;
			sb.append(createIndentation(recursions) + constructBag + " first " + first + "\n");
			subPrinting(first, sb, recursions, conn);
			sb.append(createIndentation(recursions) + constructBag + " rest " + rest + "\n");
			subPrinting(rest, sb, recursions, conn);
		}
	}

	private String createIndentation(int recursions) {
		String t = "";
		for (int i = 0; i <= recursions; i++) {
			t += "\t";
		}
		return t;
	}
}
