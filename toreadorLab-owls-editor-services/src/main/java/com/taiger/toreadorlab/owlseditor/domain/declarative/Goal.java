package com.taiger.toreadorlab.owlseditor.domain.declarative;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Goal extends Incorporates<Indicator> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4067351021332387184L;

	private List<Indicator> incorporates;

	public List<Indicator> getIncorporates() {
		return incorporates;
	}

	public void setIncorporates(List<Indicator> incorporates) {
		this.incorporates = incorporates;
	}

	@Override
	public void addIncorporate(Indicator incorporate) {
		if (this.incorporates == null)
			this.incorporates = new ArrayList<>();
		this.incorporates.add(incorporate);
	}

	public List<String> getCompactRepresentation() {
		List<String> result = new ArrayList<>();
		String entry = this.getLabel()==null?"":this.getLabel().replaceAll(" ", "_");
		if (this.incorporates != null && !incorporates.isEmpty()) {
			for (Indicator indicator : incorporates) {

				List<String> compactIndie = indicator.getCompactRepresentation();
				for (String en : compactIndie) {
					result.add(entry + "."+en);
				}
			}
			return result;
		} else
			return Arrays.asList(entry);	}

}
