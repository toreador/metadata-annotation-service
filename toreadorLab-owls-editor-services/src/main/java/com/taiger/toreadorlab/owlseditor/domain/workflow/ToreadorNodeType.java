package com.taiger.toreadorlab.owlseditor.domain.workflow;

import java.io.Serializable;

public class ToreadorNodeType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4399866565453141918L;
	private String type;
	private String operatorType;
	private ToreadorNodeConfiguration conf;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOperatorType() {
		return operatorType;
	}
	public void setOperatorType(String operatorType) {
		this.operatorType = operatorType;
	}
	public ToreadorNodeConfiguration getConf() {
		return conf;
	}
	public void setConf(ToreadorNodeConfiguration conf) {
		this.conf = conf;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((conf == null) ? 0 : conf.hashCode());
		result = prime * result + ((operatorType == null) ? 0 : operatorType.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ToreadorNodeType other = (ToreadorNodeType) obj;
		if (conf == null) {
			if (other.conf != null)
				return false;
		} else if (!conf.equals(other.conf))
			return false;
		if (operatorType == null) {
			if (other.operatorType != null)
				return false;
		} else if (!operatorType.equals(other.operatorType))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ToreadorNodeType [type=" + type + ", operatorType=" + operatorType + ", conf=" + conf + "]";
	}
	
}
