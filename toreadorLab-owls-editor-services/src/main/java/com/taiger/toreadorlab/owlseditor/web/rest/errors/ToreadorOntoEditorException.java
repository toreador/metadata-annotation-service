package com.taiger.toreadorlab.owlseditor.web.rest.errors;

import org.springframework.http.HttpStatus;

/**
 * Created by rodrigo on 14/11/16. Taiger
 */
public class ToreadorOntoEditorException extends Exception {

	private static final long serialVersionUID = -2140430076887392997L;

	private HttpStatus status;
	private ExceptionData data;

	public ToreadorOntoEditorException(Exception exception) {
		super(exception);
	}

	public ToreadorOntoEditorException(HttpStatus status, ExceptionData data) {
		this.data = data;
		this.status = status;
	}

	public ToreadorOntoEditorException(Exception e, HttpStatus status, ExceptionData data) {
		super(e);
		this.data = data;
		this.status = status;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public ExceptionData getData() {
		return data;
	}

	public void setData(ExceptionData data) {
		this.data = data;
	}
}
