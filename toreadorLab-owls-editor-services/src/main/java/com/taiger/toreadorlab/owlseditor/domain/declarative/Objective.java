package com.taiger.toreadorlab.owlseditor.domain.declarative;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Objective implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2115616650376388015L;
	
	private String label;
	
	private String type;
	
	private String constraint;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getConstraint() {
		return constraint;
	}

	public void setConstraint(String constraint) {
		this.constraint = constraint;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<String> getCompactRepresentation() {
		if (this.label==null) return new ArrayList<>();
		return Arrays.asList(this.label==null?null:this.getLabel().replaceAll(" ", "_"));
	}

}
