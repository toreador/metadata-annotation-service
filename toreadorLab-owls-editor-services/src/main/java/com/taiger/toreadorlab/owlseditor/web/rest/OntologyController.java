package com.taiger.toreadorlab.owlseditor.web.rest;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.taiger.toreadorlab.owlseditor.domain.Element;
import com.taiger.toreadorlab.owlseditor.domain.PropertyValues;
//import com.taiger.toreadorlab.owlseditor.service.ElasticsearchService;
import com.taiger.toreadorlab.owlseditor.service.OntologyService;
import com.taiger.toreadorlab.owlseditor.service.ServiceUser;
import com.taiger.toreadorlab.owlseditor.web.rest.errors.ToreadorOntoEditorException;

@RestController
@RequestMapping("/api/ontology")
public class OntologyController {

	private static final Log log = LogFactory.getLog(OntologyController.class);

	@Autowired
	OntologyService ontologyService;

//	@Autowired
//	ElasticsearchService elasticsearchService;

	@RequestMapping(method = RequestMethod.GET, value="/getClasses")
	public ResponseEntity<Element[]> getClasses() {
		log.info("/getClasses");

		List<Element> listElements = ontologyService.getClasses();

		return ResponseEntity.ok(listElements.toArray(new Element[listElements.size()]));
	}



    @RequestMapping(method = RequestMethod.GET, value="/downloadOWLFiles")
    public ResponseEntity<?> downloadOWLFiles(@RequestParam String path) {
        log.info("/downloadOWLFiles");
        boolean res=false;
        try {
            res=ontologyService.downloadOWLFiles(path);
        }catch (Exception e){
            e.printStackTrace();
        }

        return ResponseEntity.ok(res);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/deleteAssignedUserService")
    public  ResponseEntity<?> deleteAssignedUserService(@RequestBody String json) {
        boolean result=false;
        try {
            JSONObject jsonObj = new JSONObject(json);
            String idUser=jsonObj.get("idUser").toString();
            String idGraph=jsonObj.get("idGraph").toString();
            String graph=jsonObj.get("graph").toString();
            String className=jsonObj.get("className").toString();
            //String altLabel=jsonObj.get("altLabel").toString();

            log.error(idUser);
            log.error(idGraph);
            log.error(graph);
            log.error(className);
            //log.error(altLabel);


            result=ontologyService.deleteAssignedUserService(Integer.parseInt(idUser), idGraph,graph,className);
            return ResponseEntity.ok(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(result);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/assignServiceToUser")
    public  ResponseEntity<?> assignServiceToUser(@RequestBody String json) {
        boolean result=false;
        try {
            JSONObject jsonObj = new JSONObject(json);
            String idUser=jsonObj.get("idUser").toString();
            String idGraph=jsonObj.get("idGraph").toString();
            String graph=jsonObj.get("graph").toString();
            String className=jsonObj.get("className").toString();
            //String altLabel=jsonObj.get("altLabel").toString();

            log.error(idUser);
            log.error(idGraph);
            log.error(graph);
            log.error(className);
            //log.error(altLabel);


             result=ontologyService.assignServiceToUser(Integer.parseInt(idUser), idGraph,graph,className);
            return ResponseEntity.ok(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(result);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/getServiceToUser/{id_user}")
    public  ResponseEntity<?> getServiceToUser(@PathVariable int id_user) {
	    log.info(id_user);
        List<ServiceUser>result=ontologyService.getServiceToUser(id_user);
        return ResponseEntity.ok(result);

    }

    @RequestMapping(method = RequestMethod.GET, value="/getServicesNames")
    public ResponseEntity< String > getServicesName() {
        log.info("/getServicesNames");

        List<String> listElements = null;
        try {
            listElements = ontologyService.getServicesNames();
        } catch (ToreadorOntoEditorException e) {
            e.printStackTrace();
        }

        String json = new Gson().toJson(listElements);
        log.info(json);
        return ResponseEntity.ok(json);
    }

	@RequestMapping(method = RequestMethod.GET, value="/getIndividuals")
	public List<Element> getIndividuals(@RequestParam String cls) {
		log.info("/getIndividuals");
		return ontologyService.getIndividuals(cls);
	}

	@RequestMapping(method = RequestMethod.GET, value="/getPropertyValues")
	public List<PropertyValues> getPropertyValues(@RequestParam String individual) {
		log.info("/getPropertyValues");
		return ontologyService.getPropertyValues(individual);
	}

	@RequestMapping(value = "/clearOntology", method = RequestMethod.POST)
	public void clearOntology() {
		log.info("/clearOntology");
		ontologyService.clearOntology();
	}

	@RequestMapping(value = "/loadOntology", method = RequestMethod.POST)
	public void loadOntology() {
		log.info("/loadOntology");
		ontologyService.loadOntology();
	}

	@RequestMapping(value = "/reloadOntology", method = RequestMethod.POST)
	public void reloadOntology() {
		log.info("/reloadOntology");
		ontologyService.reloadOntology();
	}


//	@RequestMapping(value = "/clearOntologyIndex", method = RequestMethod.POST)
//	public void clearOntologyIndex() {
//		log.info("/clearOntologyIndex");
//		elasticsearchService.clearOntologyIndex();
//	}

//	@RequestMapping(value = "/buildOntologyIndex", method = RequestMethod.POST)
//	public void buildOntologyIndex() {
//		log.info("/buildOntologyIndex");
//		ontologyService.buildOntologyIndex();
//	}

	@RequestMapping(value = "/setOntology", method = RequestMethod.POST)
	public void setOntology(@RequestBody String ontologyFile) {
		log.info("/setOntologyIndex " + ontologyFile);
		//ontologyService.setCurrentOntology(ontologyFile);
	}

//	@RequestMapping(value = "/reinit", method = RequestMethod.POST)
//	public void reinit() {
//		log.info("/reinit");
//		ontologyService.clearOntology();
//		ontologyService.loadOntology();
//		elasticsearchService.clearOntologyIndex();
//		ontologyService.buildOntologyIndex();
//	}

	// CRUD Operations

	/**
	 * Create new individual.
	 *
	 * @param individual
	 *            String containing the individual to be created
	 */
	@RequestMapping(value = "/individual",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createIndividual(@RequestBody List<PropertyValues> individual, @RequestParam(required=false) String context) {
		log.info("POST /individual");
		try {
			Element e = ontologyService.createIndividual(individual, context);
			return ResponseEntity.ok(e);
		} catch (ToreadorOntoEditorException e) {
			return new ResponseEntity<>(e.getData(), e.getStatus());
		}
	}

	/**
	 * Create new individual.
	 *
	 * @param id
	 *            String containing the individal id
	 * @param individual
	 *            String containing the individual to be created
	 */
	@RequestMapping(value = "/individual", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updateIndividual(@RequestParam String id, @RequestParam(required=false) String context, @RequestBody List<PropertyValues> individual) {
		log.info("PUT /individual");
		try {
			Element e = ontologyService.updateIndividual(id, individual, context);
			return ResponseEntity.ok(e);
		} catch (ToreadorOntoEditorException e) {
			return new ResponseEntity<>(e.getData(), e.getStatus());
		}
	}

	/**
	 * Create new individual.
	 *
	 * @param id
	 *            String containing the individual id
	 */
	@RequestMapping(value = "/individual", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteIndividual(@RequestParam String id, @RequestParam(required=false) String context) {
		log.info("DELETE /individual");
		try {
			Element e = ontologyService.deleteIndividual(id, context);
			return ResponseEntity.ok(e);
		} catch (ToreadorOntoEditorException e) {
			return new ResponseEntity<>(e.getData(), e.getStatus());
		}
	}

	@RequestMapping(value = "/possibleProperties", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getPossibleProperties(@RequestParam String cls) {
		try {
			return ResponseEntity.ok(ontologyService.getExclusiveProperties(cls));
		} catch (ToreadorOntoEditorException e) {
			return new ResponseEntity<>(e.getData(), e.getStatus());
		}
	}

	@RequestMapping(value = "/possibleObjects", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getPossibleObjects(@RequestParam String property) {
		try {
			return ResponseEntity.ok(ontologyService.getPossibleObjects(property));
		} catch (ToreadorOntoEditorException e) {
			return new ResponseEntity<>(e.getData(), e.getStatus());
		}
	}
}
