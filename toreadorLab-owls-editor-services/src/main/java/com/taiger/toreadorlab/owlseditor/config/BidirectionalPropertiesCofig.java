package com.taiger.toreadorlab.owlseditor.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by rodrigo on 14/11/16.
 * Taiger
 */

@Configuration
@ConfigurationProperties("iconverse.ontology.bidirectional")
public class BidirectionalPropertiesCofig {
    List<String> properties;

    public List<String> getProperties() {
        return properties;
    }

    public void setProperties(List<String> properties) {
        this.properties = properties;
    }
}

