package com.taiger.toreadorlab.owlseditor.web.rest;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.taiger.toreadorlab.owlseditor.domain.DeclarativeModel;
import com.taiger.toreadorlab.owlseditor.domain.Element;
import com.taiger.toreadorlab.owlseditor.domain.declarative.ToreadorService;
import com.taiger.toreadorlab.owlseditor.domain.services.DeclarativeModelServices;
import com.taiger.toreadorlab.owlseditor.domain.workflow.ToreadorOWLSServiceResponse;
import com.taiger.toreadorlab.owlseditor.domain.workflow.ToreadorReferenceModeEnum;
import com.taiger.toreadorlab.owlseditor.domain.workflow.ToreadorWorkflow;
import com.taiger.toreadorlab.owlseditor.service.DeclarativeModelService;
import com.taiger.toreadorlab.owlseditor.service.MappingService;
import com.taiger.toreadorlab.owlseditor.service.UserService;
import com.taiger.toreadorlab.owlseditor.service.WorkflowService;
import com.taiger.toreadorlab.owlseditor.web.rest.errors.ToreadorOntoEditorException;

@RestController
@RequestMapping("/api/services")
public class ToreadorServicesController {

	private static final Log log = LogFactory.getLog(ToreadorServicesController.class);

	@Autowired
	MappingService mapperService;

	@Autowired
	DeclarativeModelService dmService;

	@Autowired
	WorkflowService wfService;

    @Autowired
    private UserService userService;




    @RequestMapping(method = RequestMethod.GET, value = "/getServiceToUser/{id_user}")
    public  ResponseEntity<?> getServiceToUser( @PathVariable long id_user) {
        log.info("/getServiceToUser"+ id_user);
        List<DeclarativeModel> dm=dmService.getUserbyId(id_user);
        return ResponseEntity.ok(dm);
    }

	@RequestMapping(method = RequestMethod.GET, value = "/obtainServicesCatalogue")
	public ResponseEntity<?> obtainServicesCatalogue() {
		log.info("/obtainServicesCatalogue");
		try {
			Integer indexedServices = mapperService.processServicesCatalogue();
			return ResponseEntity.ok(indexedServices);
		} catch (ToreadorOntoEditorException e) {
			return new ResponseEntity<>(e.getData(), e.getStatus());
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/possibleObjects")
	public ResponseEntity<?> getPossibleObjects(@RequestParam String property) {
		log.info("/possibleObjects params:[property: " + property + "]");
		try {
			List<Element> results = mapperService.getPossibleObjects(property);
			if (results != null)
				Collections.sort(results, Element.labelComparator);
			return ResponseEntity.ok(results);
		} catch (ToreadorOntoEditorException e) {
			return new ResponseEntity<>(e.getData(), e.getStatus());
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getDeclarativeModels")
	public ResponseEntity<?> getDeclarativeModels(@RequestParam String userContext) {
		log.info("/getDeclarativeModels params:[userContext: " + userContext + "]");
		try {
			List<String> declarativeModels = dmService.getDeclarativeModels(userContext);
			return ResponseEntity.ok(declarativeModels);
		} catch (ToreadorOntoEditorException e) {
			return new ResponseEntity<>(e.getData(), e.getStatus());
		}
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/deleteAllUserDeclarativeModel")
	public ResponseEntity<?> deleteAllUserDeclarativeModel(@RequestParam String userContext) {
		log.info("/deleteAllUserDeclarativeModel params:[userContext: " + userContext + "]");
		try {
			dmService.deleteAllUsersDeclarativeModel(userContext);
			return ResponseEntity.ok("All Declarative Models removed from the User context " + userContext
					+ " in Toreador Ontology repository");
		} catch (ToreadorOntoEditorException e) {
			return new ResponseEntity<>(e.getData(), e.getStatus());
		}
	}

	@RequestMapping(method = RequestMethod.PUT, consumes = "application/ld+json", value = "/updateDeclarativeModel")
	public ResponseEntity<?> updateDeclarativeModel(@RequestBody String declarativeModel,
			@RequestParam String id) {
		log.info("/updateDeclarativeModel params:[id: " + id + "]");
		try {
			ObjectMapper om = new ObjectMapper();
			LinkedHashMap dms = null;
			try {
				dms = om.readValue(declarativeModel, LinkedHashMap.class);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			DeclarativeModelServices dmId = dmService.updateDeclarativeModel(dms, id);
			return ResponseEntity.ok(dmId);
		} catch (ToreadorOntoEditorException e) {
			return new ResponseEntity<>(e.getData(), e.getStatus());
		}
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/ld+json", value = "/submitDeclarativeModel")
	public ResponseEntity<?> submitDeclarativeModel(@RequestBody String declarativeModel) throws JSONException {
		log.info("/submitDeclarativeModel params:[declarativeModels: " + declarativeModel + "]");
		DeclarativeModelServices services = new DeclarativeModelServices();

		try {
			ObjectMapper om = new ObjectMapper();
			LinkedHashMap dms = om.readValue(declarativeModel, LinkedHashMap.class);
			//dms=addRootToLinkedHashMap("Example",dms);

				DeclarativeModelServices dmId = dmService.processDecalrativeModelHashMap(dms,true);
				for (String key : dmId.getServicesByArea().keySet()) {
					Set<ToreadorService> serv = dmId.getServicesByArea(key);
					Set<ToreadorService> oldSrv = services.getServicesByArea(key);
					if (oldSrv == null)
						services.setServicesToArea(key, serv);
					else if (serv != null && oldSrv != null) {
						for (ToreadorService service : serv) {
							if (!oldSrv.contains(service)) {
								oldSrv.add(service);
							}
						}
						services.setServicesToArea(key, oldSrv);
					}
				}

			return ResponseEntity.ok(services);
		} catch (ToreadorOntoEditorException e) {
			return new ResponseEntity<>(e.getData(), e.getStatus());
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json", value = "/submitDeclarativeModels")
	public ResponseEntity<?> submitDeclarativeModels(@RequestBody String declarativeModels) {
		log.info("/submitDeclarativeModel params:[declarativeModels: " + declarativeModels + "]");
		ObjectMapper om = new ObjectMapper();
		Map<String, LinkedHashMap> dms;
		try {
			dms = om.readValue(declarativeModels, Map.class);
			DeclarativeModelServices services = new DeclarativeModelServices();
			try {
				for (String declKey : dms.keySet()) {
					LinkedHashMap string = dms.get(declKey);

					DeclarativeModelServices dmId = dmService.processDecalrativeModelHashMap(string,false);
					for (String key : dmId.getServicesByArea().keySet()) {
						Set<ToreadorService> serv = dmId.getServicesByArea(key);
						Set<ToreadorService> oldSrv = services.getServicesByArea(key);
						if (oldSrv == null)
							services.setServicesToArea(key, serv);
						else if (serv != null && oldSrv != null) {
							for (ToreadorService service : serv) {
								if (!oldSrv.contains(service)) {
									oldSrv.add(service);
								}
							}
							services.setServicesToArea(key, oldSrv);
						}
					}

				}
				return ResponseEntity.ok(services);
			} catch (ToreadorOntoEditorException e) {
				return new ResponseEntity<>(e.getData(), e.getStatus());
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;

	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/deleteDeclarativeModel")
	public ResponseEntity<?> deleteDeclarativeModel( @RequestParam String id) {
		log.info("/deleteDeclarativeModel params:[ id: " + id + "]");
		try {
			dmService.deleteDeclarativeModel(id);
			return ResponseEntity.ok("Model " + id + " Removed from the User context " + id
					+ " in Toreador Ontology repository");
		} catch (ToreadorOntoEditorException e) {
			return new ResponseEntity<>(e.getData(), e.getStatus());
		}
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json", value = "/createOWLSfromWorkflow")
	public ResponseEntity<?> createOWLSfromWorkflow(@RequestBody ToreadorWorkflow workflow, @RequestParam(required = false, name = "explicitReferences") ToreadorReferenceModeEnum explicitReferences) {
		log.info("/createOWLSfromWorkflow params:[workflow: " + workflow + "]");
		try {
			if (explicitReferences==null) explicitReferences=ToreadorReferenceModeEnum.EXPLICIT_REFERENCES;
			String owls = wfService.transformWorkflowToOWLS(workflow, explicitReferences);
			ToreadorOWLSServiceResponse response = new ToreadorOWLSServiceResponse();
			response.setData(owls);
			return ResponseEntity.ok(response);
		} catch (ToreadorOntoEditorException e) {
			return new ResponseEntity<>(e.getData(), e.getStatus());
		}
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/rdf+xml", produces = "text/plain", value = "/printWorkflow")
	public ResponseEntity<?> printOWLS(@RequestBody String owls) {
		log.info("/printWorkflow params:[owls: " + owls + "]");
		try {
			String printed = wfService.printWorkflow(owls);
			return ResponseEntity.ok(printed);
		} catch (ToreadorOntoEditorException e) {
			return new ResponseEntity<>(e.getData(), e.getStatus());
		}
	}

	private LinkedHashMap addRootToLinkedHashMap(String root,LinkedHashMap dms){
        LinkedHashMap newmap= (LinkedHashMap) dms.clone();
        dms.clear();
        dms.put(root, newmap);
	    return dms;
    }

}
