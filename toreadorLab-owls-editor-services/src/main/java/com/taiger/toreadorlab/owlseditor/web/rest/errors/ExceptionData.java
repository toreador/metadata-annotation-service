package com.taiger.toreadorlab.owlseditor.web.rest.errors;

import java.io.Serializable;

/**
 * Created by rodrigo on 14/11/16.
 * Taiger
 */
public class ExceptionData implements Serializable {

    private static final long serialVersionUID = -9079617743671564044L;
    private String message;

    private Object data;

    public ExceptionData(String message) {
        this.message = message;
    }

    public ExceptionData(String message, Object data) {
        this.data = data;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
