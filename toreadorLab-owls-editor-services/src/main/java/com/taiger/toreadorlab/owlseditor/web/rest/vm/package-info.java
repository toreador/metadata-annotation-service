/**
 * View Models used by Spring MVC REST controllers.
 */
package com.taiger.toreadorlab.owlseditor.web.rest.vm;
