package com.taiger.toreadorlab.owlseditor.domain;

import java.io.Serializable;

public class Parameter implements Serializable {
	/**
		 *
		 */
	private static final long serialVersionUID = -2032262687445824819L;
	private String id;
	private String description;
	private boolean mandatory;
	private String defaultValue;
	private String key;
	private String type;
	private String value;
    private Boolean inputData;
    private Boolean outputData;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

    public Boolean getInputData() {
        return inputData;
    }

    public void setInputData(Boolean inputData) {
        this.inputData = inputData;
    }

    public Boolean getOutputData() {
        return outputData;
    }

    public void setOutputData(Boolean outputData) {
        this.outputData = outputData;
    }

    @Override
	public String toString() {
		return "{ \"description\": \"" + description + "\"\n" + "\"mandatory\": " + mandatory + "\n"
				+ "\"defaultValue\": \"" + defaultValue + "\"\n" + "\"value\": " + value + "\n" + "\"key\": \"" + key + "\"\n" + "\"type\": \""
				+ type + "\"\n}";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((defaultValue == null) ? 0 : defaultValue.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + (mandatory ? 1231 : 1237);
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parameter other = (Parameter) obj;
		if (defaultValue == null) {
			if (other.defaultValue != null)
				return false;
		} else if (!defaultValue.equals(other.defaultValue))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (mandatory != other.mandatory)
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
}
