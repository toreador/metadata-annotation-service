package com.taiger.toreadorlab.owlseditor.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.taiger.toreadorlab.owlseditor.domain.DeclarativeModel;
import com.taiger.toreadorlab.owlseditor.domain.declarative.DataAnalytics;
import com.taiger.toreadorlab.owlseditor.domain.declarative.DataIntegration;
import com.taiger.toreadorlab.owlseditor.domain.declarative.DataPreparation;
import com.taiger.toreadorlab.owlseditor.domain.declarative.DataProcessing;
import com.taiger.toreadorlab.owlseditor.domain.declarative.DataRepresentation;
import com.taiger.toreadorlab.owlseditor.domain.declarative.DataVisualization;
import com.taiger.toreadorlab.owlseditor.domain.declarative.Feature;
import com.taiger.toreadorlab.owlseditor.domain.declarative.Goal;
import com.taiger.toreadorlab.owlseditor.domain.declarative.Indicator;
import com.taiger.toreadorlab.owlseditor.domain.declarative.Objective;
import com.taiger.toreadorlab.owlseditor.domain.declarative.Person;
import com.taiger.toreadorlab.owlseditor.domain.declarative.ProjectSpecification;
import com.taiger.toreadorlab.owlseditor.domain.declarative.ToreadorDeclarativeModel;
import com.taiger.toreadorlab.owlseditor.domain.declarative.ToreadorService;
import com.taiger.toreadorlab.owlseditor.domain.services.DeclarativeModelServices;
import com.taiger.toreadorlab.owlseditor.repository.DeclarativeModelRepository;
import com.taiger.toreadorlab.owlseditor.web.rest.errors.ToreadorOntoEditorException;

/**
 * Service Implementation for managing DeclarativeModel.
 */
@Service
@Transactional
public class DeclarativeModelService {

	private final Logger log = LoggerFactory.getLogger(DeclarativeModelService.class);

	private final DeclarativeModelRepository declarativeModelRepository;

	@Autowired
	OntologyService ontologyService;

	@Value("${iconverse.ontology.atosDeploymentServiceEndpoint}")
	private String atosDeployerEndpoint;

	@Value("${iconverse.ontology.baseURI}")
	private String baseURI;

	public DeclarativeModelService(DeclarativeModelRepository declarativeModelRepository) {
		this.declarativeModelRepository = declarativeModelRepository;
	}

	/**
	 * Save a declarativeModel.
	 *
	 * @param declarativeModel
	 *            the entity to save
	 * @return the persisted entity
	 */
	public DeclarativeModel save(DeclarativeModel declarativeModel) {
		log.debug("Request to save DeclarativeModel : {}", declarativeModel);
		return declarativeModelRepository.save(declarativeModel);
	}

	/**
	 * Get all the declarativeModels.
	 *
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<DeclarativeModel> findAll() {
		log.debug("Request to get all DeclarativeModels");
		return declarativeModelRepository.findAll();
	}

	/**
	 * Get one declarativeModel by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public DeclarativeModel findOne(Long id) {
		log.debug("Request to get DeclarativeModel : {}", id);
		return declarativeModelRepository.findOne(id);
	}

	/**
	 * Delete the declarativeModel by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete DeclarativeModel : {}", id);
		declarativeModelRepository.delete(id);
	}

	/*
	 * From the other old project
	 */

	public boolean deleteAllUsersDeclarativeModel(String context) throws ToreadorOntoEditorException {
		return ontologyService.deleteGraph(context);
	}

	/*
	 * public DeclarativeModel assignServiceToUser(User user, String graph_uri){
	 * DeclarativeModel dm = new DeclarativeModel(); dm.setGraphUri(graph_uri);
	 * dm.setUser(user); declarativeModelRepository.save(dm); return dm; }
	 */

	public List<DeclarativeModel> getUserbyId(long id_user) {

		List<DeclarativeModel> dm = declarativeModelRepository.findByUserId(id_user);

		return dm;
	}

	public DeclarativeModelServices processDecalrativeModelHashMap(LinkedHashMap dm, Boolean single)
			throws ToreadorOntoEditorException {
		log.warn("processDecalrativeModelHashMap: " + dm.toString());
		DeclarativeModelServices results = new DeclarativeModelServices();
		ToreadorDeclarativeModel declMod = transformToDeclarativeModel(dm, single);

		Set<ToreadorService> analy = new HashSet<>();
		Set<ToreadorService> prep = new HashSet<>();
		Set<ToreadorService> repre = new HashSet<>();
		Set<ToreadorService> proce = new HashSet<>();
		Set<ToreadorService> visu = new HashSet<>();
		Set<ToreadorService> inte = new HashSet<>();
		try {
			for (String s : declMod.getDataAnalytics().getCompactRepresentation()) {
				if (s != null) {
					log.info("Constrains: " + declMod.getDataAnalytics().getConstrains());
					log.info("Id: " + declMod.getDataAnalytics().getId());
					Set<ToreadorService> aux = ontologyService.extractService(s,
							declMod.getDataAnalytics().getConstrains(), declMod.getDataAnalytics().getId());
					System.out.println("Analytics: " + s);
					// results.addServicesToArea(declMod.getDataAnalytics().getLabel(),);
					if (aux != null) {
						for (ToreadorService se : aux) {
							if (se.getService()!=null && se.getService().getArea() != null) {
								if (se.getService().getArea().name().equalsIgnoreCase("analytics")) {
									addElement(analy, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Preparation")) {
									addElement(prep, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Representation")) {
									addElement(repre, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Processing")) {
									addElement(proce, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Visualization")) {
									addElement(visu, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Integration")) {
									addElement(inte, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Ingestion")) {
									addElement(inte, se);
								}
							}
						}
					}
				}

			}

			for (String s : declMod.getDataPreparation().getCompactRepresentation()) {
				System.out.println("Preparation: " + s);
				if (s != null) {
					Set<ToreadorService> aux = ontologyService.extractService(s,
							declMod.getDataPreparation().getConstrains(), declMod.getDataPreparation().getId());
					if (aux != null) {
						for (ToreadorService se : aux) {
							if (se.getService()!=null && se.getService().getArea() != null) {
								if (se.getService().getArea().name().equalsIgnoreCase("analytics")) {
									addElement(analy, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Preparation")) {
									addElement(prep, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Representation")) {
									addElement(repre, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Processing")) {
									addElement(proce, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Visualization")) {
									addElement(visu, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Integration")) {
									addElement(inte, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Ingestion")) {
									addElement(inte, se);
								}
							}
						}
					}
				}
			}
			for (String s : declMod.getDataRepresentation().getCompactRepresentation()) {
				System.out.println("Representation: " + s);
				if (s != null) {
					Set<ToreadorService> aux = ontologyService.extractService(s,
							declMod.getDataRepresentation().getConstrains(), declMod.getDataRepresentation().getId());
					if (aux != null) {
						for (ToreadorService se : aux) {
							if (se.getService()!=null && se.getService().getArea() != null) {
								if (se.getService().getArea().name().equalsIgnoreCase("analytics")) {
									addElement(analy, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Preparation")) {
									addElement(prep, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Representation")) {
									addElement(repre, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Processing")) {
									addElement(proce, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Visualization")) {
									addElement(visu, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Integration")) {
									addElement(inte, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Ingestion")) {
									addElement(inte, se);
								}
							}
						}
					}
				}
			}
			for (String s : declMod.getDataProcessing().getCompactRepresentation()) {
				System.out.println("Processing: " + s);
				if (s != null) {
					Set<ToreadorService> aux = ontologyService.extractService(s,
							declMod.getDataProcessing().getConstrains(), declMod.getDataProcessing().getId());
					if (aux != null) {
						for (ToreadorService se : aux) {
							if (se.getService()!=null && se.getService().getArea() != null) {
								if (se.getService().getArea().name().equalsIgnoreCase("analytics")) {
									addElement(analy, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Preparation")) {
									addElement(prep, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Representation")) {
									addElement(repre, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Processing")) {
									addElement(proce, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Visualization")) {
									addElement(visu, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Integration")) {
									addElement(inte, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Ingestion")) {
									addElement(inte, se);
								}
							}
						}
					}
				}
			}
			for (String s : declMod.getDataVisualization().getCompactRepresentation()) {
				System.out.println("Visualization: " + s);
				if (s != null) {
					Set<ToreadorService> aux = ontologyService.extractService(s,
							declMod.getDataVisualization().getConstrains(), declMod.getDataVisualization().getId());
					if (aux != null) {
						for (ToreadorService se : aux) {
							if (se.getService()!=null && se.getService().getArea() != null) {
								if (se.getService().getArea().name().equalsIgnoreCase("analytics")) {
									addElement(analy, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Preparation")) {
									addElement(prep, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Representation")) {
									addElement(repre, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Processing")) {
									addElement(proce, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Visualization")) {
									addElement(visu, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Integration")) {
									addElement(inte, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Ingestion")) {
									addElement(inte, se);
								}
							}
						}
					}
				}
			}
			///
			for (String s : declMod.getDataIntegration().getCompactRepresentation()) {
				System.out.println("Integration: " + s);
				if (s != null) {
					Set<ToreadorService> aux = ontologyService.extractService(s,
							declMod.getDataIntegration().getConstrains(), declMod.getDataIntegration().getLabel());
					if (aux != null) {
						for (ToreadorService se : aux) {
							if (se.getService()!=null && se.getService().getArea() != null) {
								if (se.getService().getArea().name().equalsIgnoreCase("analytics")) {
									addElement(analy, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Preparation")) {
									addElement(prep, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Representation")) {
									addElement(repre, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Processing")) {
									addElement(proce, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Visualization")) {
									addElement(visu, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Integration")) {
									addElement(inte, se);
								}
								if (se.getService().getArea().name().equalsIgnoreCase("Ingestion")) {
									addElement(inte, se);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("ERROR: " + e.getMessage());
		}
		///

		results.addServicesToArea(declMod.getDataAnalytics().getLabel(), analy);
		results.addServicesToArea(declMod.getDataPreparation().getLabel(), prep);
		results.addServicesToArea(declMod.getDataRepresentation().getLabel(), repre);
		results.addServicesToArea(declMod.getDataProcessing().getLabel(), proce);
		results.addServicesToArea(declMod.getDataVisualization().getLabel(), visu);
		results.addServicesToArea(declMod.getDataIntegration().getLabel(), inte);
		return results;
	}

	private void addElement(Set<ToreadorService> v, ToreadorService se) {
		if (v != null && v.isEmpty()) {
			log.info("is empty: " + se.getService().getId());
			v.add(se);
		} else if (v != null && !v.isEmpty()) {
			boolean duplicate = false;
			for (ToreadorService toreadorService : v) {
				if (toreadorService.getService().getId().equals(se.getService().getId())) {
					log.info("duplicated: " + se.getService().getId());
					duplicate = true;
					break;
				}
			}
			if (!duplicate && se != null) {
				log.info("not duplicated: " + se.getService().getId());
				v.add(se);
			}
		}
	}

	private ToreadorDeclarativeModel transformToDeclarativeModel(LinkedHashMap dm, Boolean single) {
		ToreadorDeclarativeModel declarativeModel = new ToreadorDeclarativeModel();
		log.info("TRANSFORM TO DECLARATIVE MODEL");
		// log.info(String.valueOf(dm.get("Example")));

		LinkedHashMap mapBody;
		if (single) {
			Set<String> sourceSet = Sets.newHashSet(dm.keySet());
			List<String> targetList = new ArrayList<String>(sourceSet);
			log.info(targetList.get(0));

			mapBody = (LinkedHashMap) dm.get(targetList.get(0));
			log.info(dm.keySet().toString());
			log.info(String.valueOf(mapBody.get("@id")));
		} else {
			mapBody = dm;
		}

		declarativeModel.setId((String) dm.get("@id"));
		declarativeModel.setContexts((Map<String, String>) dm.get("@context"));
		declarativeModel
				.setDataRepresentation(extractRepresentation((LinkedHashMap) mapBody.get("tdm:representation")));
		declarativeModel.setDataPreparation(extractPreparation((LinkedHashMap) mapBody.get("tdm:preparation")));
		declarativeModel.setDataProcessing(extractProcessing((LinkedHashMap) mapBody.get("tdm:processing")));
		declarativeModel.setDataAnalytics(extractAnalytics((LinkedHashMap) mapBody.get("tdm:analytics")));
		declarativeModel.setDataVisualization(extractVisualization((LinkedHashMap) mapBody.get("tdm:display")));
		///
		declarativeModel.setDataIntegration(extractIntegration((LinkedHashMap) mapBody.get("tdm:integration")));
		///
		declarativeModel.setProjectSpecification(extractAnagraphic(mapBody));
		return declarativeModel;
	}

	///
	private DataIntegration extractIntegration(LinkedHashMap linkedHashMap) {
		DataIntegration dataIntegration = new DataIntegration();
		dataIntegration.setId((String) linkedHashMap.get("@id"));
		dataIntegration.setType((String) linkedHashMap.get("@type"));
		dataIntegration.setLabel((String) linkedHashMap.get("tdm:label"));
		dataIntegration.setIncorporates(extractGoals((List<LinkedHashMap>) linkedHashMap.get("tdm:incorporates")));
		return dataIntegration;
	}
	///

	private DataVisualization extractVisualization(LinkedHashMap linkedHashMap) {
		DataVisualization dataVisualization = new DataVisualization();
		dataVisualization.setId((String) linkedHashMap.get("@id"));
		dataVisualization.setType((String) linkedHashMap.get("@type"));
		dataVisualization.setLabel((String) linkedHashMap.get("tdm:label"));
		dataVisualization
				.setIncorporates(extractGoalsOrFeatures((List<LinkedHashMap>) linkedHashMap.get("tdm:incorporates")));
		return dataVisualization;
	}

	private DataAnalytics extractAnalytics(LinkedHashMap linkedHashMap) {
		DataAnalytics dataAnalytics = new DataAnalytics();
		dataAnalytics.setId((String) linkedHashMap.get("@id"));
		dataAnalytics.setType((String) linkedHashMap.get("@type"));
		dataAnalytics.setLabel((String) linkedHashMap.get("tdm:label"));
		dataAnalytics.setIncorporates(extractGoals((List<LinkedHashMap>) linkedHashMap.get("tdm:incorporates")));
		return dataAnalytics;
	}

	private DataProcessing extractProcessing(LinkedHashMap linkedHashMap) {
		DataProcessing dataProcessing = new DataProcessing();
		dataProcessing.setId((String) linkedHashMap.get("@id"));
		dataProcessing.setType((String) linkedHashMap.get("@type"));
		dataProcessing.setLabel((String) linkedHashMap.get("tdm:label"));
		dataProcessing.setIncorporates(extractGoals((List<LinkedHashMap>) linkedHashMap.get("tdm:incorporates")));
		return dataProcessing;
	}

	private DataPreparation extractPreparation(LinkedHashMap linkedHashMap) {
		DataPreparation dataPreparation = new DataPreparation();
		dataPreparation.setId((String) linkedHashMap.get("@id"));
		dataPreparation.setType((String) linkedHashMap.get("@type"));
		dataPreparation.setLabel((String) linkedHashMap.get("tdm:label"));
		dataPreparation.setIncorporates(extractGoals((List<LinkedHashMap>) linkedHashMap.get("tdm:incorporates")));
		return dataPreparation;
	}

	private DataRepresentation extractRepresentation(LinkedHashMap linkedHashMap) {
		DataRepresentation representation = new DataRepresentation();
		representation.setId((String) linkedHashMap.get("@id"));
		representation.setType((String) linkedHashMap.get("@type"));
		representation.setLabel((String) linkedHashMap.get("tdm:label"));
		representation.setIncorporates(extractFeatures((List<LinkedHashMap>) linkedHashMap.get("tdm:incorporates")));
		return representation;
	}

	private List<Goal> extractGoals(List<LinkedHashMap> linkedHashMaps) {
		List<Goal> goals = new ArrayList<>();
		for (LinkedHashMap linkedHashMap : linkedHashMaps) {
			Goal goal = new Goal();
			goal.setConstraint(
					linkedHashMap.get("tdm:constraint") != null ? linkedHashMap.get("tdm:constraint").toString() : "");
			goal.setLabel((String) linkedHashMap.get("tdm:label"));
			goal.setType((String) linkedHashMap.get("@type"));
			List<LinkedHashMap> maps = (List<LinkedHashMap>) linkedHashMap.get("tdm:incorporates");
			if (maps != null && !maps.isEmpty())
				goal.setIncorporates(extractIndicators(maps));
			goals.add(goal);
		}
		return goals;
	}

	private List<Indicator> extractIndicators(List<LinkedHashMap> linkedHashMaps) {
		List<Indicator> indicators = new ArrayList<>();
		for (LinkedHashMap linkedHashMap : linkedHashMaps) {
			Indicator indicator = new Indicator();
			indicator.setConstraint(
					linkedHashMap.get("tdm:constraint") != null ? linkedHashMap.get("tdm:constraint").toString() : "");
			indicator.setLabel((String) linkedHashMap.get("tdm:label"));
			indicator.setType((String) linkedHashMap.get("@type"));
			indicator.setVisualizationType((String) linkedHashMap.get("tdm:visualisationType"));
			List<LinkedHashMap> maps = (List<LinkedHashMap>) linkedHashMap.get("tdm:incorporates");
			if (maps != null && !maps.isEmpty())
				indicator.setIncorporates(extractFeaturesOrObjective(maps));
			indicators.add(indicator);
		}
		return indicators;
	}

	private List<Objective> extractGoalsOrFeatures(List<LinkedHashMap> linkedHashMaps) {
		List<Objective> features = new ArrayList<>();
		for (LinkedHashMap linkedHashMap : linkedHashMaps) {
			String type = (String) linkedHashMap.get("@type");
			log.info(type);
			if (type.trim().equals("tdm:Goal")) {
				Goal goal = new Goal();
				goal.setConstraint(linkedHashMap.get("tdm:constraint") != null
						? linkedHashMap.get("tdm:constraint").toString() : "");
				goal.setLabel((String) linkedHashMap.get("tdm:label"));
				goal.setType(type);
				List<LinkedHashMap> maps = (List<LinkedHashMap>) linkedHashMap.get("tdm:incorporates");
				if (maps != null && !maps.isEmpty())
					goal.setIncorporates(extractIndicators(maps));
				features.add(goal);
			} else {
				Feature feature = new Feature();
				feature.setConstraint(linkedHashMap.get("tdm:constraint") != null
						? linkedHashMap.get("tdm:constraint").toString() : "");
				feature.setLabel((String) linkedHashMap.get("tdm:label"));
				feature.setType(type);
				List<LinkedHashMap> maps = (List<LinkedHashMap>) linkedHashMap.get("tdm:incorporates");
				if (maps != null && !maps.isEmpty())
					feature.setIncorporates(extractFeaturesOrObjective(maps));
				features.add(feature);
			}
		}
		return features;
	}

	private List<Feature> extractFeatures(List<LinkedHashMap> linkedHashMaps) {
		List<Feature> features = new ArrayList<>();
		for (LinkedHashMap linkedHashMap : linkedHashMaps) {
			Feature feature = new Feature();
			feature.setConstraint(
					linkedHashMap.get("tdm:constraint") != null ? linkedHashMap.get("tdm:constraint").toString() : "");
			feature.setLabel((String) linkedHashMap.get("tdm:label"));
			feature.setType((String) linkedHashMap.get("@type"));
			List<LinkedHashMap> maps = (List<LinkedHashMap>) linkedHashMap.get("tdm:incorporates");
			if (maps != null && !maps.isEmpty())
				feature.setIncorporates(extractFeaturesOrObjective(maps));
			features.add(feature);
		}
		return features;
	}

	private List<Objective> extractFeaturesOrObjective(List<LinkedHashMap> linkedHashMaps) {
		List<Objective> features = new ArrayList<>();
		for (LinkedHashMap linkedHashMap : linkedHashMaps) {
			String type = (String) linkedHashMap.get("@type");
			log.info(type);
			if (type.trim().equals("tdm:Objective")) {
				Objective objective = new Objective();
				objective.setType(type);
				objective.setConstraint(linkedHashMap.get("tdm:constraint") != null
						? linkedHashMap.get("tdm:constraint").toString() : "");
				objective.setLabel((String) linkedHashMap.get("tdm:label"));
				features.add(objective);
			} else {
				Feature feature = new Feature();
				feature.setConstraint(linkedHashMap.get("tdm:constraint") != null
						? linkedHashMap.get("tdm:constraint").toString() : "");
				feature.setLabel((String) linkedHashMap.get("tdm:label"));
				feature.setType(type);
				List<LinkedHashMap> maps = (List<LinkedHashMap>) linkedHashMap.get("tdm:incorporates");
				if (maps != null && !maps.isEmpty())
					feature.setIncorporates(extractFeaturesOrObjective(maps));
				features.add(feature);
			}
		}
		return features;
	}

	private ProjectSpecification extractAnagraphic(LinkedHashMap linkedHashMap) {
		ProjectSpecification projectSpecification = new ProjectSpecification();
		if (linkedHashMap != null && !linkedHashMap.isEmpty()) {
			// datasources
			List<String> datasources = (List<String>) linkedHashMap.get("tdm:targetDataSources");
			if (datasources != null && !datasources.isEmpty()) {
				for (String s : datasources) {
					if (!Strings.isNullOrEmpty(s.trim()))
						projectSpecification.addTargetDataSource(s);
				}
			}
			// Id
			String id = (String) linkedHashMap.get("@id");
			if (id != null)
				projectSpecification.setIdentifier(id);
			// Anagraphics
			LinkedHashMap anagraphics = (LinkedHashMap) linkedHashMap.get("tdm:anagraphic");
			Person person = new Person();
			person.setFamilyName((String) anagraphics.get("tdm:firstname"));
			person.setGivenName((String) anagraphics.get("tdm:lastname"));
			person.setAffiliation((String) anagraphics.get("tdm:affiliation"));
			person.setRole((String) anagraphics.get("tdm:role"));
			projectSpecification.addAdditionalType((String) anagraphics.get("tdm:configurationName"));
			projectSpecification.setAuthor(person);
			projectSpecification.setDescription((String) anagraphics.get("tdm:project_description"));
			projectSpecification.setTitle((String) anagraphics.get("tdm:project_title"));
		}
		return projectSpecification;
	}

	public DeclarativeModelServices updateDeclarativeModel(LinkedHashMap decalrativeModel, String id)
			throws ToreadorOntoEditorException {
		deleteDeclarativeModel(id);
		return processDecalrativeModelHashMap(decalrativeModel, true);
	}

	public boolean deleteDeclarativeModel(String id) throws ToreadorOntoEditorException {
		return false;
		// TODO implement this
	}

	// method that replaces every bnode in the graph. Bnodes may give some
	// problems at the moment of querying
	private Model replaceBnodes(Model rdfModel, String id) {
		IRI context = SimpleValueFactory.getInstance().createIRI(id);
		Model newModel = new LinkedHashModel();
		Map<BNode, IRI> mapping = new HashMap<>();
		rdfModel.forEach(new Consumer<Statement>() {
			@Override
			public void accept(Statement t) {
				IRI subject = null;
				IRI object = null;
				if (t.getSubject() instanceof BNode) {
					subject = mapping.get(t.getSubject());
					if (subject == null) {
						subject = SimpleValueFactory.getInstance().createIRI(id);
						mapping.put((BNode) t.getSubject(), subject);
					}
				}
				if (t.getObject() instanceof BNode) {
					object = mapping.get(t.getObject());
					if (object == null) {
						object = SimpleValueFactory.getInstance().createIRI(id);
						mapping.put((BNode) t.getObject(), object);
					}
				}
				if (subject != null && object != null) {
					newModel.add(subject, t.getPredicate(), object, context);
				} else if (subject != null && object == null) {
					newModel.add(subject, t.getPredicate(), t.getObject(), context);

				} else if (subject == null && object != null) {
					newModel.add(t.getSubject(), t.getPredicate(), object, context);
				} else {
					newModel.add(t.getSubject(), t.getPredicate(), t.getObject(), context);
				}
			}
		});
		return newModel;
	}

	public List<String> getDeclarativeModels(String userContext) throws ToreadorOntoEditorException {
		return ontologyService.getAllContextsInGraph(userContext);
	}
}
