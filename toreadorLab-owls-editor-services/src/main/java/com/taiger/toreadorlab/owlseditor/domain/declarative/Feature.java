package com.taiger.toreadorlab.owlseditor.domain.declarative;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Feature extends Incorporates<Objective> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Objective> incorporates = new ArrayList<>();
	@Override

	public List<Objective> getIncorporates() {
		return incorporates;
	}
	@Override

	public void setIncorporates(List<Objective> incorporates) {
		this.incorporates = incorporates;
	}
	@Override
	public void addIncorporate(Objective feature){
		if (this.incorporates==null) this.incorporates= new ArrayList<>();
		this.incorporates.add(feature);
	}

	public List<String> getCompactRepresentation() {
		List<String> result = new ArrayList<>();
		String entry = this.getLabel()==null?"":this.getLabel().replaceAll(" ", "_");
		if (this.incorporates != null && !incorporates.isEmpty()) {
			for (Objective indicator : incorporates) {

				List<String> compactIndie = indicator.getCompactRepresentation();
				for (String en : compactIndie) {
					result.add(entry + "."+en);				}
			}
			return result;
		} else
			return Arrays.asList(entry);	}
	
}
