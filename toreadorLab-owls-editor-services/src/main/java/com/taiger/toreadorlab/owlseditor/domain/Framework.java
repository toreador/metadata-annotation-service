package com.taiger.toreadorlab.owlseditor.domain;

import java.io.Serializable;

public class Framework implements Serializable {
/**
	 * 
	 */
	private static final long serialVersionUID = -4762927608033125814L;
	private String id;
private String name;
private String version;
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getVersion() {
	return version;
}
public void setVersion(String version) {
	this.version = version;
}
@Override
public String toString() {
	return "{ \"name\": \""+name+"\"\n"+
			" \"version\": \""+version+"\"\n}";
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
}
