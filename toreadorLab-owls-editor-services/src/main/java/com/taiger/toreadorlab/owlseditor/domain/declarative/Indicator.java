package com.taiger.toreadorlab.owlseditor.domain.declarative;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Indicator extends Incorporates<Objective> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Objective> incorporates = new ArrayList<>();;
	private String visualizationType;
	
	public List<Objective> getIncorporates() {
		return incorporates;
	}
	public void setIncorporates(List<Objective> incorporates) {
		this.incorporates = incorporates;
	}
	
	public String getVisualizationType() {
		return visualizationType;
	}
	public void setVisualizationType(String visualizationType) {
		this.visualizationType = visualizationType;
	}
	@Override
	public void addIncorporate(Objective incorporate) {
		if (this.incorporates==null) this.incorporates= new ArrayList<>();
		this.incorporates.add(incorporate);
		
	}
	public List<String> getCompactRepresentation() {
		List<String> result = new ArrayList<>();
		String entry = this.getLabel()==null?"":this.getLabel().replaceAll(" ", "_");
		if (this.incorporates != null && !incorporates.isEmpty()) {
			for (Objective indicator : incorporates) {
				List<String> compactIndie = indicator.getCompactRepresentation();
				for (String en : compactIndie) {
					result.add(entry + "."+en);				}
			}
			return result;
		} else
			return Arrays.asList(entry);	}

}
