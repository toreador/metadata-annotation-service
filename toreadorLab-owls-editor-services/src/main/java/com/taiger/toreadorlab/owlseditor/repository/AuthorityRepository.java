package com.taiger.toreadorlab.owlseditor.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.taiger.toreadorlab.owlseditor.domain.Authority;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
