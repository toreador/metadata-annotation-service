package com.taiger.toreadorlab.owlseditor.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Element implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String uri;
    private String label;
    private String graph;
    private Element type;
    private Set<Element> types;
    private Set<String> aliases;
    private HashMap<String, Object> properties;

    private static final ObjectMapper mapper = new ObjectMapper();

    public Element() {
    }

    public Element(String id, String graph) {
        this.id = id;
    }

    public Element(String id, String label, String graph) {
        this.id = id;
        this.label = label;
        this.graph = graph;
    }

    public Element(String id, String uri, String label, String graph) {
        this.id = id;
        this.uri = uri;
        this.label = label;
        this.graph = graph;
    }

    public Element(String id, String label, Element type, String graph) {
        this.id = id;
        this.label = label;
        this.graph = graph;
        this.setType(type);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Element getType() {
        return type;
    }

    public void setType(Element type) {
        this.type = type;
    }

    public Set<String> getAliases() {
        return aliases;
    }

    public void setAliases(Set<String> aliases) {
        this.aliases = aliases;
    }

    @Override
    public String toString() {
        return "Element{" +
                "id='" + id + '\'' +
                ", uri='" + uri + '\'' +
                ", graph='" + graph + '\'' +
                ", label='" + label + '\'' +
                ", type='" + type + '\'' +
                ", aliases=" + aliases +
                '}';
    }

    public Set<Element> getTypes() {
        return types;
    }

    public void setTypes(Set<Element> types) {
        this.types = types;
    }

    public Set<String> getTypesIds() {
        Set<String> typeIds = new HashSet<String>();
        if (types != null) {
            for (Element type : types) {
                typeIds.add(type.id);
            }
        }
        return typeIds;
    }

    public void makeTypes(Set<String> typeIds, String context) {
        types = new HashSet<Element>();
        if (types != null) {
            for (String typeId : typeIds) {
                types.add(new Element(typeId, context));
            }
        }
    }

    public String getTypesString() {
        if (types != null) {
            List<String> typeIds = new ArrayList<String>();
            for (Element type : types) {
                typeIds.add(type.getId());
            }
            return StringUtils.join(typeIds, ",");
        }
        return null;
    }

    public void setTypesFromString(String typeIds, String context) {
        makeTypes(new HashSet<String>(Arrays.asList(typeIds.split(","))), context);
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Element) {
            return uri.equals(((Element) obj).getUri());
        } else {
            return false;
        }
    }

    public HashMap<String, Object> getProperties() {
        return properties;
    }

    public void setProperties(HashMap<String, Object> properties) {
        this.properties = properties;
    }

    public void addProperty(String property, Object value, String graph) {
        if (properties == null) {
            properties = new HashMap<String, Object>();
        }
        if (graph!=null) this.graph = graph;
        properties.put(property, value);
    }

    public Object getProperty(String key) {
        if (properties != null) {
            return properties.get(key);
        }
        return null;
    }

    public String pretty() {
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
        }
        return this.toString();
    }

    public static boolean containsElement(Set<Element> elements, Element element) {
        for (Element e : elements) {
            if (e.getUri().equals(element.getUri())) {
                return true;
            }
        }
        return false;
    }

    public String getGraph() {
		return graph;
	}

	public void setGraph(String graph) {
		this.graph = graph;
	}

	public static final Comparator<Element> labelComparator = new Comparator<Element>() {
        @Override
        public int compare(Element e1, Element e2) {
        	if (e1.getLabel()==null) return e1.getId().compareTo(e2.getId());
        	else return e1.getLabel().compareTo(e2.getLabel());
        }
    };
}
