package com.taiger.toreadorlab.owlseditor.domain.declarative;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ToreadorDeclarativeModel implements Serializable {

	/**
	 * Class used to define a person
	 */
	private static final long serialVersionUID = 2814330402061054923L;
	private ProjectSpecification projectSpecification;
	private DataPreparation dataPreparation;
	private DataRepresentation dataRepresentation;
	private DataAnalytics dataAnalytics;
	private DataProcessing dataProcessing;
	private DataVisualization dataVisualization;
	///
	private DataIntegration dataIntegration;
	///
	private String id;
	private Map<String,String> contexts;

	public ProjectSpecification getProjectSpecification() {
		return projectSpecification;
	}

	public void setProjectSpecification(ProjectSpecification projectSpecification) {
		this.projectSpecification = projectSpecification;
	}

	public DataPreparation getDataPreparation() {
		return dataPreparation;
	}

	public void setDataPreparation(DataPreparation dataPreparation) {
		this.dataPreparation = dataPreparation;
	}

	public DataRepresentation getDataRepresentation() {
		return dataRepresentation;
	}

	public void setDataRepresentation(DataRepresentation dataRepresentation) {
		this.dataRepresentation = dataRepresentation;
	}

	public DataAnalytics getDataAnalytics() {
		return dataAnalytics;
	}

	public void setDataAnalytics(DataAnalytics dataAnalytics) {
		this.dataAnalytics = dataAnalytics;
	}

	public DataProcessing getDataProcessing() {
		return dataProcessing;
	}

	public void setDataProcessing(DataProcessing dataProcessing) {
		this.dataProcessing = dataProcessing;
	}

	public DataVisualization getDataVisualization() {
		return dataVisualization;
	}

	public void setDataVisualization(DataVisualization dataVisualization) {
		this.dataVisualization = dataVisualization;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<String,String> getContexts() {
		return contexts;
	}

	public void setContexts(Map<String,String> contexts) {
		this.contexts = contexts;
	}

	public String getContext(String key){
		return this.contexts.get(key);
	}

	public void addToContexts(String key, String value){
		if (this.contexts == null) this.contexts = new HashMap<>();
		this.contexts.put(key, value);
	}

    public DataIntegration getDataIntegration() {
        return dataIntegration;
    }

    public void setDataIntegration(DataIntegration dataIntegration) {
        this.dataIntegration = dataIntegration;
    }
}
