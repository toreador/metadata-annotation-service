package com.taiger.toreadorlab.owlseditor.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "area",
    "name",
    "metrics",
    "mode",
    "version",
    "description",
    "framework",
    "properties",
    "url"
})
public class ServiceDescription implements Serializable {
	/**
	 * DTO of the service description as it comes from ENG
	 */




        @JsonProperty("id")
        private Integer id;
        @JsonProperty("area")
        private String area;
        @JsonProperty("name")
        private String name;
        @JsonProperty("metrics")
        private List<Object> metrics = null;
        @JsonProperty("mode")
        private String mode;
        @JsonProperty("version")
        private String version;
        @JsonProperty("description")
        private String description;
        @JsonProperty("framework")
        private Framework framework;
        @JsonProperty("properties")
        private List<Properties> properties = null;
        @JsonProperty("url")
        private String url;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("id")
        public Integer getId() {
            return id;
        }

        @JsonProperty("id")
        public void setId(Integer id) {
            this.id = id;
        }

        @JsonProperty("area")
        public String getArea() {
            return area;
        }

        @JsonProperty("area")
        public void setArea(String area) {
            this.area = area;
        }

        @JsonProperty("name")
        public String getName() {
            return name;
        }

        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        @JsonProperty("metrics")
        public List<Object> getMetrics() {
            return metrics;
        }

        @JsonProperty("metrics")
        public void setMetrics(List<Object> metrics) {
            this.metrics = metrics;
        }

        @JsonProperty("mode")
        public String getMode() {
            return mode;
        }

        @JsonProperty("mode")
        public void setMode(String mode) {
            this.mode = mode;
        }

        @JsonProperty("version")
        public String getVersion() {
            return version;
        }

        @JsonProperty("version")
        public void setVersion(String version) {
            this.version = version;
        }

        @JsonProperty("description")
        public String getDescription() {
            return description;
        }

        @JsonProperty("description")
        public void setDescription(String description) {
            this.description = description;
        }

        @JsonProperty("framework")
        public Framework getFramework() {
            return framework;
        }

        @JsonProperty("framework")
        public void setFramework(Framework framework) {
            this.framework = framework;
        }

        @JsonProperty("properties")
        public List<Properties> getProperties() {
            return properties;
        }

        @JsonProperty("properties")
        public void setProperties(List<Properties> properties) {
            this.properties = properties;
        }

        @JsonProperty("url")
        public String getUrl() {
            return url;
        }

        @JsonProperty("url")
        public void setUrl(String url) {
            this.url = url;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }


}
