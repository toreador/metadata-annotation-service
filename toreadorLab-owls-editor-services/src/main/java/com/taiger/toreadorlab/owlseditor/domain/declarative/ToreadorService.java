package com.taiger.toreadorlab.owlseditor.domain.declarative;

import java.io.Serializable;

import it.eng.toreador.catalogue.Service;

public class ToreadorService implements Serializable,  Comparable<ToreadorService>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2717129553703426070L;
	private String owl;
	private it.eng.toreador.catalogue.Service service;

	public String getOwl() {
		return owl;
	}

	public void setOwl(String owl) {
		this.owl = owl;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	@Override
	public int hashCode() {
		return service.getId().intValue();
	}

	@Override
	public boolean equals(Object obj) {
		return this.service.getId() == ((ToreadorService) obj).getService().getId();
	}


	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ToreadorService o) {
		if(this.service.getId() == o.getService().getId())  return 0 ; 
	    return 1;
	}
}
