package com.taiger.toreadorlab.owlseditor.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.taiger.toreadorlab.owlseditor.domain.DeclarativeModel;
import com.taiger.toreadorlab.owlseditor.service.DeclarativeModelService;
import com.taiger.toreadorlab.owlseditor.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing DeclarativeModel.
 */
@RestController
@RequestMapping("/api")
public class DeclarativeModelResource {

    private final Logger log = LoggerFactory.getLogger(DeclarativeModelResource.class);

    private static final String ENTITY_NAME = "declarativeModel";

    private final DeclarativeModelService declarativeModelService;

    public DeclarativeModelResource(DeclarativeModelService declarativeModelService) {
        this.declarativeModelService = declarativeModelService;
    }

    /**
     * POST  /declarative-models : Create a new declarativeModel.
     *
     * @param declarativeModel the declarativeModel to create
     * @return the ResponseEntity with status 201 (Created) and with body the new declarativeModel, or with status 400 (Bad Request) if the declarativeModel has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/declarative-models")
    @Timed
    public ResponseEntity<DeclarativeModel> createDeclarativeModel(@Valid @RequestBody DeclarativeModel declarativeModel) throws URISyntaxException {
        log.debug("REST request to save DeclarativeModel : {}", declarativeModel);
        if (declarativeModel.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new declarativeModel cannot already have an ID")).body(null);
        }
        DeclarativeModel result = declarativeModelService.save(declarativeModel);
        return ResponseEntity.created(new URI("/api/declarative-models/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /declarative-models : Updates an existing declarativeModel.
     *
     * @param declarativeModel the declarativeModel to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated declarativeModel,
     * or with status 400 (Bad Request) if the declarativeModel is not valid,
     * or with status 500 (Internal Server Error) if the declarativeModel couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/declarative-models")
    @Timed
    public ResponseEntity<DeclarativeModel> updateDeclarativeModel(@Valid @RequestBody DeclarativeModel declarativeModel) throws URISyntaxException {
        log.debug("REST request to update DeclarativeModel : {}", declarativeModel);
        if (declarativeModel.getId() == null) {
            return createDeclarativeModel(declarativeModel);
        }
        DeclarativeModel result = declarativeModelService.save(declarativeModel);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, declarativeModel.getId().toString()))
            .body(result);
    }

    /**
     * GET  /declarative-models : get all the declarativeModels.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of declarativeModels in body
     */
    @GetMapping("/declarative-models")
    @Timed
    public List<DeclarativeModel> getAllDeclarativeModels() {
        log.debug("REST request to get all DeclarativeModels");
        return declarativeModelService.findAll();
    }

    /**
     * GET  /declarative-models/:id : get the "id" declarativeModel.
     *
     * @param id the id of the declarativeModel to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the declarativeModel, or with status 404 (Not Found)
     */
    @GetMapping("/declarative-models/{id}")
    @Timed
    public ResponseEntity<DeclarativeModel> getDeclarativeModel(@PathVariable Long id) {
        log.debug("REST request to get DeclarativeModel : {}", id);
        DeclarativeModel declarativeModel = declarativeModelService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(declarativeModel));
    }

    /**
     * DELETE  /declarative-models/:id : delete the "id" declarativeModel.
     *
     * @param id the id of the declarativeModel to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/declarative-models/{id}")
    @Timed
    public ResponseEntity<Void> deleteDeclarativeModel(@PathVariable Long id) {
        log.debug("REST request to delete DeclarativeModel : {}", id);
        declarativeModelService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
