package com.taiger.toreadorlab.owlseditor.domain;

import java.io.Serializable;

public class TuningProperty implements Serializable
{
    private String id;

    private String name= "TuningProperty";

    private String category;

    private String measure;

    private String description;

    private String[] mappings;

    private String minValue;

    private String mandatory;

    private String value;

    private String type;

    private String defaultValue;

    private String key;

    private String maxValue;

    public String getId ()
    {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId (String id)

    {
        this.id = id;
    }

    public String getCategory ()
    {
        return category;
    }

    public void setCategory (String category)
    {
        this.category = category;
    }

    public String getMeasure ()
    {
        return measure;
    }

    public void setMeasure (String measure)
    {
        this.measure = measure;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String[] getMappings ()
    {
        return mappings;
    }

    public void setMappings (String[] mappings)
    {
        this.mappings = mappings;
    }

    public String getMinValue ()
    {
        return minValue;
    }

    public void setMinValue (String minValue)
    {
        this.minValue = minValue;
    }

    public String getMandatory ()
    {
        return mandatory;
    }

    public void setMandatory (String mandatory)
    {
        this.mandatory = mandatory;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getDefaultValue ()
    {
        return defaultValue;
    }

    public void setDefaultValue (String defaultValue)
    {
        this.defaultValue = defaultValue;
    }

    public String getKey ()
    {
        return key;
    }

    public void setKey (String key)
    {
        this.key = key;
    }

    public String getMaxValue ()
    {
        return maxValue;
    }

    public void setMaxValue (String maxValue)
    {
        this.maxValue = maxValue;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", category = "+category+", measure = "+measure+", description = "+description+", mappings = "+mappings+", minValue = "+minValue+", mandatory = "+mandatory+", value = "+value+", type = "+type+", defaultValue = "+defaultValue+", key = "+key+", maxValue = "+maxValue+"]";
    }
}
