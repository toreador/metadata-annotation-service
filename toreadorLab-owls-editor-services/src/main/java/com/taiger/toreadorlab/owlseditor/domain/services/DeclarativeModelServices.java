package com.taiger.toreadorlab.owlseditor.domain.services;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.taiger.toreadorlab.owlseditor.domain.declarative.ToreadorService;

public class DeclarativeModelServices {
	private Map<String, Set<ToreadorService>> servicesByArea = new HashMap();

	public Map<String, Set<ToreadorService>> getServicesByArea() {
		return servicesByArea;
	}

	public void setServicesByArea(Map<String, Set<ToreadorService>> servicesByArea) {
		this.servicesByArea = servicesByArea;
	}

	public void addServiceToArea(String area, ToreadorService service) {
		Set<ToreadorService> services = this.servicesByArea.get(area);
		if (services == null)
			services = new HashSet<ToreadorService>();
		if (!services.contains(service)) {
			services.add(service);
			this.servicesByArea.put(area, services);
		}
	}

//	public void addServicesToArea(String area, /*List<Service> services*/List<ToreadorService> services) {
//		if(services != null && !services.isEmpty()){
//			String areaService = services.get(0).getService().getArea().name();
//			List<ToreadorService> oldServices = this.servicesByArea.get(area);
//			if (oldServices != null){
//				for (ToreadorService service : oldServices) {
//					if (!services.contains(service))
//						services.add(service);
//				}
//				this.servicesByArea.put(areaService, services);
//			}
//		}
//	}
//	public static void removeDuplicates(Set<ToreadorService> al) {
//        for(int i = 0; i < al.size(); i++) {
//            for(int j = i + 1; j < al.size(); j++) {
//            	((ToreadorService)al(i)).getService().getId();
//                if(((ToreadorService)al.get(i))   .equals(al.get(j))){
//                    al.remove(j);
//                    j--;
//                }
//            }
//        }
//    }
	
	public void addServicesToArea(String area, /*List<Service> services*/Set<ToreadorService> services) {
		//List<Service> oldServices = this.servicesByArea.get(area);
        Set<ToreadorService> oldServices = this.servicesByArea.get(area);
		if (oldServices != null)
			for (ToreadorService service : oldServices) {
				if (!services.contains(service))
					services.add(service);
			}
		this.servicesByArea.put(area, services);
	}

	public void setServicesToArea(String area, Set<ToreadorService> services) {
		this.servicesByArea.put(area, services);
	}

	public Set<ToreadorService> getServicesByArea(String area) {
		return this.servicesByArea.get(area);
	}
}
