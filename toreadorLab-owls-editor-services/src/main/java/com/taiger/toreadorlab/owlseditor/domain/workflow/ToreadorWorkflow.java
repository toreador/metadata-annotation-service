package com.taiger.toreadorlab.owlseditor.domain.workflow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ToreadorWorkflow implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5669839717206689243L;
	private ToreadorElement elements;

	private String id;

	public ToreadorElement getElements() {
		return elements;
	}

	public void setElements(ToreadorElement elements) {
		this.elements = elements;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	// gets the nodes that are not in the source connection of any edge

	// FIXME here we have a problem... in case there are loops that enters the
	// initial node, we do not know where the flow starts. In this case we need
	// additional information from the toreador workflow document that indicates
	// which ones a re the starting nodes.
	//
	// Solved by adding two additional nodes types: START and END
	// keep in mind that START and END do not take part in the workflow
	// generation they are just placeholder to know where the flow start and
	// ends.
	public List<ToreadorNode> getStartingNodes() {
		List<ToreadorNode> nodes = new ArrayList<>();
		for (ToreadorNode node : elements.getNodes()) {
			if (node.getData().getNodeType().getType().equals(ToreadorConstantStrings.WORKFLOW_SERVICE_TYPE_START)) {
				nodes.addAll(this.getNextNodes(node));
			}
		}
		return nodes;
	}

	public List<ToreadorNode> getNextNodes(ToreadorNode node) {
		if (node == null)
			return new ArrayList<>();
		List<ToreadorNode> nodes = new ArrayList<>();
		for (ToreadorEdge edge : elements.getEdges()) {
			if (edge.getData().getSource().equals(node.getData().getId())) {
				//ToreadorNode node2 = elements.getNode(edge.getData().getTarget());
//				if (!!node2.getNodeType().equals(ToreadorConstantStrings.WORKFLOW_SERVICE_TYPE_END)
//						&& !!node2.getNodeType().equals(ToreadorConstantStrings.WORKFLOW_SERVICE_TYPE_START))
					nodes.add(elements.getNode(edge.getData().getTarget()));
			}
		}
        //Set<ToreadorNode> foo = new HashSet<ToreadorNode>(nodes);

        //List<ToreadorNode> list = new ArrayList(foo);
		return nodes;
	}

	// gets the nodes that are not in the target connection of any edge
	public List<ToreadorNode> getEndingNodes() {
		List<ToreadorNode> nodes = elements.getNodes();
		for (ToreadorEdge edge : elements.getEdges()) {
			nodes.remove(elements.getNode(edge.getData().getSource()));
		}
		return nodes;
	}

	// gets the edges outcoming a given node
	public List<ToreadorEdge> getOutEdges(ToreadorNode node) {
		List<ToreadorEdge> edges = elements.getEdges();
		for (ToreadorEdge edge : elements.getEdges()) {
			if (!edge.getData().getSource().equals(node.getData().getId()))
				edges.remove(edge);
		}
		return edges;
	}

	// gets the edges incoming a given node
	public List<ToreadorEdge> getInEdges(ToreadorNode node) {
		List<ToreadorEdge> edges = elements.getEdges();
		for (ToreadorEdge edge : elements.getEdges()) {
			if (!edge.getData().getTarget().equals(node.getData().getId()))
				edges.remove(edge);
		}
		return edges;
	}

	// get the node that is the target of an edge
	public ToreadorNode getNodeFromEdge(ToreadorEdge edge) {
		return elements.getNode(edge.getData().getTarget());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((elements == null) ? 0 : elements.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ToreadorWorkflow other = (ToreadorWorkflow) obj;
		if (elements == null) {
			if (other.elements != null)
				return false;
		} else if (!elements.equals(other.elements))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ToreadorWorkflow [elements=" + elements + ", id=" + id + "]";
	}

	public String getCycleNodeType(ToreadorNode node) {
		List<ToreadorEdge> inEdges = this.getInEdges(node);
		for (ToreadorEdge edge : inEdges) {
			ToreadorNode fromNode = this.getNodeFromEdge(edge);
			if ((fromNode.getNodeType().equals(ToreadorConstantStrings.WORKFLOW_SERVICE_TYPE_OPERATOR)) && ((fromNode
					.getOperatorType().equals(ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_REPEATUNTIL))
					|| (fromNode.getOperatorType().equals(ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_REPEATWHILE))
			// ||
			// (fromNode.getOperatorType().equals(ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_IF))
			)) {
				return fromNode.getOperatorType();

			}
		}
		return null;
	}

	public ToreadorNode getFirstJoinFromHere(List<ToreadorNode> nextNodes) {
		for (ToreadorNode node : nextNodes) {
			if (node.getNodeType().equals(ToreadorConstantStrings.WORKFLOW_SERVICE_TYPE_OPERATOR)
					&& node.getOperatorType().equals(ToreadorConstantStrings.WORKFLOW_OPERATOR_TYPE_JOIN)) {
				return node;
			} else {
				return getFirstJoinFromHere(this.getNextNodes(node));

			}
		}
		return null;
	}

}
