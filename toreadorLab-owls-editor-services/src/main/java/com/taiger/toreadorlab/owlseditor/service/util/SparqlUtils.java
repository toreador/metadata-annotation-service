package com.taiger.toreadorlab.owlseditor.service.util;

import java.util.Map;

import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.elasticsearch.common.collect.ImmutableMap;

public class SparqlUtils {

	public static ValueFactory valueFactory = SimpleValueFactory.getInstance();

	public static String idFromLabel(String label, String baseURI) {
		return baseURI + label.replaceAll("\\s", "").replaceAll("\\\\", "/").replaceAll("\"", "--");
	}

	/**
	 * See http://www.w3.org/TR/rdf-sparql-query/#grammarEscapes
	 *
	 * @param name
	 * @return
	 */
	private static final Map SPARQL_ESCAPE_SEARCH_REPLACEMENTS = ImmutableMap.builder().put("\t", "\\t")
			.put("\n", "\\n").put("\r", "\\r").put("\b", "\\b").put("\f", "\\f").put("\"", "\\\"").put("'", "\\'")
			.put("\\", "\\\\").build();

	public static String escape(String string) {

		StringBuilder bufOutput = new StringBuilder(string);
		for (int i = 0; i < bufOutput.length(); i++) {
			String replacement = (String) SPARQL_ESCAPE_SEARCH_REPLACEMENTS.get("" + bufOutput.charAt(i));
			if (replacement != null) {
				bufOutput.deleteCharAt(i);
				bufOutput.insert(i, replacement);
				// advance past the replacement
				i += (replacement.length() - 1);
			}
		}
		return bufOutput.toString();
	}

	public static String toUri(String id) {
		String uri = "";
		if (!id.startsWith("<")) {
			uri = "<";
		}
		uri += valueFactory.createURI(id);
		if (!uri.endsWith(">")) {
			uri += ">";
		}
		return uri;
	}
}
