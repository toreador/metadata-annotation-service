package com.taiger.toreadorlab.owlseditor.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.model.vocabulary.SKOS;
import org.eclipse.rdf4j.model.vocabulary.XMLSchema;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.MalformedQueryException;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.query.UpdateExecutionException;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.taiger.toreadorlab.owlseditor.domain.Element;
import com.taiger.toreadorlab.owlseditor.service.util.SparqlUtils;
import com.taiger.toreadorlab.owlseditor.web.rest.errors.ExceptionData;
import com.taiger.toreadorlab.owlseditor.web.rest.errors.ToreadorOntoEditorException;

import it.eng.toreador.catalogue.ApplicationProperty;
import it.eng.toreador.catalogue.Catalogue;
import it.eng.toreador.catalogue.Property;
import it.eng.toreador.catalogue.StaticProperty;
import it.eng.toreador.catalogue.TuningProperty;


@Service

public class MappingService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private ObjectMapper obejctMapper;

    @Autowired
    private OntologyService ontologyService;

    private static final String areaAnalytics = "analytics";
    private static final String areaIngestion = "ingestion";
    private static final String areaPreparation = "preparation";
    private static final String areaDataIntegration = "dataintegration";
    private static final String areaProcessing = "processing";
    private static final String areaVisualization = "visualization";
    private static final String areaRepresentation = "representation";
    @Value("${iconverse.ontology.serviceCategoryQuery}")
    private String serviceCategoriesQuery;
    @Value("${iconverse.ontology.serviceCategoryProperty}")
    private String serviceCategoriesProperty;
    @Value("${iconverse.ontology.serviceCatalogueEndpoint}")
    private String serviceCatalogueEndpoint;
    @Value("${iconverse.ontology.serviceCatalogueUsername}")
    private String serviceCatalogueEndpointUser;
    @Value("${iconverse.ontology.serviceCataloguePassword}")
    private String serviceCatalogueEndpointPassword;
    @Value("${iconverse.ontology.servicesURI}")
    private String servicesBaseURI;
    @Value("${iconverse.ontology.wsdlEndpoint}")
    private String wsdlEndpoint;
    @Value("${iconverse.ontology.baseURI}")
    private String baseURI;

    private ValueFactory valueFactory = SimpleValueFactory.getInstance();

    @Value("${iconverse.resources}")
    private String resourcesPath;

    // Sesame
    private Repository repository;
    private RepositoryConnection repositoryConnection;
    @Value("${iconverse.ontology.mode}")
    private String ontologyMode;
    @Value("${iconverse.ontology.baseUriOnly}")
    private boolean baseUriOnly;
    @Value("${iconverse.ontology.sparqlendpoint.select}")
    private String sparqlSelectEndpoint;
    @Value("${iconverse.ontology.sparqlendpoint.update}")
    private String sparqlUpdateEndpoint;

    @PostConstruct
    public void init() throws UnknownHostException {
        // init Sesame repository
        try {
            if ("memory".equals(ontologyMode)) {
                initMemoryRepository();
            } else if ("sparql".equals(ontologyMode)) {
                initSparqlRepository();
            }
        } catch (RepositoryException | RDFParseException | IOException e) {
            log.error("Error initializing ontology repository", e);
        }
        // initialize object mapper
        obejctMapper = new ObjectMapper();
    }

    private void initMemoryRepository() throws RepositoryException, RDFParseException, IOException {
        repository = new SailRepository(new MemoryStore());
        repository.initialize();
        repositoryConnection = repository.getConnection();
        // load ontology
        // File ontologyFile = new File(resourcesPath + ontologyPath);
        // repositoryConnection.add(ontologyFile, baseURI, RDFFormat.TURTLE);
        // repositoryConnection.commit();
    }

    private void initSparqlRepository() throws RepositoryException {
        repository = new SPARQLRepository(sparqlSelectEndpoint);
        repository.initialize();
        repositoryConnection = repository.getConnection();
    }

    public void reinit() throws UnknownHostException {
        try {
            repositoryConnection.close();
            repository.shutDown();
        } catch (RepositoryException e) {
            log.error("Error closing ontology repository", e);
        }
        init();
    }

    /**
     * @return
     * @throws ToreadorOntoEditorException
     */
    public Integer processServicesCatalogue() throws ToreadorOntoEditorException {
        try {
        	// Create a trust manager that does not validate certificate chains
	        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
	            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	                return null;
	            }
	            public void checkClientTrusted(X509Certificate[] certs, String authType) {
	            }
	            public void checkServerTrusted(X509Certificate[] certs, String authType) {
	            }
	        } };
	        // Install the all-trusting trust manager
	        final SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

	        // Create all-trusting host name verifier
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        };
	        
	        // Install the all-trusting host verifier
	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
            // get the services catalog from remote API rest service
            URL url = new URL(serviceCatalogueEndpoint);

            String userPassword = serviceCatalogueEndpointUser + ":" + serviceCatalogueEndpointPassword;
            String encoding = new sun.misc.BASE64Encoder().encode(userPassword.getBytes());
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Authorization", "Basic " + encoding);
            conn.setRequestProperty("Accept", "application/json");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setConnectTimeout(99999999);
			conn.setReadTimeout(99999999);
            // if everything is ok goes on otherwise throws beautiful errors

            if (conn.getResponseCode() != 200) {
                throw new ToreadorOntoEditorException(new RuntimeException("Wrong response from remote endpoint"),
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    new ExceptionData("Service endpoint answered with " + conn.getResponseCode(), null));
            }

            // object mapper to deserialize the services description //in case
            // the json changes then we need to change the model
            //obejctMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            //Services[] services = obejctMapper.readValue(conn.getInputStream(), it.eng.toreador.catalogue.Service);


            /*String json="[{\n" +
                "  \"services\": [{\n" +
                "    \"id\": 10,\n" +
                "    \"area\": \"ANALYTICS\",\n" +
                "    \"name\": \"spark-kmeans\",\n" +
                "    \"metrics\": [],\n" +
                "    \"mode\": \"BATCH\",\n" +
                "    \"version\": \"1.0.0\",\n" +
                "    \"description\": \"Exports a file from an FTP location to HDFS\",\n" +
                "    \"framework\": {\n" +
                "      \"id\": 1,\n" +
                "      \"name\": \"Spark\",\n" +
                "      \"version\": \"1.6.3\"\n" +
                "    },\n" +
                "    \"properties\": [{\n" +
                "        \"it.eng.toreador.catalogue.ApplicationProperty\": {\n" +
                "          \"id\": 1,\n" +
                "          \"description\": \"the hdfs path where the file will be written\",\n" +
                "          \"mandatory\": true,\n" +
                "          \"defaultValue\": null,\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"hdfsPath\",\n" +
                "          \"type\": \"STRING\",\n" +
                "          \"inputData\": false,\n" +
                "          \"outputData\": true\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.TuningProperty\": {\n" +
                "          \"id\": 5,\n" +
                "          \"description\": \"FTP Buffersize\",\n" +
                "          \"mandatory\": true,\n" +
                "          \"defaultValue\": \"262144\",\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"ftp.buffersize\",\n" +
                "          \"type\": \"STRING\",\n" +
                "          \"minValue\": \"1000\",\n" +
                "          \"maxValue\": \"1048576\",\n" +
                "          \"measure\": \"BYTES\",\n" +
                "          \"mappings\": [],\n" +
                "          \"category\": false\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.ApplicationProperty\": {\n" +
                "          \"id\": 2,\n" +
                "          \"description\": \"the FTP path for the input data\",\n" +
                "          \"mandatory\": true,\n" +
                "          \"defaultValue\": null,\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"ftpPath\",\n" +
                "          \"type\": \"STRING\",\n" +
                "          \"inputData\": true,\n" +
                "          \"outputData\": false\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.StaticProperty\": {\n" +
                "          \"id\": 3,\n" +
                "          \"description\": \"the url for ftp service\",\n" +
                "          \"mandatory\": false,\n" +
                "          \"defaultValue\": \"ftp://localhost:21\",\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"ftp.uri\",\n" +
                "          \"type\": \"STRING\",\n" +
                "          \"externalized\": false,\n" +
                "          \"uri\": null\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.StaticProperty\": {\n" +
                "          \"id\": 4,\n" +
                "          \"description\": \"the url for hdfs service\",\n" +
                "          \"mandatory\": true,\n" +
                "          \"defaultValue\": null,\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"hadoop.fs.defaultFS\",\n" +
                "          \"type\": \"STRING\",\n" +
                "          \"externalized\": false,\n" +
                "          \"uri\": null\n" +
                "        }\n" +
                "      }\n" +
                "    ],\n" +
                "    \"url\": \"docker://nodo1.toreador.org:8082/ftptohdfs:1.0.0.BUILD-SNAPSHOT\"\n" +
                "  }]\n" +
                "}]";


            String json2 ="[{\n" +
                "  \"services\": [{\n" +
                "    \"id\": 7,\n" +
                "    \"area\": \"INGESTION\",\n" +
                "    \"name\": \"ftptohdfs\",\n" +
                "    \"metrics\": [],\n" +
                "    \"mode\": \"BATCH\",\n" +
                "    \"version\": \"1.0.0\",\n" +
                "    \"description\": \"Exports a file from an FTP location to HDFS\",\n" +
                "    \"framework\": {\n" +
                "      \"id\": 1,\n" +
                "      \"name\": \"Hadoop\",\n" +
                "      \"version\": \"2.7.2\"\n" +
                "    },\n" +
                "    \"properties\": [{\n" +
                "        \"it.eng.toreador.catalogue.ApplicationProperty\": {\n" +
                "          \"id\": 1,\n" +
                "          \"description\": \"the hdfs path where the file will be written\",\n" +
                "          \"mandatory\": true,\n" +
                "          \"defaultValue\": null,\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"hdfsPath\",\n" +
                "          \"type\": \"STRING\",\n" +
                "          \"inputData\": false,\n" +
                "          \"outputData\": true\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.TuningProperty\": {\n" +
                "          \"id\": 5,\n" +
                "          \"description\": \"FTP Buffersize\",\n" +
                "          \"mandatory\": true,\n" +
                "          \"defaultValue\": \"262144\",\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"ftp.buffersize\",\n" +
                "          \"type\": \"STRING\",\n" +
                "          \"minValue\": \"1000\",\n" +
                "          \"maxValue\": \"1048576\",\n" +
                "          \"measure\": \"BYTES\",\n" +
                "          \"mappings\": [],\n" +
                "          \"category\": false\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.ApplicationProperty\": {\n" +
                "          \"id\": 2,\n" +
                "          \"description\": \"the FTP path for the input data\",\n" +
                "          \"mandatory\": true,\n" +
                "          \"defaultValue\": null,\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"ftpPath\",\n" +
                "          \"type\": \"STRING\",\n" +
                "          \"inputData\": true,\n" +
                "          \"outputData\": false\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.StaticProperty\": {\n" +
                "          \"id\": 3,\n" +
                "          \"description\": \"the url for ftp service\",\n" +
                "          \"mandatory\": false,\n" +
                "          \"defaultValue\": \"ftp://localhost:21\",\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"ftp.uri\",\n" +
                "          \"type\": \"STRING\",\n" +
                "          \"externalized\": false,\n" +
                "          \"uri\": null\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.StaticProperty\": {\n" +
                "          \"id\": 4,\n" +
                "          \"description\": \"the url for hdfs service\",\n" +
                "          \"mandatory\": true,\n" +
                "          \"defaultValue\": null,\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"hadoop.fs.defaultFS\",\n" +
                "          \"type\": \"STRING\",\n" +
                "          \"externalized\": false,\n" +
                "          \"uri\": null\n" +
                "        }\n" +
                "      }\n" +
                "    ],\n" +
                "    \"url\": \"docker://nodo1.toreador.org:8082/ftptohdfs:1.0.0.BUILD-SNAPSHOT\"\n" +
                "  }, {\n" +
                "    \"id\": 8,\n" +
                "    \"area\": \"PREPARATION\",\n" +
                "    \"name\": \"spark-cleanser\",\n" +
                "    \"metrics\": [{\n" +
                "      \"id\": 1,\n" +
                "      \"name\": \"time\",\n" +
                "      \"minValue\": 0,\n" +
                "      \"maxValue\": null,\n" +
                "      \"value\": null,\n" +
                "      \"measure\": \"MILLISECONDS\",\n" +
                "      \"url\": \"${historyServerUrl}/api/v1/applications\"\n" +
                "    }],\n" +
                "    \"mode\": \"BATCH\",\n" +
                "    \"version\": \"1.0.0\",\n" +
                "    \"description\": \"Replaces every occurrency of a regular expression with an empty string\",\n" +
                "    \"framework\": {\n" +
                "      \"id\": 2,\n" +
                "      \"name\": \"Spark\",\n" +
                "      \"version\": \"1.6.3\"\n" +
                "    },\n" +
                "    \"properties\": [{\n" +
                "        \"it.eng.toreador.catalogue.TuningProperty\": {\n" +
                "          \"id\": 89,\n" +
                "          \"description\": \"Whether to compress data spilled during shuffles\",\n" +
                "          \"mandatory\": false,\n" +
                "          \"defaultValue\": \"1\",\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"spark.shuffle.spill.compress\",\n" +
                "          \"type\": \"BOOLEAN\",\n" +
                "          \"minValue\": \"0\",\n" +
                "          \"maxValue\": \"1\",\n" +
                "          \"measure\": \"NONE\",\n" +
                "          \"mappings\": [],\n" +
                "          \"category\": false\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.TuningProperty\": {\n" +
                "          \"id\": 91,\n" +
                "          \"description\": \"The codec used to compress internal data such as RDD partitions, event log, broadcast variables and shuffle outputs\",\n" +
                "          \"mandatory\": false,\n" +
                "          \"defaultValue\": \"0\",\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"spark.io.compression.codec\",\n" +
                "          \"type\": \"INT\",\n" +
                "          \"minValue\": \"0\",\n" +
                "          \"maxValue\": \"2\",\n" +
                "          \"measure\": \"NONE\",\n" +
                "          \"mappings\": [{\n" +
                "              \"id\": 11,\n" +
                "              \"intValue\": 0,\n" +
                "              \"name\": \"lz4\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"id\": 12,\n" +
                "              \"intValue\": 1,\n" +
                "              \"name\": \"lzf\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"id\": 13,\n" +
                "              \"intValue\": 2,\n" +
                "              \"name\": \"snappy\"\n" +
                "            }\n" +
                "          ],\n" +
                "          \"category\": true\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.TuningProperty\": {\n" +
                "          \"id\": 95,\n" +
                "          \"description\": \"Number of cores to allocate for each task\",\n" +
                "          \"mandatory\": false,\n" +
                "          \"defaultValue\": \"1\",\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"spark.task.cpus\",\n" +
                "          \"type\": \"INT\",\n" +
                "          \"minValue\": \"1\",\n" +
                "          \"maxValue\": \"12\",\n" +
                "          \"measure\": \"NONE\",\n" +
                "          \"mappings\": [],\n" +
                "          \"category\": false\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.TuningProperty\": {\n" +
                "          \"id\": 93,\n" +
                "          \"description\": \"Default number of partitions in RDDs returned by transformations like join, reduceByKey, and parallelize when not set by user\",\n" +
                "          \"mandatory\": false,\n" +
                "          \"defaultValue\": \"1\",\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"spark.default.parallelism\",\n" +
                "          \"type\": \"INT\",\n" +
                "          \"minValue\": \"1\",\n" +
                "          \"maxValue\": \"12\",\n" +
                "          \"measure\": \"NONE\",\n" +
                "          \"mappings\": [],\n" +
                "          \"category\": false\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.TuningProperty\": {\n" +
                "          \"id\": 85,\n" +
                "          \"description\": \"The number of cores to use on each executor\",\n" +
                "          \"mandatory\": false,\n" +
                "          \"defaultValue\": \"1\",\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"spark.executor.cores\",\n" +
                "          \"type\": \"INT\",\n" +
                "          \"minValue\": \"1\",\n" +
                "          \"maxValue\": \"4\",\n" +
                "          \"measure\": \"NONE\",\n" +
                "          \"mappings\": [],\n" +
                "          \"category\": false\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.StaticProperty\": {\n" +
                "          \"id\": 9,\n" +
                "          \"description\": \"the url for hdfs service\",\n" +
                "          \"mandatory\": true,\n" +
                "          \"defaultValue\": null,\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"hadoop.fs.defaultFS\",\n" +
                "          \"type\": \"STRING\",\n" +
                "          \"externalized\": false,\n" +
                "          \"uri\": null\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.TuningProperty\": {\n" +
                "          \"id\": 92,\n" +
                "          \"description\": \"Size of each piece of a block for TorrentBroadcastFactory\",\n" +
                "          \"mandatory\": false,\n" +
                "          \"defaultValue\": \"4194304\",\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"spark.broadcast.blockSize\",\n" +
                "          \"type\": \"INT\",\n" +
                "          \"minValue\": \"2097152\",\n" +
                "          \"maxValue\": \"8388608\",\n" +
                "          \"measure\": \"BYTES\",\n" +
                "          \"mappings\": [],\n" +
                "          \"category\": false\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.TuningProperty\": {\n" +
                "          \"id\": 86,\n" +
                "          \"description\": \"Amount of memory to use per executor process\",\n" +
                "          \"mandatory\": false,\n" +
                "          \"defaultValue\": \"1073741824\",\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"spark.executor.memory\",\n" +
                "          \"type\": \"INT\",\n" +
                "          \"minValue\": \"1073741824\",\n" +
                "          \"maxValue\": \"17179869184\",\n" +
                "          \"measure\": \"BYTES\",\n" +
                "          \"mappings\": [],\n" +
                "          \"category\": false\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.ApplicationProperty\": {\n" +
                "          \"id\": 7,\n" +
                "          \"description\": \"HDFS output path to the cleansed data\",\n" +
                "          \"mandatory\": true,\n" +
                "          \"defaultValue\": null,\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"outputpath\",\n" +
                "          \"type\": \"STRING\",\n" +
                "          \"inputData\": false,\n" +
                "          \"outputData\": true\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.TuningProperty\": {\n" +
                "          \"id\": 87,\n" +
                "          \"description\": \"Maximum size of map outputs to fetch simultaneously from each reduce task\",\n" +
                "          \"mandatory\": false,\n" +
                "          \"defaultValue\": \"50331648\",\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"spark.reducer.maxSizeInFlight\",\n" +
                "          \"type\": \"INT\",\n" +
                "          \"minValue\": \"25165824\",\n" +
                "          \"maxValue\": \"100663296\",\n" +
                "          \"measure\": \"BYTES\",\n" +
                "          \"mappings\": [],\n" +
                "          \"category\": false\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.ApplicationProperty\": {\n" +
                "          \"id\": 8,\n" +
                "          \"description\": \"String regular expression to cleanse\",\n" +
                "          \"mandatory\": true,\n" +
                "          \"defaultValue\": null,\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"stringpattern\",\n" +
                "          \"type\": \"STRING\",\n" +
                "          \"inputData\": false,\n" +
                "          \"outputData\": false\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.TuningProperty\": {\n" +
                "          \"id\": 90,\n" +
                "          \"description\": \"Whether to compress broadcast variables before sending them\",\n" +
                "          \"mandatory\": false,\n" +
                "          \"defaultValue\": \"1\",\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"spark.broadcast.compress\",\n" +
                "          \"type\": \"BOOLEAN\",\n" +
                "          \"minValue\": \"0\",\n" +
                "          \"maxValue\": \"1\",\n" +
                "          \"measure\": \"NONE\",\n" +
                "          \"mappings\": [],\n" +
                "          \"category\": false\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.TuningProperty\": {\n" +
                "          \"id\": 88,\n" +
                "          \"description\": \"Whether to compress map output files\",\n" +
                "          \"mandatory\": false,\n" +
                "          \"defaultValue\": \"1\",\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"spark.shuffle.compress\",\n" +
                "          \"type\": \"BOOLEAN\",\n" +
                "          \"minValue\": \"0\",\n" +
                "          \"maxValue\": \"1\",\n" +
                "          \"measure\": \"NONE\",\n" +
                "          \"mappings\": [],\n" +
                "          \"category\": false\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.TuningProperty\": {\n" +
                "          \"id\": 83,\n" +
                "          \"description\": \"Number of cores to use for the driver process\",\n" +
                "          \"mandatory\": false,\n" +
                "          \"defaultValue\": \"1\",\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"spark.driver.cores\",\n" +
                "          \"type\": \"INT\",\n" +
                "          \"minValue\": \"1\",\n" +
                "          \"maxValue\": \"4\",\n" +
                "          \"measure\": \"NONE\",\n" +
                "          \"mappings\": [],\n" +
                "          \"category\": false\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.ApplicationProperty\": {\n" +
                "          \"id\": 6,\n" +
                "          \"description\": \"HDFS path to input data\",\n" +
                "          \"mandatory\": true,\n" +
                "          \"defaultValue\": null,\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"inputdatapath\",\n" +
                "          \"type\": \"STRING\",\n" +
                "          \"inputData\": true,\n" +
                "          \"outputData\": false\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.TuningProperty\": {\n" +
                "          \"id\": 84,\n" +
                "          \"description\": \"Amount of memory to use for the driver process\",\n" +
                "          \"mandatory\": false,\n" +
                "          \"defaultValue\": \"1073741824\",\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"spark.driver.memory\",\n" +
                "          \"type\": \"INT\",\n" +
                "          \"minValue\": \"1073741824\",\n" +
                "          \"maxValue\": \"17179869184\",\n" +
                "          \"measure\": \"BYTES\",\n" +
                "          \"mappings\": [],\n" +
                "          \"category\": false\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.TuningProperty\": {\n" +
                "          \"id\": 94,\n" +
                "          \"description\": \"If set to true, performs speculative execution of tasks\",\n" +
                "          \"mandatory\": false,\n" +
                "          \"defaultValue\": \"0\",\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"spark.speculation\",\n" +
                "          \"type\": \"BOOLEAN\",\n" +
                "          \"minValue\": \"0\",\n" +
                "          \"maxValue\": \"1\",\n" +
                "          \"measure\": \"NONE\",\n" +
                "          \"mappings\": [],\n" +
                "          \"category\": false\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"it.eng.toreador.catalogue.TuningProperty\": {\n" +
                "          \"id\": 96,\n" +
                "          \"description\": \"Class to use for serializing objects that will be sent over the network or need to be cached in serialized form\",\n" +
                "          \"mandatory\": false,\n" +
                "          \"defaultValue\": \"0\",\n" +
                "          \"value\": null,\n" +
                "          \"key\": \"spark.serializer\",\n" +
                "          \"type\": \"INT\",\n" +
                "          \"minValue\": \"0\",\n" +
                "          \"maxValue\": \"1\",\n" +
                "          \"measure\": \"NONE\",\n" +
                "          \"mappings\": [{\n" +
                "              \"id\": 14,\n" +
                "              \"intValue\": 0,\n" +
                "              \"name\": \"JavaSerializer\"\n" +
                "            },\n" +
                "            {\n" +
                "              \"id\": 15,\n" +
                "              \"intValue\": 1,\n" +
                "              \"name\": \"KryoSerializer\"\n" +
                "            }\n" +
                "          ],\n" +
                "          \"category\": true\n" +
                "        }\n" +
                "      }\n" +
                "    ],\n" +
                "    \"url\": \"docker://nodo1.toreador.org:8082/spark-cleanser:1.0.0.BUILD-SNAPSHOT\"\n" +
                "  }]\n" +
                "}]";
                */

            it.eng.toreador.catalogue.Catalogue[] services = obejctMapper.readValue(conn.getInputStream(), it.eng.toreador.catalogue.Catalogue[].class);
            //it.eng.toreador.catalogue.Catalogue[] services = obejctMapper.readValue(json, it.eng.toreador.catalogue.Catalogue[].class);
            //it.eng.toreador.catalogue.Catalogue[] services2 = obejctMapper.readValue(json2, it.eng.toreador.catalogue.Catalogue[].class);
            log.info(services.toString());
            // closes connection
            conn.disconnect();
            // transalte Services to elements of the OWL-S ontology
            mapServicesToOntology(services[0]);
            //mapServicesToOntology(services2[0]);
            // just for returning something
            //return services[0].getServices().size();
            return services[0].getServices().size();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        } catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return -1;

    }

    private void mapServicesToOntology(Catalogue services) throws ToreadorOntoEditorException {
        //List<it.eng.toreador.catalogue.Service> Lista de servicios dentro del catalogo.
        for (it.eng.toreador.catalogue.Service serviceDescription : services.getServices()) {
            log.info(serviceDescription.getArea().toString().trim());
            String serviceArea = null;
            switch (serviceDescription.getArea().toString().toLowerCase()) {
                case areaAnalytics: {
                    log.info(baseURI + "dataAnalytics");
                    serviceArea = "dataAnalytics";
                    break;
                }
                case areaIngestion: {
                    log.info("ingestion not yet existing in the TDM model!");
                    serviceArea = baseURI + "dataAnalytics";
                    break;
                }
                case areaPreparation: {
                    log.info(baseURI + "dataPreparation");
                    serviceArea = "dataPreparation";
                    break;
                }
                case areaDataIntegration: {
                    log.info(baseURI + "dataIntegration");
                    serviceArea = "dataIntegration";
                    break;
                }
                case areaProcessing: {
                    log.info(baseURI + "dataProcessing");
                    serviceArea = "dataProcessing";
                    break;
                }
                case areaVisualization: {
                    log.info(baseURI + "dataDisplayReporting");
                    serviceArea = "dataDisplayReporting";
                    break;
                }
                case areaRepresentation: {
                    log.info(baseURI + "dataRepresentation");
                    serviceArea = "dataRepresentation";
                    break;
                }
                default: {
                    log.warn("no area match, returning DEFAULT!!!");
                    // serviceArea = baseURI + "dataAnalytics";
                    break;
                }
            }
            // if there is an area identified then add service to ontology
            if (serviceArea != null)
                createInstanceMatarializedService(serviceArea, serviceDescription);
        }
    }

    public boolean checkIndividualExists(String id)
        throws MalformedQueryException, RepositoryException, QueryEvaluationException {
        String query = "SELECT ?a ?o " + "WHERE{" + "  " + SparqlUtils.toUri(id) + " a ?o " + "}";
        log.info(query);
        TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, query);
        TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
        log.info(tupleQueryResult.hasNext() + "");
        return tupleQueryResult.hasNext();
    }

    private void createInstanceMatarializedService(String serviceArea, it.eng.toreador.catalogue.Service serviceDescription)
        throws ToreadorOntoEditorException {
        // existence of individual)
        // some logging
        try {
            log.info("ID: " + serviceDescription.getId());
            log.info("Area: " + serviceDescription.getArea());
            log.info("Name: " + serviceDescription.getName());
//            log.info("Metrics: " + serviceDescription.getMetrics());
            log.info("Mode: " + serviceDescription.getMode());
            log.info("Version: " + serviceDescription.getVersion());
            log.info("Description: " + serviceDescription.getDescription());
            //log.info("Framework: " + serviceDescription.getFramework().getName());

            log.info("INFORMACION SOBRE LAS PROPIEDADES");

            /*for (Property properties : serviceDescription.getProperties()) {
                log.info(properties.toString());
                log.info("ID Property: " +String.valueOf(properties.getId()));
                log.info("Description Property: " +properties.getDescription());
                log.info("Mandatory Property: " +properties.isMandatory());
                log.info("default Property: " +properties.getDefaultValue());
                log.info("value Property: " +properties.getValue());
                log.info("Key Property: " +properties.getKey());
                log.info("Type Property: " +properties.getType());

                if(properties instanceof it.eng.toreador.catalogue.TuningProperty ){
                    log.info("minValue: " + ((TuningProperty) properties).getMinValue());
                    log.info("mmaxValue: " + ((TuningProperty) properties).getMaxValue());
                    log.info("measure: " + ((TuningProperty) properties).getMeasure());
                    log.info("mappings: " + ((TuningProperty) properties).getMappings());
                    log.info("Category: " + ((TuningProperty) properties).isCategory());
                }
                if(properties instanceof it.eng.toreador.catalogue.StaticProperty){
                     log.info("Externalized: " + String.valueOf(((it.eng.toreador.catalogue.StaticProperty) properties).isExternalized()));
                     log.info("URI: " + String.valueOf(((it.eng.toreador.catalogue.StaticProperty) properties).getUri()));
                }
                if(properties instanceof it.eng.toreador.catalogue.ApplicationProperty){
                    log.info("Input: " + String.valueOf(((it.eng.toreador.catalogue.ApplicationProperty) properties).isInputData()));
                    log.info("Output: " + String.valueOf(((it.eng.toreador.catalogue.ApplicationProperty) properties).isOutputData()));
                 }


            }*/

            log.info(serviceDescription.getProperties().toString());
            Set<it.eng.toreador.catalogue.Property> aux = new HashSet<>();

            for(Property parameter: serviceDescription.getProperties()){
                if(parameter instanceof TuningProperty==false){
                    aux.add(parameter);
                }
            }
            serviceDescription.setProperties(aux);
            log.info(serviceDescription.getProperties().toString());

            log.info("FIN SOBRE LAS PROPIEDADES");
            // available areas
            // <http://toreador-project.eu/TDM/TDM.owl#dataAnalytics>
            // <http://toreador-project.eu/TDM/TDM.owl#dataDisplayReporting>
            // <http://toreador-project.eu/TDM/TDM.owl#dataPreparation>
            // <http://toreador-project.eu/TDM/TDM.owl#dataProcessing>
            // <http://toreador-project.eu/TDM/TDM.owl#dataRepresentation>

            String label;
            if(serviceDescription.getFramework()!=null){
                label = SparqlUtils
                    .escape(serviceDescription.getFramework().getName().replaceAll(" ", "") + serviceDescription.getArea() +
                        serviceDescription.getId() + serviceDescription.getName().replaceAll(" ","")+"-"+serviceDescription.getFramework().getVersion());

            }else{
                label = SparqlUtils
                    .escape("nullFramework" + serviceDescription.getArea() +
                        serviceDescription.getId() + serviceDescription.getName().replaceAll(" ","")+"-"+"nullFramework");
            }


            if (label == null) {
                log.warn("Label must not be null");
                throw new ToreadorOntoEditorException(HttpStatus.BAD_REQUEST, new ExceptionData("Label must not be null"));
            }

            String graphUri = SparqlUtils.idFromLabel(label, servicesBaseURI) + ".owl";
            String serviceUri = graphUri + "#";
            // delete entire old graph of the service
            //ontologyService.deleteGraph(graphUri);

            try {
                ModelBuilder rdfGraphModelBuilder = new ModelBuilder();
                // create the main elements identifiers
                IRI groundingIRI = (valueFactory.createIRI(serviceUri + label + "Grounding"));
                IRI processIRI = (valueFactory.createIRI(serviceUri + label + "Process"));
                IRI profileIRI = (valueFactory.createIRI(serviceUri + label + "Profile"));
                IRI serviceIRI = (valueFactory.createIRI(serviceUri + label + "Service"));
                // create the basic model with imports and namespaces and set
                // context
                rdfGraphModelBuilder.namedGraph(graphUri).setNamespace("mapping", serviceUri)
                    .setNamespace("process", "http://www.daml.org/services/owl-s/1.2/Process.owl#")
                    .setNamespace("service", "http://www.daml.org/services/owl-s/1.2/Service.owl#")
                    .setNamespace("grounding", "http://www.daml.org/services/owl-s/1.2/Grounding.owl#")
                    .setNamespace("profile", "http://www.daml.org/services/owl-s/1.2/Profile.owl#")
                    // imports the owl-s ontologies required
                    .add(OWL.ONTOLOGY, OWL.IMPORTS,
                        valueFactory.createIRI("http://www.daml.org/services/owl-s/1.2/Grounding.owl"))
                    .add(OWL.ONTOLOGY, OWL.IMPORTS,
                        valueFactory.createIRI("http://www.daml.org/services/owl-s/1.2/Profile.owl"))
                    .add(OWL.ONTOLOGY, OWL.IMPORTS,
                        valueFactory.createIRI("http://www.daml.org/services/owl-s/1.2/Process.owl"))
                    .add(OWL.ONTOLOGY, OWL.IMPORTS,
                        valueFactory.createIRI("http://www.daml.org/services/owl-s/1.2/Service.owl"))
                    // create instances of main components and relate them whit
                    // main service
                    .add(groundingIRI, RDF.TYPE, "grounding:WsdlGrounding")
                    .add(processIRI, RDF.TYPE, "process:AtomicProcess").add(profileIRI, RDF.TYPE, "profile:Profile")
                    .add(serviceIRI, RDF.TYPE, "service:Service")

                    .subject(serviceIRI).add("service:supports", groundingIRI).add("servicetransformWorkflowToOWLS:describedBy", processIRI)
                    .add(RDFS.LABEL, label + "Service").add("service:presents", profileIRI)

                    .subject(groundingIRI).add(RDFS.LABEL, label + "Grounding").add("service:supportedBy", serviceIRI)

                    .subject(processIRI).add(RDFS.LABEL, label + "Process").add("service:describes", serviceIRI)

                    .subject(profileIRI).add("service:presentedBy", serviceIRI).add("profile:serviceName", /*serviceDescription.getName()*/label)
                    .add(RDFS.LABEL, label + "Profile")
                    .add("profile:textDescription", valueFactory.createLiteral(serviceDescription.getDescription()));
                // blank node for servicecategory(Profile)
                IRI serviceCategory = valueFactory.createIRI(serviceUri + label + "ServiceCategory");
                rdfGraphModelBuilder.subject(serviceCategory).add(RDF.TYPE, "profile:ServiceCategory")
                	.add(SKOS.ALT_LABEL, matchCategory(serviceDescription))
                    .add(RDFS.LABEL, label + "ServiceCategory")
                    .add("profile:categoryName", matchCategory(serviceDescription))
                    .add("profile:taxonomy",
                    baseURI.endsWith("#") ? baseURI.substring(0, baseURI.length() - 1) : baseURI)

                    .subject(profileIRI).add("profile:serviceCategory", serviceCategory);

                // GROUNDING

                IRI atomicProcessGrouding = valueFactory.createIRI(serviceUri + label + "AtomicProcessGrounding");
                IRI operationRef = valueFactory.createIRI(serviceUri + UUID.randomUUID());
                // relate atomicgrounding instance to grounding instance
                rdfGraphModelBuilder.subject(groundingIRI).add("grounding:hasAtomicProcessGrounding", atomicProcessGrouding)
                    // create atomic grunding instance and its properties
                    .subject(atomicProcessGrouding).add(RDF.TYPE, "grounding:WsdlAtomicProcessGrounding")
                    .add(RDFS.LABEL, label + "AtomicProcessGrounding").add("grounding:owlsProcess", processIRI)
                    .add("grounding:wsdlDocument", valueFactory.createLiteral(wsdlEndpoint, XMLSchema.ANYURI))

                    .add("grounding:wsdlOperation", operationRef)
                    // wsdloperationRef instance
                    .subject(operationRef).add("grounding:operation",
                    valueFactory.createLiteral(wsdlEndpoint + "#" + label, XMLSchema.ANYURI));

                // PARAMETERS///

                // for each parameter create the parameter class, if the parameter
                // is output then add also the output instance

                for (Property parameter : serviceDescription.getProperties()) {
                    Literal datatype = valueFactory.createLiteral(XMLSchema.STRING.stringValue(), XMLSchema.ANYURI);
                    switch (parameter.getType().toString().toLowerCase()) {
                        case "int": {
                            datatype = valueFactory.createLiteral(XMLSchema.INT.stringValue(), XMLSchema.ANYURI);
                            break;
                        }
                        case "double": {
                            datatype = valueFactory.createLiteral(XMLSchema.DOUBLE.stringValue(), XMLSchema.ANYURI);
                            break;
                        }
                        case "boolean": {
                            datatype = valueFactory.createLiteral(XMLSchema.BOOLEAN.stringValue(), XMLSchema.ANYURI);
                            break;
                        }
                        case "datetime": {
                            datatype = valueFactory.createLiteral(XMLSchema.DATETIME.stringValue(), XMLSchema.ANYURI);
                            break;
                        }
                        case "date": {
                            datatype = valueFactory.createLiteral(XMLSchema.DATE.stringValue(), XMLSchema.ANYURI);
                            break;
                        }
                        case "time": {
                            datatype = valueFactory.createLiteral(XMLSchema.TIME.stringValue(), XMLSchema.ANYURI);
                            break;
                        }
                        case "long": {
                            datatype = valueFactory.createLiteral(XMLSchema.LONG.stringValue(), XMLSchema.ANYURI);
                            break;
                        }
                        case "float": {
                            datatype = valueFactory.createLiteral(XMLSchema.FLOAT.stringValue(), XMLSchema.ANYURI);
                            break;
                        }
                        case "uri":
                        case "url": {
                            datatype = valueFactory.createLiteral(XMLSchema.ANYURI.stringValue(), XMLSchema.ANYURI);
                            break;
                        }
                        default: {
                            break;
                        }
                    }



                    IRI parameterIRI = valueFactory.createIRI(serviceUri + parameter.getKey().trim());
                    // add default value
                    rdfGraphModelBuilder.subject(parameterIRI).add(baseURI + "defaultValue",
                        valueFactory.createLiteral(parameter.getDefaultValue() == null ? "null" : parameter.getDefaultValue(), XMLSchema.STRING));
                    // add mandatory value
                    rdfGraphModelBuilder.subject(parameterIRI).add(baseURI + "mandatory",
                        valueFactory.createLiteral(parameter.isMandatory()));


                    ApplicationProperty appProperty=null;
                    StaticProperty staProperty =null;
                    if(parameter instanceof ApplicationProperty==true){
                        appProperty= (ApplicationProperty) parameter;

                        if(appProperty.isInputData()){
                            // create parameter instance
                            rdfGraphModelBuilder.subject(parameterIRI).add(RDF.TYPE, "process:Input")
                                .add("process:parameterType", datatype).add(RDFS.LABEL, parameter.getKey())
                                .add(RDFS.COMMENT, parameter.getDescription())
                                // relate input to process instance
                                .subject(processIRI).add("process:hasInput", parameterIRI)
                                // relate parameter to profile instance
                                .subject(profileIRI).add("profile:hasInput", parameterIRI);

                            // grounding part
                            IRI inputBnode = valueFactory.createIRI(serviceUri + UUID.randomUUID());
                            // relate wsdlinput map to aptomicprocess grounding
                            rdfGraphModelBuilder.subject(atomicProcessGrouding).add("grounding:wsdlInput", inputBnode)
                                // create wsdlinput map
                                .subject(inputBnode).add(RDF.TYPE, "grounding:WsdlInputMessageMap")
                                .add("grounding:wsdlMessagePart",
                                    valueFactory.createLiteral(wsdlEndpoint + "#" + parameter.getKey().trim(), XMLSchema.ANYURI))
                                .add("grounding:owlsParameter", serviceUri + parameter.getKey().trim());
                            // end of grounginpart for input parameters
                        }
                        if(appProperty.isOutputData()){
                            //if (parameter.getKey().toLowerCase().startsWith("output") || (parameter.getDescription().toLowerCase().contains("output"))) {
                                rdfGraphModelBuilder.subject(parameterIRI).add(RDF.TYPE, "process:Output")
                                    .add("process:parameterType", datatype).add(RDFS.LABEL, parameter.getKey().trim())
                                    .add(RDFS.COMMENT, parameter.getDescription())
                                    // relate input to process instance
                                    .subject(processIRI).add("process:hasOutput", parameterIRI)
                                    // relate parameter to profile instance
                                    .subject(profileIRI).add("profile:hasOutput", parameterIRI);

                                // grounding part
                                IRI outputBnode = valueFactory.createIRI(serviceUri + UUID.randomUUID());
                                // relate wsdlinput map to aptomicprocess grounding
                                rdfGraphModelBuilder.subject(atomicProcessGrouding).add("grounding:wsdlOutput", outputBnode)
                                    // create wsdlinput map
                                    .subject(outputBnode).add(RDF.TYPE, "grounding:WsdlOutputMessageMap")
                                    .add("grounding:wsdlMessagePart",
                                        valueFactory.createLiteral(wsdlEndpoint + "#" + parameter.getKey().trim(),
                                            XMLSchema.ANYURI))
                                    .add("grounding:owlsParameter", serviceUri + parameter.getKey().trim());
                                // end of grounginpart for input parameters

                            //}
                        }

                    }else{
                        staProperty= (StaticProperty) parameter;
                    }



                    // check if the parameter is output
                    // TODO this should be provided by ENG doing this kind of check
                    // is risky
                    //FIXME this check should change once from Service description they indicate which parameters are input and which ones are output


                    }

                    //To parser models to owl and save them in files
                //transformGraphToOwl(rdfGraphModelBuilder,label);



                //Test if the graphUri has results in blazegraph and adds if the result count is 0.
                String queryAux = "select (count(distinct ?s) AS ?count) where {graph <"+graphUri+"> {?s ?p ?o.}}";
                TupleQuery tupleQuery;
                tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, queryAux);
                TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
                String numberOfResults="";
                while (tupleQueryResult.hasNext()) {
                    BindingSet result = tupleQueryResult.next();
                    numberOfResults = String.valueOf(result.getValue("count").stringValue());
                }
                Integer nResults=Integer.parseInt(numberOfResults);
                // upload the graph generated so far in the triple store ONLY STATIC PROPERTIES

                if(nResults==0) {
                    ontologyService.uploadGraph(rdfGraphModelBuilder.build(), graphUri);
                    log.info("Individual created");
                }

            } catch (RepositoryException | MalformedQueryException | QueryEvaluationException
                | UpdateExecutionException e) {
                log.error("Error creating individual", e);
                throw new ToreadorOntoEditorException(e, HttpStatus.INTERNAL_SERVER_ERROR,
                    new ExceptionData("Unexpected error"));
            }
        } catch (NullPointerException e) {
            log.error(e.getMessage());

        }
    }

    private String matchCategory(it.eng.toreador.catalogue.Service serviceDescription) {
        String category = "THIS NEED TO CHANGE!!!!";
        category = serviceDescription.getName();
        
        return category;
    }

    private String getRelatedSpecificationIfExists(String area) throws ToreadorOntoEditorException {
        String query = "SELECT DISTINCT ?specification ?type WHERE{ ?specification rdf:type ?type. ?specification <http://toreador-project.eu/TDM/TDM.owl#hasArea> <"
            + area + ">. ?type rdfs:subClassOf* <http://toreador-project.eu/TDM/TDM.owl#Specification>.}";
        TupleQuery tupleQuery;
        try {
            tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, query);
            TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
            while (tupleQueryResult.hasNext()) {
                BindingSet result = tupleQueryResult.next();
                String specification = result.getValue("specification").stringValue();
                return specification;
            }
        } catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
            log.info("Unexpected query exception", e);
            throw new ToreadorOntoEditorException(e, HttpStatus.INTERNAL_SERVER_ERROR,
                new ExceptionData("Unexpected error getting exclusive properties", null));
        }
        return null;
    }

    private void transformGraphToOwl(ModelBuilder rdfGraphModelBuilder, String label){
        String path = "/Users/Christian/Desktop/servicesCatalogue/"+label+".owl";
        File file = new File(path);
        try {
            BufferedWriter bw=new BufferedWriter(new FileWriter(file));
            Rio.write(rdfGraphModelBuilder.build(), bw, RDFFormat.RDFXML);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private List<Element> getServiceCategoryes() {
        try {
            TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL,
                serviceCategoriesQuery);
            TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
            List<Element> elements = new ArrayList<Element>();
            while (tupleQueryResult.hasNext()) {
                BindingSet bs = tupleQueryResult.next();
                // VAR NAMES: ?a ?b ?c ?d
                String a = bs.getBinding("a").getValue().stringValue().replaceAll(" ", "_");
                String b = bs.getBinding("b").getValue().stringValue().replaceAll(" ", "_");
                String c = bs.getBinding("c").getValue().stringValue().replaceAll(" ", "_");
                String d ="";
                if( bs.getBinding("d")!=null) {
                    d = bs.getBinding("d").getValue().stringValue().replaceAll(" ", "_");
                }
                String context = bs.getBinding("g").getValue().stringValue().replaceAll(" ", "_");

                Element element = new Element(a + "." + b + "." + c + "." + d, a + "." + b + "." + c + "." + d,
                    context);
                elements.add(element);
            }
            return elements;
        } catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
            log.error("Error indexing elements", e);
        }
        return null;
    }

    public void initializeOntology() throws ToreadorOntoEditorException {
        // TODO Auto-generated method stub
    }

    public List<Element> getPossibleObjects(String individual) throws ToreadorOntoEditorException {
        if (individual.equals(serviceCategoriesProperty))
            return getServiceCategoryes();
        else
            return ontologyService.getPossibleObjects(individual);
    }
}
