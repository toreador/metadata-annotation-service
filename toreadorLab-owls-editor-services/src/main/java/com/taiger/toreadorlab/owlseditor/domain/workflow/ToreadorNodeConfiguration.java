package com.taiger.toreadorlab.owlseditor.domain.workflow;

import java.io.Serializable;

public class ToreadorNodeConfiguration implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7945891020191828772L;
	
	private String condition;
	
	private int iterations;
	
	private String ifTrue;
	
	private String ifFalse;
	
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getIfTrue() {
		return ifTrue;
	}
	public void setIfTrue(String ifTrue) {
		this.ifTrue = ifTrue;
	}
	public String getIfFalse() {
		return ifFalse;
	}
	public void setIfFalse(String ifFalse) {
		this.ifFalse = ifFalse;
	}
	public int getIterations() {
		return iterations;
	}
	public void setIterations(int iterations) {
		this.iterations = iterations;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((condition == null) ? 0 : condition.hashCode());
		result = prime * result + ((ifFalse == null) ? 0 : ifFalse.hashCode());
		result = prime * result + ((ifTrue == null) ? 0 : ifTrue.hashCode());
		result = prime * result + iterations;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ToreadorNodeConfiguration other = (ToreadorNodeConfiguration) obj;
		if (condition == null) {
			if (other.condition != null)
				return false;
		} else if (!condition.equals(other.condition))
			return false;
		if (ifFalse == null) {
			if (other.ifFalse != null)
				return false;
		} else if (!ifFalse.equals(other.ifFalse))
			return false;
		if (ifTrue == null) {
			if (other.ifTrue != null)
				return false;
		} else if (!ifTrue.equals(other.ifTrue))
			return false;
		if (iterations != other.iterations)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ToreadorNodeConfiguration [condition=" + condition + ", iterations=" + iterations + ", ifTrue=" + ifTrue
				+ ", ifFalse=" + ifFalse + "]";
	}
}
