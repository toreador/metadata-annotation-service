package com.taiger.toreadorlab.owlseditor.domain.declarative;

import java.io.Serializable;

public class Person implements Serializable {

	/**
	 * Person definition calss
	 */
	private static final long serialVersionUID = 1927085680105014509L;

	private String givenName;
	private String familyName;
	private String affiliation;
	private String role;


	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getAffiliation() {
		return affiliation;
	}

	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getRole(){
		return this.role;
	}
	
}
