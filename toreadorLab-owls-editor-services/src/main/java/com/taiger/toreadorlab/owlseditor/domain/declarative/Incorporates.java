package com.taiger.toreadorlab.owlseditor.domain.declarative;

import java.io.Serializable;
import java.util.List;

public abstract class Incorporates<T> extends Objective implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8842135609950137870L;

	public abstract List<T> getIncorporates();

	public abstract void setIncorporates(List<T> incorporates);

	public abstract void addIncorporate(T incorporate);

}
