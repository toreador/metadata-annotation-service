package com.taiger.toreadorlab.owlseditor.domain;

import java.io.Serializable;
import java.util.List;

public class Services implements Serializable{
	/**
	 * Container for services descriptions
	 */
	private static final long serialVersionUID = 1L;
	private String context;
	private String id;

	private List<ServiceDescription> services;


	public List<ServiceDescription> getServices() {
		return services;
	}
	public void setServices(List<ServiceDescription> services) {
		this.services = services;
	}

	@Override
	public String toString() {
		return "{ \"services\": "+services.toString()+"}";
	}

	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}


}
