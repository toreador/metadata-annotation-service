package com.taiger.toreadorlab.owlseditor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.taiger.toreadorlab.owlseditor.domain.DeclarativeModel;

/**
 * Spring Data JPA repository for the DeclarativeModel entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DeclarativeModelRepository extends JpaRepository<DeclarativeModel,Long> {

    @Query("select declarative_model from DeclarativeModel declarative_model where declarative_model.user.login = ?#{principal.username}")
    List<DeclarativeModel> findByUserIsCurrentUser();

    @Query("select dm from DeclarativeModel dm where dm.user.id = ?1")
    List<DeclarativeModel> findByUserId(long id);


}
