package com.taiger.toreadorlab.owlseditor.domain.workflow;

public class ToreadorConstantStrings {

	// service type
	public static final String WORKFLOW_SERVICE_TYPE_SERVICE = "SERVICE";
	public static final String WORKFLOW_SERVICE_TYPE_OPERATOR = "OPERATOR";
	public static final String WORKFLOW_SERVICE_TYPE_START = "START";
	public static final String WORKFLOW_SERVICE_TYPE_END = "STOP";
	// operator type
	public static final String WORKFLOW_OPERATOR_TYPE_SEQUENCE = "SEQUENCE";
	public static final String WORKFLOW_OPERATOR_TYPE_SPLIT = "SPLIT";
	public static final String WORKFLOW_OPERATOR_TYPE_JOIN = "JOIN";
	public static final String WORKFLOW_OPERATOR_TYPE_SPLITJOIN = "SPLITJOIN";
	public static final String WORKFLOW_OPERATOR_TYPE_ANYORDER = "ANYORDER";
	public static final String WORKFLOW_OPERATOR_TYPE_CHOICE = "CHOICE";
	public static final String WORKFLOW_OPERATOR_TYPE_IF = "IF";
	public static final String WORKFLOW_OPERATOR_TYPE_ITERATE = "ITERATE";
	public static final String WORKFLOW_OPERATOR_TYPE_REPEATWHILE =  "REPEATWHILE";
	public static final String WORKFLOW_OPERATOR_TYPE_REPEATUNTIL = "REPEATUNTIL";

}
