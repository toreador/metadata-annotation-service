package com.taiger.toreadorlab.owlseditor.domain;

import java.io.Serializable;

public class Library implements Serializable {
	/**
		 * 
		 */
	private static final long serialVersionUID = 66566651234112341L;

	private String id;
	private String artifactId;
	private String groupId;
	private String version;

	public String getArtifactId() {
		return artifactId;
	}

	public void setArtifactId(String artifactId) {
		this.artifactId = artifactId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "{ \"artifactId\": \"" + artifactId + "\"\n" + " \"groupId\": \"" + groupId + "\"\n"
				+ " \"version\": \"" + version + "\"\n}";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
