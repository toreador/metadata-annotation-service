package com.taiger.toreadorlab.owlseditor.service;

import java.io.Serializable;

public class ServiceUser implements Serializable {
    private int id;
    private int idUser;
    private String idGraph;
    private String graph;
    private String className;
    private String altLabel;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getIdGraph() {
        return idGraph;
    }

    public void setIdGraph(String idGraph) {
        this.idGraph = idGraph;
    }

    public String getGraph() {
        return graph;
    }

    public void setGraph(String graph) {
        this.graph = graph;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getAltLabel() {
        return altLabel;
    }

    public void setAltLabel(String altLabel) {
        this.altLabel = altLabel;
    }

    @Override
    public String toString() {
        return "UserService{" +
            "id=" + id +
            ", idUser=" + idUser +
            ", idGraph='" + idGraph + '\'' +
            ", graph='" + graph + '\'' +
            ", className='" + className + '\'' +
            '}';
    }
}
