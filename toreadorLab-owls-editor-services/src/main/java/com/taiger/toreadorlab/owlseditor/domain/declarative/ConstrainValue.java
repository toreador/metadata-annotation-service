package com.taiger.toreadorlab.owlseditor.domain.declarative;

import java.io.Serializable;

public class ConstrainValue implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 23L;
	private String value;
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
