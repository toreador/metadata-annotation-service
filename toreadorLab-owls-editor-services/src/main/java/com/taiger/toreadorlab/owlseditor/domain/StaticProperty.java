package com.taiger.toreadorlab.owlseditor.domain;

import java.io.Serializable;

public class StaticProperty implements Serializable
{
    private String id;

    private String name= "StaticProperty";

    private String externalized;

    private String description;

    private String mandatory;

    private String value;

    private String type;

    private String defaultValue;

    private String uri;

    private String key;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExternalized ()
    {
        return externalized;
    }

    public void setExternalized (String externalized)
    {
        this.externalized = externalized;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getMandatory ()
    {
        return mandatory;
    }

    public void setMandatory (String mandatory)
    {
        this.mandatory = mandatory;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getDefaultValue ()
    {
        return defaultValue;
    }

    public void setDefaultValue (String defaultValue)
    {
        this.defaultValue = defaultValue;
    }

    public String getUri ()
    {
        return uri;
    }

    public void setUri (String uri)
    {
        this.uri = uri;
    }

    public String getKey ()
    {
        return key;
    }

    public void setKey (String key)
    {
        this.key = key;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", externalized = "+externalized+", description = "+description+", mandatory = "+mandatory+", value = "+value+", type = "+type+", defaultValue = "+defaultValue+", uri = "+uri+", key = "+key+"]";
    }
}

