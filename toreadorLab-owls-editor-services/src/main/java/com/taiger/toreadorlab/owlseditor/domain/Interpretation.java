package com.taiger.toreadorlab.owlseditor.domain;

import java.util.Map;

/**
 * The meaning of a message, including Intent and elements.
 */
public class Interpretation {

    public enum Intent {
        QUESTION, INFORMATION, ORDER, GREETING
    }

    private Intent intent;
    private String pattern;
    private Map<String, String> bindings;

    public Intent getIntent() {
        return intent;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public Map<String, String> getBindings() {
        return bindings;
    }

    public void setBindings(Map<String, String> bindings) {
        this.bindings = bindings;
    }

    @Override
    public String toString() {
        return "Interpretation{" +
                "intent='" + intent + "'" +
                ", pattern='" + pattern + "'" +
                ", bindings='" + bindings + "'" +
                "}";
    }
}
