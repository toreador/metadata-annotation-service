package com.taiger.toreadorlab.owlseditor.domain.declarative;

import java.io.Serializable;

public class DataSource implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4818675197216548387L;
	private String identifier;
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
}
