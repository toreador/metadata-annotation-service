package com.taiger.toreadorlab.owlseditor.domain;

import java.util.List;

public class PropertyValues {

    private Element property;
    private List<Element> values;
    
    public PropertyValues() {}

    @Override
    public String toString() {
        return "Element{" +
                "property='" + property + '\'' +
                ", values='" + values + '\'' +
                '}';
    }

    public Element getProperty() {
        return property;
    }

    public void setProperty(Element property) {
        this.property = property;
    }

    public List<Element> getValues() {
        return values;
    }

    public void setValues(List<Element> values) {
        this.values = values;
    }

}
