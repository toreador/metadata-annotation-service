package com.taiger.toreadorlab.owlseditor.domain;

import java.io.Serializable;

public class ApplicationProperty implements Serializable
{
    private String id;

    private String name= "ApplicationProperty";

    private String description;

    private Boolean mandatory;

    private String value;

    private String type;

    private String defaultValue;

    private String outputData;

    private String inputData;

    private String key;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public Boolean isMandatory ()
    {
        return mandatory;
    }

    public void setMandatory (Boolean mandatory)
    {
        this.mandatory = mandatory;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getDefaultValue ()
    {
        return defaultValue;
    }

    public void setDefaultValue (String defaultValue)
    {
        this.defaultValue = defaultValue;
    }

    public String getOutputData ()
    {
        return outputData;
    }

    public void setOutputData (String outputData)
    {
        this.outputData = outputData;
    }

    public String getInputData ()
    {
        return inputData;
    }

    public void setInputData (String inputData)
    {
        this.inputData = inputData;
    }

    public String getKey ()
    {
        return key;
    }

    public void setKey (String key)
    {
        this.key = key;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", description = "+description+", mandatory = "+mandatory+", value = "+value+", type = "+type+", defaultValue = "+defaultValue+", outputData = "+outputData+", inputData = "+inputData+", key = "+key+"]";
    }
}

