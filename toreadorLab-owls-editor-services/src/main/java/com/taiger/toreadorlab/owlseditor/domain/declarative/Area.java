package com.taiger.toreadorlab.owlseditor.domain.declarative;

import java.io.Serializable;
import java.util.List;

public abstract class Area<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String label;
	
	private String id;
	
	private String type;
	
	protected List<T> incorporates;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public List<T> getIncorporates() {
		return incorporates;
	}

	public void setIncorporates(List<T> incorporates) {
		this.incorporates = incorporates;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
