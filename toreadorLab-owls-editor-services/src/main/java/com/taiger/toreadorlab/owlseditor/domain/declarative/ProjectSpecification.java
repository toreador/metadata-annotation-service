package com.taiger.toreadorlab.owlseditor.domain.declarative;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProjectSpecification implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8866494749251295046L;

	private Person author;
	private String description;
	private String identifier;
	private List<String> additionalType;
	private List<DataSource> targetDataSource;
	private String title;

	public Person getAuthor() {
		return author;
	}

	public void setAuthor(Person author) {
		this.author = author;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public List<DataSource> getTargetDataSource() {
		return targetDataSource;
	}

	public void setTargetDataSource(List<DataSource> targetDataSource) {
		this.targetDataSource = targetDataSource;
	}

	public List<String> getAdditionalType() {
		return additionalType;
	}

	public void setAdditionalType(List<String> additionalType) {
		this.additionalType = additionalType;
	}

	public void addAdditionalType(String additionalType) {
		if (this.additionalType == null)
			this.additionalType = new ArrayList<>();
		this.additionalType.add(additionalType);
	}

	public void addTargetDataSource(String s) {
		DataSource ds = new DataSource();
		ds.setIdentifier(s);
		if (this.targetDataSource == null)
			this.targetDataSource = new ArrayList<>();
		this.targetDataSource.add(ds);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
