package com.taiger.toreadorlab.owlseditor.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang.StringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.URI;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.query.Binding;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.GraphQuery;
import org.eclipse.rdf4j.query.GraphQueryResult;
import org.eclipse.rdf4j.query.MalformedQueryException;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.query.Update;
import org.eclipse.rdf4j.query.UpdateExecutionException;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.eclipse.rdf4j.rio.RDFHandler;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.rdfxml.util.RDFXMLPrettyWriterFactory;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.taiger.toreadorlab.owlseditor.config.BidirectionalPropertiesCofig;
import com.taiger.toreadorlab.owlseditor.config.OntologyFiles;
import com.taiger.toreadorlab.owlseditor.domain.Element;
import com.taiger.toreadorlab.owlseditor.domain.Parameter;
import com.taiger.toreadorlab.owlseditor.domain.PropertyValues;
import com.taiger.toreadorlab.owlseditor.domain.declarative.Constrain;
import com.taiger.toreadorlab.owlseditor.domain.declarative.ToreadorService;
import com.taiger.toreadorlab.owlseditor.service.util.SparqlUtils;
import com.taiger.toreadorlab.owlseditor.web.rest.errors.ExceptionData;
import com.taiger.toreadorlab.owlseditor.web.rest.errors.ToreadorOntoEditorException;

@Service
public class OntologyService {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private ValueFactory valueFactory = SimpleValueFactory.getInstance();

	@Value("${iconverse.resources}")
	private String resourcesPath;

	// Sesame
	private Repository repository;
	private RepositoryConnection repositoryConnection;
	@Value("${iconverse.ontology.mode}")
	private String ontologyMode;
	@Value("${iconverse.ontology.base" + "UriOnly}")
	private boolean baseUriOnly;
	@Value("${iconverse.ontology.sparqlendpoint.select}")
	private String sparqlSelectEndpoint;
	@Value("${iconverse.ontology.sparqlendpoint.update}")
	private String sparqlUpdateEndpoint;
	@Value("${iconverse.ontology.baseURI}")
	private String baseURI;
	@Value("${iconverse.ontology.ontologyFolder}")
	private String ontologyFolder;
	@Autowired
	private OntologyFiles ontologyFile;

	@Value("${iconverse.ontology.serviceCatalogueEndpoint}")
	private String serviceCatalogueEndpoint;
	@Value("${iconverse.ontology.serviceCatalogueUsername}")
	private String serviceCatalogueEndpointUser;
	@Value("${iconverse.ontology.serviceCataloguePassword}")
	private String serviceCatalogueEndpointPassword;

	@Value("${iconverse.resourcesOntology}")
	private String resourcesOntology;
	// @Value("${ontology.baseURI}")
	// private String baseURI;
	// @Value("${ontology.ontologyPath}")
	// private String ontologyPath;

	// private String currentOntology;
	@Value("${spring.datasource.password}")
	private String pass;

//	@Autowired
//	ElasticsearchService elasticsearchService;

	@Autowired
	BidirectionalPropertiesCofig bidirectionalProperties;

	@PostConstruct
	public void init() throws UnknownHostException {
		// init Sesame repository
		try {
			if ("memory".equals(ontologyMode)) {
				initMemoryRepository();
			} else if ("sparql".equals(ontologyMode)) {
				initSparqlRepository();
			}
		} catch (RepositoryException | RDFParseException | IOException e) {
			log.error("Error initializing ontology repository", e);
		}

	}

	private void initMemoryRepository() throws RepositoryException, RDFParseException, IOException {
		repository = new SailRepository(new MemoryStore());
		repository.initialize();
		repositoryConnection = repository.getConnection();
		// load ontology
		// File ontologyFile = new File(resourcesPath + ontologyPath);
		// repositoryConnection.add(ontologyFile, baseURI, RDFFormat.TURTLE);
		// repositoryConnection.commit();
	}

	private void initSparqlRepository() throws RepositoryException {
		repository = new SPARQLRepository(sparqlSelectEndpoint);
		repository.initialize();
		repositoryConnection = repository.getConnection();
	}

	public void reinit() throws UnknownHostException {
		try {
			repositoryConnection.close();
			repository.shutDown();
		} catch (RepositoryException e) {
			log.error("Error closing ontology repository", e);
		}
		init();
	}

//	public void buildOntologyIndex() {
//		indexElements();
//		// indexElementsOfType("owl:Class");
//		// indexElementsOfType("owl:NamedIndividual");
//		// indexElementsOfType("owl:ObjectProperty");
//		// indexElementsOfType("owl:DatatypeProperty");
//	}
//
//	private void indexElements() {
//		// prefix skos: <http://www.w3.org/2004/02/skos/core#>select ?g ?s ?t ?l
//		// ?a where { {GRAPH ?g { ?s a ?t. ?s rdfs:label ?l. } } union {GRAPH ?g
//		// { ?s a ?t. ?s skos:altLabel ?a. }}}
//		String query = "prefix skos: <http://www.w3.org/2004/02/skos/core#>select ?g ?s ?t ?l ?a where { {GRAPH ?g  {    ?s a ?t.    ?s rdfs:label ?l. } }  union  {GRAPH ?g {   ?s a ?t.    ?s skos:altLabel ?a.  }}}";
//		try {
//			TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, query);
//			TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
//			Map<String, Element> elements = new HashMap<String, Element>();
//			while (tupleQueryResult.hasNext()) {
//				BindingSet bs = tupleQueryResult.next();
//				String id = bs.getBinding("s").getValue().stringValue();
//				String context = bs.getBinding("g").getValue().stringValue();
//				Binding labelBinding = bs.getBinding("l");
//				String label = (labelBinding != null) ? labelBinding.getValue().stringValue() : null;
//				Binding altLabelBinding = bs.getBinding("a");
//				String altLabel = (altLabelBinding != null) ? altLabelBinding.getValue().stringValue() : null;
//				Binding typeBinding = bs.getBinding("t");
//				String type = (typeBinding != null) ? typeBinding.getValue().stringValue() : null;
//
//				// make elements with id, type, label and aliases (labels and
//				// altLabels)
//				Element element = elements.get(id);
//				if (element == null) {
//					element = new Element();
//					element.setId(id);
//					element.setGraph(context);
//					element.setTypes(new HashSet<Element>());
//					element.setAliases(new HashSet<String>());
//					elements.put(id, element);
//				}
//				if (label != null) {
//					element.setLabel(label);
//					element.getAliases().add(label);
//				}
//				if (altLabel != null) {
//					element.getAliases().add(altLabel);
//				}
//				if (label == null && altLabel != null && element.getLabel() == null) {
//					element.setLabel(altLabel);
//				}
//				if (type != null) {
//					element.getTypes().add(new Element(type, context));
//				}
//			}
//
//			for (Element element : elements.values()) {
//				indexElement(element);
//			}
//			log.info("Indexed {} elements", elements.size());
//		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
//			log.error("Error indexing elements", e);
//		}
//	}

//	private void indexElement(Element element) {
//		ArrayList<String> prepositions = new ArrayList<String>() {
//			{
//				add("a");
//				add("of");
//				add("the");
//				add("at");
//				add("on");
//				add("for");
//				add("to");
//				add("and");
//				add("his");
//				add("her");
//				add("into");
//				add("in");
//			}
//		};
//		List<String> chains = null;
//		if (element.getAliases() != null) {
//			chains = new ArrayList<String>();
//			for (String alias : element.getAliases()) {
//				List<String> tokens = elasticsearchService.getTokens(alias);
//				tokens.removeAll(prepositions);
//				String chain = StringUtils.join(tokens, "_");
//				chains.add(chain);
//			}
//		}
//		elasticsearchService.indexElement(element, chains);
//	}

	public boolean downloadOWLFiles(String path) {
		JSch jsch = new JSch();
		Session session = null;
		String privateKey = resourcesPath + "/redes.pem";
		ChannelSftp sftpChannel = null;
		Channel channel = null;

		/*
		 * String regex = "(?<=[A-Z])(?=[0-9])|(?<=[0-9])(?=[A-Z])"; String
		 * tests = "HadoopINGESTION76ftptohdfs-2.7.2.owl"; String[] s =
		 * tests.split(regex);
		 * 
		 * Pattern VALID_PATTERN = Pattern.compile("[0-9]+|[A-Za-z]+|-[0-9].*");
		 * Matcher matcher = VALID_PATTERN.matcher(tests); while
		 * (matcher.find()) { log.info(matcher.group()); }
		 * log.info("empezamos");
		 */

		try {
			jsch.addIdentity(privateKey);
			session = jsch.getSession("ec2-user", "toreador.hopto.org", 22);
			session.setConfig("StrictHostKeyChecking", "no");

			session.connect();
			Vector<ChannelSftp.LsEntry> lsVec = null;
			channel = session.openChannel("sftp");
			channel.connect();
			sftpChannel = (ChannelSftp) channel;

			lsVec = (Vector<ChannelSftp.LsEntry>) sftpChannel.ls(resourcesOntology); // sftpChannel.lpwd()

			for (int i = 0; i < lsVec.size(); i++) {
				log.info(lsVec.get(i).getFilename().toString());
				String filename = lsVec.get(i).getFilename().toString();
				if (!".".equals(lsVec.get(i).getFilename()) && !"..".equals(lsVec.get(i).getFilename())) {

					/*
					 * ReadableByteChannel in = Channels.newChannel( new
					 * URL("http://toreador.hopto.org:8081/BDMOntologies/" +
					 * filename).openStream());
					 * 
					 * FileChannel out = new FileOutputStream( path +
					 * lsVec.get(i).getFilename()).getChannel();
					 * 
					 * 
					 * out.transferFrom(in, 0, Long.MAX_VALUE);
					 */

					sftpChannel.get(resourcesOntology + filename, path + filename);

				}

			}

		} catch (JSchException e) {
			e.printStackTrace();

		} catch (SftpException e) {
			e.printStackTrace();
		} finally {
			sftpChannel.exit();
			channel.disconnect();
			session.disconnect();
		}

		/*
		 * try( ReadableByteChannel in= Channels.newChannel( new
		 * URL("http://54.229.142.100:8081/BDMOntologies/"+
		 * "SparkANALYTICS53spark-stream-gradientboostedtree-regression-predict-2.1.1.owl"
		 * ).openStream()); FileChannel out=new FileOutputStream(
		 * "/Users/Christian/Documents/gitHub/toreador/lab-Toreador/toreadorLab-owls-editor-resources/ontology/owlFiles/"
		 * +
		 * "SparkANALYTICS53spark-stream-gradientboostedtree-regression-predict-2.1.1.owl"
		 * ).getChannel() ) {
		 * 
		 * out.transferFrom(in, 0, Long.MAX_VALUE); } catch
		 * (FileNotFoundException e) { e.printStackTrace(); } catch
		 * (MalformedURLException e) { e.printStackTrace(); } catch (IOException
		 * e) { e.printStackTrace(); }
		 */

		return true;
	}

	public List<String> queryS(String s) {
		String query = "prefix : <" + getNsFromUri(s) + ">" + "select ?d ?l where {"
		// + " <" + s + "> :hasDescription ?o"
				+ "  optional {<" + s + "> :hasDescription ?d}." // description
				// of S
				+ "  optional {?i a <" + s + ">. ?i rdfs:label ?l}." // instances
				// of S
				+ "  optional {<" + s + "> :isCategoryOf ?i. ?i rdfs:label ?l}." // elements
				// with
				// category
				// S
				+ "}";
		try {
			TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, query);
			TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
			List<String> values = new ArrayList<String>();
			while (tupleQueryResult.hasNext()) {
				BindingSet bs = tupleQueryResult.next();
				String value = null;
				if (bs.getBinding("d") != null) {
					value = bs.getBinding("d").getValue().stringValue();
				} else if (bs.getBinding("l") != null) {
					value = bs.getBinding("l").getValue().stringValue();
				}
				values.add(value);
			}
			return values;
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			log.error("Error in queryDescription", e);
		}
		return null;
	}

	public List<String> querySP(String s, String p) {
		String query = "prefix : <" + getNsFromUri(s) + ">" + "select ?o ?l where {" + "  <" + s + "> <" + p + "> ?o"
				+ "  optional { ?o rdfs:label ?l }" + "}";
		try {
			TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, query);
			TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
			List<String> values = new ArrayList<String>();
			while (tupleQueryResult.hasNext()) {
				BindingSet bs = tupleQueryResult.next();
				String value = null;
				if (bs.getBinding("l") != null) {
					value = bs.getBinding("l").getValue().stringValue();
				} else if (bs.getBinding("o") != null) {
					value = bs.getBinding("o").getValue().stringValue();
				}
				values.add(value);
			}
			return values;
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			log.error("Error in queryDescription", e);
		}
		return null;
	}

	public List<String> queryMoreSSP(String s1, String s2, String p) {
		String query = "prefix : <" + getNsFromUri(p) + ">" + "select ?l where {" + "  {" + "    <" + s1 + "> <" + p
				+ "> ?o." + "    bind(<" + s1 + "> as ?s)." + "    optional { ?s rdfs:label ?l }." + "  }" + "  union"
				+ "  {" + "    <" + s1 + "> ?hasSpec ?spec." + "    ?spec <" + p + "> ?o." // get
				// property
				// value
				// from
				// the
				// Specification
				+ "    bind(<" + s1 + "> as ?s)." + "    optional { ?s rdfs:label ?l }." + "  }" + "  union" + "  {"
				+ "    <" + s2 + "> <" + p + "> ?o." + "    bind(<" + s2 + "> as ?s)."
				+ "    optional { ?s rdfs:label ?l }." + "  }" + "  union" + "  {" + "    <" + s2 + "> ?hasSpec ?spec."
				+ "    ?spec <" + p + "> ?o." // get property value from the
				// Specification
				+ "    bind(<" + s2 + "> as ?s)." + "    optional { ?s rdfs:label ?l }." + "  }" + "}"
				+ "order by desc(?o) limit 1";
		// log.info("SPARQL: {}", query);
		try {
			TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, query);
			TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
			List<String> values = new ArrayList<String>();
			while (tupleQueryResult.hasNext()) {
				BindingSet bs = tupleQueryResult.next();
				String value = null;
				if (bs.getBinding("l") != null) {
					value = bs.getBinding("l").getValue().stringValue();
				} else if (bs.getBinding("s") != null) {
					value = bs.getBinding("s").getValue().stringValue();
				}
				values.add(value);
			}
			return values;
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			log.error("Error in queryDescription", e);
		}
		return null;
	}

	public List<String> queryLessSSP(String s1, String s2, String p) {
		String query = "prefix : <" + getNsFromUri(p) + ">" + "select ?l where {" + "  {" + "    <" + s1 + "> <" + p
				+ "> ?o." + "    bind(<" + s1 + "> as ?s)." + "    optional { ?s rdfs:label ?l }." + "  }" + "  union"
				+ "  {" + "    <" + s1 + "> ?hasSpec ?spec." + "    ?spec <" + p + "> ?o." // get
				// property
				// value
				// from
				// the
				// Specification
				+ "    bind(<" + s1 + "> as ?s)." + "    optional { ?s rdfs:label ?l }." + "  }" + "  union" + "  {"
				+ "    <" + s2 + "> <" + p + "> ?o." + "    bind(<" + s2 + "> as ?s)."
				+ "    optional { ?s rdfs:label ?l }." + "  }" + "  union" + "  {" + "    <" + s2 + "> ?hasSpec ?spec."
				+ "    ?spec <" + p + "> ?o." // get property value from the
				// Specification
				+ "    bind(<" + s2 + "> as ?s)." + "    optional { ?s rdfs:label ?l }." + "  }" + "}"
				+ "order by asc(?o) limit 1";
		// log.info("SPARQL: {}", query);
		try {
			TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, query);
			TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
			List<String> values = new ArrayList<String>();
			while (tupleQueryResult.hasNext()) {
				BindingSet bs = tupleQueryResult.next();
				String value = null;
				if (bs.getBinding("l") != null) {
					value = bs.getBinding("l").getValue().stringValue();
				} else if (bs.getBinding("s") != null) {
					value = bs.getBinding("s").getValue().stringValue();
				}
				values.add(value);
			}
			return values;
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			log.error("Error in queryDescription", e);
		}
		return null;
	}

	public boolean assignServiceToUser(int idUser, String idGraph, String graph, String className) {
		String myDriver = "com.zaxxer.hikari.HikariDataSource";
		String myUrl = "jdbc:mysql://localhost:3306/toreadorLabOwlsEditorServices?useUnicode=true&characterEncoding=utf8&useSSL=false";
		// create table useServices (id int(10) auto_increment primary key,
		// idUser int(10), idGraph varchar(2000), graph varchar(2000), className
		// varchar(2000));
		String query = "delete from userServices where idUser=" + idUser + " and idGraph='" + idGraph + "' and graph='"
				+ graph + "' and className='" + className + "';";
		// query="insert into userServices values (NULL,"+idUser+",
		// '"+idGraph+"', '"+graph+"', '"+className+"');";
		log.info(query);
		try {
			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", pass);
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.execute();
			query = "insert into userServices values (NULL," + idUser + ", '" + idGraph + "', '" + graph + "', '"
					+ className + "');";
			preparedStmt = conn.prepareStatement(query);
			preparedStmt.execute();
			conn.close();
			return true;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean deleteAssignedUserService(int idUser, String idGraph, String graph, String className) {
		String myDriver = "com.zaxxer.hikari.HikariDataSource";
		String myUrl = "jdbc:mysql://localhost:3306/toreadorLabOwlsEditorServices?useUnicode=true&characterEncoding=utf8&useSSL=false";
		// create table useServices (id int(10) auto_increment primary key,
		// idUser int(10), idGraph varchar(2000), graph varchar(2000), className
		// varchar(2000));
		String query = "delete from userServices where idUser=" + idUser + " and idGraph='" + idGraph + "' and graph='"
				+ graph + "' and className='" + className + "';";
		// query="insert into userServices values (NULL,"+idUser+",
		// '"+idGraph+"', '"+graph+"', '"+className+"');";
		log.info(query);
		try {
			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", pass);
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.execute();
			conn.close();
			return true;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public List<ServiceUser> getServiceToUser(int idUser) {
		String myDriver = "com.zaxxer.hikari.HikariDataSource";
		String myUrl = "jdbc:mysql://localhost:3306/toreadorLabOwlsEditorServices?useUnicode=true&characterEncoding=utf8&useSSL=false";
		// create table useServices (id int(10) auto_increment primary key,
		// idUser int(10), idGraph varchar(2000), graph varchar(2000), className
		// varchar(2000));
		String queryAux2 = "select * from userServices where idUser=" + idUser + ";";
		log.info(queryAux2);
		try {
			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", pass);
			// create the java statement
			java.sql.Statement st = conn.createStatement();

			// execute the query, and get a java resultset
			ResultSet rs = st.executeQuery(queryAux2);
			List<ServiceUser> userServices = new ArrayList<>();
			while (rs.next()) {
				ServiceUser s = new ServiceUser();
				s.setId(rs.getInt("id"));
				s.setIdUser(rs.getInt("idUser"));
				s.setIdGraph(rs.getString("idGraph"));
				s.setGraph(rs.getString("graph"));
				s.setClassName(rs.getString("className"));
				// s.setAltLabel(rs.getString("altLabel"));
				userServices.add(s);

			}

			st.close();
			return userServices;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getNsFromUri(String uri) {
		return uri.substring(0, uri.indexOf("#") + 1);
	}


	public List<Element> getClasses() {
		String query = "prefix owl: <http://www.w3.org/2002/07/owl#>" + "select ?g ?s ?l where {GRAPH ?g {"
				+ "  ?s a owl:Class." + "  optional {?s rdfs:label ?l}." + "FILTER (!isBlank(?s))}} order by ?s ?l";
		try {
			TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, query);
			TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
			List<Element> elements = new ArrayList<Element>();
			while (tupleQueryResult.hasNext()) {
				BindingSet bs = tupleQueryResult.next();
				String id = bs.getBinding("s").getValue().stringValue();
				String context = bs.getBinding("g").getValue().stringValue();
				Binding labelBinding = bs.getBinding("l");
				String label = (labelBinding != null) ? labelBinding.getValue().stringValue() : id;// .replaceAll(baseURI,
				// "");

				if (baseUriOnly) {
					if (id.startsWith(baseURI)) {
						Element element = new Element(id, label, context);
						elements.add(element);
					}
				} else {
					Element element = new Element(id, label, context);
					elements.add(element);
				}
			}
			return elements;
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			log.error("Error indexing elements", e);
		}
		return null;
	}

	public List<String> getServicesNames() throws ToreadorOntoEditorException {
		String query = "SELECT DISTINCT ?label WHERE{ ?aux rdfs:range ?class . ?a a ?class1 . ?class1 rdfs:subClassOf* ?class. optional {?a rdfs:label ?label . }. {SELECT ?g ?a {GRAPH ?g {?a rdf:type ?class1.}}}}";
		log.debug(query);
		try {
			TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, query);
			TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
			List<String> elements = new ArrayList<String>();
			while (tupleQueryResult.hasNext()) {
				BindingSet bs = tupleQueryResult.next();
				Binding labelBinding = bs.getBinding("label");
				if (labelBinding != null) {
					String label = labelBinding.getValue().stringValue();
					elements.add(label);
				}

			}
			return elements;
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			log.error("Error indexing elements", e);
		}
		return null;
	}

	public List<Element> getIndividuals(String cls) {
		String query = "prefix owl: <http://www.w3.org/2002/07/owl#>" + "select ?g ?s ?l where { GRAPH ?g {"
				+ "  ?s a ?class1 . ?class1 rdfs:subClassOf* <" + cls + ">." + "  optional {?s rdfs:label ?l}."
				+ "}} order by ?l ?s";
		log.info("QUERY: " + query);
		try {
			TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, query);
			TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
			List<Element> elements = new ArrayList<Element>();
			while (tupleQueryResult.hasNext()) {
				BindingSet bs = tupleQueryResult.next();
				String id = bs.getBinding("s").getValue().stringValue();
				String context = bs.getBinding("g").getValue().stringValue();
				Binding labelBinding = bs.getBinding("l");
				String label = (labelBinding != null) ? labelBinding.getValue().stringValue()
						: id.replaceAll(baseURI, "");
				if (baseUriOnly) {
					if (id.startsWith(baseURI)) {
						Element element = new Element(id, label, context);
						elements.add(element);
					}
				} else {
					Element element = new Element(id, label, context);
					elements.add(element);
				}
			}
			return elements;
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			log.error("Error indexing elements", e);
		}
		return null;
	}

	public List<PropertyValues> getPropertyValues(String individual) {
		String query = "prefix owl: <http://www.w3.org/2002/07/owl#>" + "select ?g ?p ?pl ?o ?ol where { graph ?g {"
				+ "  <" + individual + "> ?p ?o." + "  optional {?p rdfs:label ?pl}."
				+ "  optional {?o rdfs:label ?ol}." + "} } order by ?pl ?p ?ol ?o";
		try {
			TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, query);
			TupleQueryResult tupleQueryResult = tupleQuery.evaluate();

			Map<String, PropertyValues> propertyValuesMap = new HashMap<String, PropertyValues>();
			List<PropertyValues> propertyValuesList = new ArrayList<PropertyValues>();
			PropertyValues propertyValues = null;

			while (tupleQueryResult.hasNext()) {
				BindingSet bs = tupleQueryResult.next();
				String property = bs.getBinding("p").getValue().stringValue();
				String graph = bs.getBinding("g").getValue().stringValue();
				org.eclipse.rdf4j.model.Value objectValue = bs.getBinding("o").getValue();
				Binding propertyLabelBinding = bs.getBinding("pl");
				String propertyLabel = (propertyLabelBinding != null) ? propertyLabelBinding.getValue().stringValue()
						: null;
				Binding objectLabelBinding = bs.getBinding("ol");
				String objectLabel = (objectLabelBinding != null) ? objectLabelBinding.getValue().stringValue() : null;

				propertyValues = propertyValuesMap.get(property);
				if (propertyValues == null) {
					propertyValues = new PropertyValues();
					propertyValues.setProperty(new Element(property, propertyLabel, graph));
					List<Element> values = new ArrayList<Element>();
					propertyValues.setValues(values);
					propertyValuesMap.put(property, propertyValues);
					propertyValuesList.add(propertyValues);
				}
				propertyValues.getValues().add(makeResourceOrLiteral(objectValue, objectLabel, graph));
			}
			return propertyValuesList;
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			log.error("Error indexing elements", e);
		}
		return null;
	}

	private Element makeResourceOrLiteral(org.eclipse.rdf4j.model.Value objectValue, String objectLabel, String graph) {
		if (objectValue instanceof URI) {
			return new Element(objectValue.stringValue(), objectLabel, graph);
		} else if (objectValue instanceof Literal) {
			return new Element(objectValue.stringValue(), objectValue.stringValue(), graph);
		}
		return null;
	}

	public void clearOntology() {
		String url = UriComponentsBuilder.fromUriString(sparqlUpdateEndpoint).build().toUriString();
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.delete(url);
	}

	public void loadOntology() {
		for (String ontology : ontologyFile.getFiles()) {
			// String ontology = currentOntology != null ? currentOntology :
			// ontologyFile;
			String ontologyUri = ontology.startsWith("http") ? ontology
					: "file://" + resourcesPath + ontologyFolder + ontology;
			// String url =
			// UriComponentsBuilder.fromUriString(sparqlUpdateEndpoint).queryParam("uri",
			// ontologyUri).queryParam("context-uri", baseURI).build()
			// .toUriString();
			log.info("LoAdInG OnToLoGy in triple Store: " + ontologyUri);
			String url = UriComponentsBuilder.fromUriString(sparqlUpdateEndpoint).queryParam("uri", ontologyUri).build()
					.toUriString();
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<Object> postForEntity = restTemplate.postForEntity(url, null, null);
			log.info("Service status: " + postForEntity.getStatusCode());
			if (postForEntity.getStatusCode().equals(HttpStatus.OK))
				log.info("Ontology Loaded");
			else
				log.warn("Ontology non Loaded!!");
		}
	}

	public void reloadOntology() {
		clearOntology();
		loadOntology();
	}

	public static String getIdFromUri(String uri) {
		return uri.substring(uri.indexOf("#") + 1);
	}

	public boolean checkIndividualExists(String id)
			throws MalformedQueryException, RepositoryException, QueryEvaluationException {
		String query = "SELECT ?a ?o " + "WHERE{" + "  " + SparqlUtils.toUri(id) + " a ?o " + "}";
		log.debug(query);
		TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, query);
		TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
		log.debug(tupleQueryResult.hasNext() + "");
		return tupleQueryResult.hasNext();
	}

	public String generatePropertiesQuery(String id, List<PropertyValues> individual) {

		String idUri = SparqlUtils.toUri(id);
		String query = "";
		// Add a row for each of the properties
		for (PropertyValues property : individual) {
			String propertyId = property.getProperty().getId();

			// Prepare to create the inverse property
			String correspondingProperty = null;
			for (String pair : bidirectionalProperties.getProperties()) {
				String[] properties = pair.split(",");

				if (properties.length != 2) {
					continue;
				} else if (propertyId.endsWith("#" + properties[0])) {
					correspondingProperty = properties[1];
				} else if (propertyId.endsWith("#" + properties[1])) {
					correspondingProperty = properties[0];
				}
			}

			// Add an objet for each of the values
			Set<String> values = new HashSet<>();
			String correspondingValues = "";
			for (Element element : property.getValues()) {
				if (element.getId() != null && !element.getId().isEmpty()) {
					// Add URI
					values.add("<" + element.getId() + ">");
					if (correspondingProperty != null) {
						correspondingValues += "<" + element.getId() + "> <" + baseURI + correspondingProperty + "> "
								+ idUri + " . ";
					}
				} else if (element.getLabel() != null && !element.getLabel().isEmpty()) {
					// Add literal
					values.add(valueFactory.createLiteral(SparqlUtils.escape(element.getLabel()), "en").toString());
				}
			}
			if (propertyId.endsWith("#type")) {
				// Add the NamedIndividual type
				values.add("<http://www.w3.org/2002/07/owl#NamedIndividual>");
			}

			if (!values.isEmpty()) {
				// First part of the sentence
				query += idUri + " <" + propertyId + "> ";
				// Finally add the objets
				query += StringUtils.join(values, ", ") + ". " + correspondingValues;
			}
		}
		return query;
	}

	public void executeUpdate(String... queries)
			throws MalformedQueryException, RepositoryException, UpdateExecutionException {
		Update update;
		for (String query : queries) {
			update = repositoryConnection.prepareUpdate(QueryLanguage.SPARQL, query);
			update.execute();
		}
////		elasticsearchService.clearOntologyIndex();
//		buildOntologyIndex();
	}

	public Element createIndividual(List<PropertyValues> individual, String context)
			throws ToreadorOntoEditorException {
		// First, check that exists and get the label (to make id and check
		// existence of individual)
		String label = null;
		// add graph name is provided
		String graph = "";
		if (context != null) {
			graph = " GRAPH <" + context + "> ";
		}
		for (PropertyValues property : individual) {
			if (property.getProperty().getId().endsWith("#label") && property.getValues().size() == 1) {
				label = SparqlUtils.escape(property.getValues().get(0).getLabel().trim());
				break;
			}
		}
		if (label == null) {
			log.warn("Label must not be null");
			throw new ToreadorOntoEditorException(HttpStatus.BAD_REQUEST, new ExceptionData("Label must not be null"));
		}

		String id = SparqlUtils.idFromLabel(label, baseURI);

		try {
			// Check that the individual does not exist before insert
			if (!checkIndividualExists(id)) {

				String query = "prefix owl: <http://www.w3.org/2002/07/owl#> " + "prefix : <" + baseURI + "> "
						+ "INSERT DATA { " + graph + " { ";
				// Add the properties
				query += generatePropertiesQuery(id, individual);
				query += " }}";
				log.debug(query);
				// TODO check update ok
				executeUpdate(query);
//				Element element = elasticsearchService.getElementById(id, context);
//				if (element == null) {
//					element = new Element(id, context);
//				}
				Element element = new Element(id, context);
				log.info("Individual created");
				return element;
			} else {
				log.info("Individual already exists");
				throw new ToreadorOntoEditorException(HttpStatus.CONFLICT,
						new ExceptionData("Individual already exists"));
			}
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException
				| UpdateExecutionException e) {
			log.error("Error creating individual", e);
			throw new ToreadorOntoEditorException(e, HttpStatus.INTERNAL_SERVER_ERROR,
					new ExceptionData("Unexpected error"));
		}
	}

	public Element updateIndividual(String id, List<PropertyValues> individual, String context)
			throws ToreadorOntoEditorException {
		String idUri = SparqlUtils.toUri(id);
		try {
			// Check that the individual does not exist before insert
			if (checkIndividualExists(id)) {
				// add graph name is provided

				String graph = "";
				if (context != null) {
					graph = " GRAPH <" + context + "> ";
				}
				String query = "prefix owl: <http://www.w3.org/2002/07/owl#> " + "prefix : <" + baseURI + "> "
						+ "DELETE { " + graph + "{  " + idUri + " ?p ?o " + "}} " + "INSERT { " + graph + " {";
				// Add the properties

				query += generatePropertiesQuery(id, individual);
				query += " }} WHERE { " + graph + " {" + idUri + " ?p ?o }}";
				Optional<PropertyValues> findFirst = individual.stream()
						.filter(p -> p.getProperty().getId().equals("http://www.w3.org/2000/01/rdf-schema#label"))
						.findFirst();
				String label = "";
				if (findFirst.isPresent()) {
					PropertyValues propertyValues = findFirst.get();
					List<Element> values = propertyValues.getValues();
					Element element = values.get(0);
					String[] split = element.getLabel().split("ServiceCategory");
					label = split[0];
				}
				log.debug(query);
				// TODO check update ok
				executeUpdate(query);
//				Element element = elasticsearchService.getElementById(id, context);
//				if (element == null) {
//					element = new Element(id, context);
//				}
				Element element = new Element(id, context);

				// ModelBuilder rdfGraphModelBuilder = new ModelBuilder();
				// rdfGraphModelBuilder.namedGraph("http://www.toreador-project.eu/BDMOntologies/HadoopINGESTION7ftptohdfs-2.7.2.owl");
				// esto sirve para actualizar el grafo cuando se salva. Aun no
				// sabemos si sera necesario. Por si acaso lo dejamo
				String s = extractRDF(context);
				/*
				 * String s=extractRDF(
				 * "http://54.229.142.100:8081/BDMOntologies/HadoopINGESTION7ftptohdfs-2.7.2.owl"
				 * );
				 * 
				 * log.info(s); String path =
				 * "/Users/Christian/Documents/gitHub/toreador/lab-Toreador/toreadorLab-owls-editor-resources/ontology/"
				 * +"HadoopINGESTION7ftptohdfs-2.7.2"+".owl"; File file = new
				 * File(path); try { BufferedWriter writer=new
				 * BufferedWriter(new FileWriter(file)); writer.write(s);
				 * writer.close(); } catch (IOException e) {
				 * e.printStackTrace(); }
				 */
				log.info(s);
				// HadoopINGESTION60hdfs-unzipper-2.7.2.owl
				String path = resourcesOntology + label + ".owl";
				File file = new File(path);
				try {
					BufferedWriter writer = new BufferedWriter(new FileWriter(file));
					writer.write(s);
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

				/*
				 * String queryAux ="select *  {  \n" +
				 * "  GRAPH <http://www.toreador-project.eu/BDMOntologies/HadoopINGESTION7ftptohdfs-2.7.2.owl> \n"
				 * +
				 * "        {  <http://www.toreador-project.eu/BDMOntologies/HadoopINGESTION7ftptohdfs-2.7.2.owl#HadoopINGESTION7ftptohdfs-2.7.2ServiceCategory> ?p ?o }}"
				 * ; TupleQuery tupleQuery; tupleQuery =
				 * repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL,
				 * queryAux); TupleQueryResult tupleQueryResult =
				 * tupleQuery.evaluate();
				 * 
				 * 
				 * String idAux; String contextAux; while
				 * (tupleQueryResult.hasNext()) { BindingSet result =
				 * tupleQueryResult.next(); idAux =
				 * result.getValue("p").stringValue(); contextAux =
				 * result.getValue("o").stringValue(); }
				 * 
				 * String myDriver = "com.zaxxer.hikari.HikariDataSource";
				 * String myUrl =
				 * "jdbc:mysql://localhost:3306/toreadorLabOwlsEditorServices?useUnicode=true&characterEncoding=utf8&useSSL=false";
				 * String queryAux2=
				 * "insert into user_services values (NULL,2,'altlabel','label1','typo1','categoryname1','taxonomy1','111','222',NULL)"
				 * ; try { Class.forName(myDriver); Connection conn =
				 * DriverManager.getConnection(myUrl, "root", pass);
				 * PreparedStatement preparedStmt =
				 * conn.prepareStatement(queryAux2); preparedStmt.execute();
				 * conn.close(); } catch (ClassNotFoundException e) {
				 * e.printStackTrace(); } catch (SQLException e) {
				 * e.printStackTrace(); }
				 */
				return element;
			} else {
				log.info("Individual does not exist");
				throw new ToreadorOntoEditorException(HttpStatus.NOT_FOUND,
						new ExceptionData("Individual does not exist"));
			}
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException
				| UpdateExecutionException e) {
			log.error("Error updating individual", e);
			throw new ToreadorOntoEditorException(e, HttpStatus.INTERNAL_SERVER_ERROR,
					new ExceptionData("Unexpected error"));
		}
	}

	public Element deleteIndividual(String id, String context) throws ToreadorOntoEditorException {
		String idUri = SparqlUtils.toUri(id);
		try {
			// Check that the individual does not exist before insert
			if (checkIndividualExists(id)) {
				// add graph name is provided
				String graph = "";
				if (context != null) {
					graph = " GRAPH <" + context + "> ";
				}
				// TODO delete element in Elasticsearch
				// Element element = elasticsearchService.getElementById(id);
				String query = "prefix owl: <http://www.w3.org/2002/07/owl#> " + "prefix : <" + baseURI + "> "
						+ "DELETE WHERE { " + graph + "{ " + idUri + " ?p ?o } }";

				String query2 = "prefix owl: <http://www.w3.org/2002/07/owl#> " + "prefix : <" + baseURI + "> "
						+ "DELETE WHERE { " + graph + "{ ?a ?p " + idUri + " } }";
				log.debug(query);
				// TODO check update ok
				executeUpdate(query, query2);
				return new Element(id, context);
			} else {
				log.info("Individual does not exist");
				throw new ToreadorOntoEditorException(HttpStatus.NOT_FOUND,
						new ExceptionData("Individual does not exist"));
			}
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException
				| UpdateExecutionException e) {
			log.error("Error deleting individual", e);
			throw new ToreadorOntoEditorException(e, HttpStatus.INTERNAL_SERVER_ERROR,
					new ExceptionData("Unexpected error"));
		}
	}

	public List<Element> getExclusiveProperties(String classUri) throws ToreadorOntoEditorException {
		List<Element> predicates;
		String query = "SELECT DISTINCT ?g ?a ?label " + "WHERE{ GRAPH ?g { " + "  ?a rdfs:domain ?class . "
				+ SparqlUtils.toUri(classUri) + " rdfs:subClassOf* ?class. " + " OPTIONAL { ?a rdfs:label ?label .} }}";
		log.debug(query);
		TupleQuery tupleQuery;
		try {
			tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, query);
			TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
			predicates = new ArrayList<>();
			while (tupleQueryResult.hasNext()) {
				BindingSet result = tupleQueryResult.next();
				Binding labelBinding = result.getBinding("label");
				String id = result.getValue("a").stringValue();
				String context = result.getValue("g").stringValue();
				String label = (labelBinding != null) ? labelBinding.getValue().stringValue()
						: id.replaceAll(baseURI, "");
				;
				predicates.add(new Element(id, label, context));
			}
			return predicates;
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			log.info("Unexpected query exception", e);
			throw new ToreadorOntoEditorException(e, HttpStatus.INTERNAL_SERVER_ERROR,
					new ExceptionData("Unexpected error getting exclusive properties", null));
		}
	}

	/**
	 * Gets possible individuals for a specified property.
	 *
	 * @param propertyUri
	 *            the class uri
	 * @return the exclusive properties
	 */
	public List<Element> getPossibleObjects(String propertyUri) throws ToreadorOntoEditorException {
		List<Element> predicates;
		String property = SparqlUtils.toUri(propertyUri);
		// SELECT DISTINCT ?g ?a ?label
		// WHERE{
		// <http://www.daml.org/services/owl-s/1.2/Grounding.owl#owlsProcess>
		// rdfs:range ?class .
		// ?a a ?class1 .
		// ?class1 rdfs:subClassOf* ?class.
		// optional {?a rdfs:label ?label . }.
		// {SELECT ?g ?a {GRAPH ?g {?a rdf:type ?class1.}}}
		// }
		String query = "SELECT DISTINCT ?g ?a ?label WHERE{ " + property
				+ " rdfs:range ?class . ?a a ?class1 . ?class1 rdfs:subClassOf* ?class. optional {?a rdfs:label ?label . }. {SELECT ?g ?a {GRAPH ?g {?a rdf:type ?class1.}}}}";
		log.debug(query);
		TupleQuery tupleQuery;
		try {
			tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, query);
			TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
			predicates = new ArrayList<>();
			while (tupleQueryResult.hasNext()) {
				BindingSet result = tupleQueryResult.next();
				String id = result.getValue("a").stringValue();
				String context = result.getValue("g").stringValue();
				Binding labelBinding = result.getBinding("label");
				String label = (labelBinding != null) ? labelBinding.getValue().stringValue()
						: id.replaceAll(baseURI, "");
				;
				predicates.add(new Element(id, label, context));
			}
			if (predicates.isEmpty()) {
				predicates = null;
			}
			return predicates;
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			log.info("Unexpected query exception", e);
			throw new ToreadorOntoEditorException(e, HttpStatus.INTERNAL_SERVER_ERROR,
					new ExceptionData("Unexpected error getting valid objects", null));
		}
	}

	public boolean deleteGraph(String context) throws ToreadorOntoEditorException {
		try {
			String query = "DELETE { GRAPH <" + context + "> { ?a ?b ?c }} WHERE {GRAPH <" + context
					+ "> { ?a ?b ?c } }";
			executeUpdate(query);
			return true;
		} catch (RepositoryException | MalformedQueryException | UpdateExecutionException e) {
			log.info("Unexpected query exception", e);
			throw new ToreadorOntoEditorException(e, HttpStatus.INTERNAL_SERVER_ERROR,
					new ExceptionData("Unexpected error getting valid objects", null));
		}
	}

	public void uploadGraph(Model rdfModel, String context) {
		try {
			repositoryConnection.begin();
			Resource contextUri = repositoryConnection.getValueFactory().createURI(context);
			Iterator<Statement> iterator = rdfModel.iterator();
			try {
				while (iterator.hasNext()) {
					Statement s = (Statement) iterator.next();
					log.info("uploadGraph " + s.toString());
					repositoryConnection.add(s, contextUri);
					log.info("context" + contextUri.toString());
				}

				repositoryConnection.commit();
				log.info("COMMITED");

			} catch (RepositoryException e) {
				try {
					repositoryConnection.rollback();
				} catch (RepositoryException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} // TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (RepositoryException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}

	public List<String> getAllContextsInGraph() {
		RepositoryResult<Resource> contextIDs = repositoryConnection.getContextIDs();
		List<String> contexts = new ArrayList<>();
		while (contextIDs.hasNext()) {
			contexts.add(contextIDs.next().stringValue());
		}
		return contexts;
	}

	public List<String> getAllContextsInGraph(String context) {
		RepositoryResult<Resource> contextIDs = repositoryConnection.getContextIDs();
		List<String> contexts = new ArrayList<>();
		while (contextIDs.hasNext()) {
			contexts.add(contextIDs.next().stringValue());
		}
		return contexts;
	}

	// public List<com.taiger.toreadorlab.owlseditor.domain.services.Service>
	// extractService(String s, Constrain constrain)
	public Set<ToreadorService> extractService(String s, Constrain constrain, String id)
			throws ToreadorOntoEditorException {
		String query = "prefix profile:<http://www.daml.org/services/owl-s/1.2/Profile.owl#> "
				+ "SELECT DISTINCT ?g ?label ?description WHERE{ ?a a ?class1 . " + "?a profile:categoryName ?cat."
				+ "?b profile:serviceCategory ?a." + "?b rdfs:label ?label."
				+ "?b profile:textDescription ?description." + "FILTER (regex(str(?cat),\"^" + s
				+ "\"))  {GRAPH ?g {?a rdf:type ?class1.}}}";
		log.info(query);
		TupleQuery tupleQuery;
		// List<com.taiger.toreadorlab.owlseditor.domain.services.Service>
		// services = new ArrayList<>();
		Set<ToreadorService> services = new HashSet();
		try {
			tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, query);
			TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
			while (tupleQueryResult.hasNext()) {
				BindingSet result = tupleQueryResult.next();
				String g = result.getValue("g").stringValue();
				String description = result.getValue("description").stringValue();
				Binding labelBinding = result.getBinding("label");
				String label = (labelBinding != null) ? labelBinding.getValue().stringValue() : g;
				/*
				 * com.taiger.toreadorlab.owlseditor.domain.services.Service
				 * service = new
				 * com.taiger.toreadorlab.owlseditor.domain.services.Service();
				 * service.setId(g); service.setName(label);
				 * service.setDescription(description); service.setOntoId(g);
				 * service.setOwls(extractRDF(g));
				 * service.setParameters(extractParameters(g, constrain));
				 * services.add(service);
				 */
				log.info("label: " + label);
				it.eng.toreador.catalogue.Service aux = retrieveService(label);
				// log.info("aux: " + aux.toString());
				String[] split = id.split("http://www.toreador-project.eu/TDM/project-spec/");
				// if(aux.getArea().name().equalsIgnoreCase(split[1])){
				if (aux != null) {
					ToreadorService test = new ToreadorService();
					test.setOwl(extractRDF(g));
					test.setService(aux);
					// log.info("OWL:"+ test.getOwl().toString());
					// log.info("aux:"+ test.getService().getName());
					services.add(test);
				}

			}
			return services;
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			log.error("Unexpected query exception", e);
			throw new ToreadorOntoEditorException(e, HttpStatus.INTERNAL_SERVER_ERROR,
					new ExceptionData("Unexpected error getting valid objects", null));
		}
	}

	private it.eng.toreador.catalogue.Service retrieveService(String label) {
		ObjectMapper obejctMapper = new ObjectMapper();
		it.eng.toreador.catalogue.Service service = null;
		try {
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };
			// Install the all-trusting trust manager
			final SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

			// get the services catalog from remote API rest service
			URL url = new URL(serviceCatalogueEndpoint);

			String userPassword = serviceCatalogueEndpointUser + ":" + serviceCatalogueEndpointPassword;
			String encoding = new sun.misc.BASE64Encoder().encode(userPassword.getBytes());
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Authorization", "Basic " + encoding);
			conn.setRequestProperty("Accept", "application/json");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setConnectTimeout(99999999);
			conn.setReadTimeout(99999999);
			// if everything is ok goes on otherwise throws beautiful errors

			if (conn.getResponseCode() != 200) {
				throw new ToreadorOntoEditorException(new RuntimeException("Wrong response from remote endpoint"),
						HttpStatus.INTERNAL_SERVER_ERROR,
						new ExceptionData("Service endpoint answered with " + conn.getResponseCode(), null));
			}
			// System.out.println(IOUtils.toString(conn.getInputStream(),
			// StandardCharsets.UTF_8));
			// obejctMapper.configure(Feature.AUTO_CLOSE_SOURCE, true);
			obejctMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

			it.eng.toreador.catalogue.Catalogue[] catalogo = obejctMapper.readValue(conn.getInputStream(),
					it.eng.toreador.catalogue.Catalogue[].class);

			// log.info(catalogo.toString());
			// closes connection
			conn.disconnect();

			for (it.eng.toreador.catalogue.Service serv : catalogo[0].getServices()) {

				// log.info(serv.toString());
				// String tests = "HadoopINGESTION7ftptohdfs-2.7.2.owl";
				// String tests = "SparkANALYTICS1asdasd-1.6.3ServiceCategory";
				Pattern VALID_PATTERN = Pattern.compile("[0-9]+");
				Matcher matcher = VALID_PATTERN.matcher(label);
				List<String> chunks = new LinkedList<String>();
				while (matcher.find()) {
					chunks.add(matcher.group());
				}

				Pattern VALID_PATTERN2 = Pattern.compile("[A-Za-z]+");
				Matcher matcher2 = VALID_PATTERN2.matcher(label);
				List<String> chunks2 = new LinkedList<String>();
				while (matcher2.find()) {
					chunks2.add(matcher2.group());
				}

				String name = "";
				for (int i = 1; i < chunks2.size() - 1; i++) {
					name += chunks2.get(i) + "-";
				}

				name = name.substring(0, name.length() - 1);

				if (serv.getId() == Integer.parseInt(chunks.get(0)) && serv.getName().equalsIgnoreCase(name)) {
					service = serv;
				}

			}

			return service;

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		} catch (ToreadorOntoEditorException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public GraphQueryResult getServiceOWLS(String g) throws ToreadorOntoEditorException {
		String query = "construct {?s ?p ?o} where {graph <" + g + "> {?s ?p ?o}}";
		// log.info(query);
		GraphQuery graphQuery;
		try {
			graphQuery = repositoryConnection.prepareGraphQuery(QueryLanguage.SPARQL, query);
			return graphQuery.evaluate();
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			log.info("Unexpected query exception", e);
			throw new ToreadorOntoEditorException(e, HttpStatus.INTERNAL_SERVER_ERROR,
					new ExceptionData("Unexpected error getting valid objects", null));
		}
	}

	private Map<String, Parameter> extractParameters(String g, Constrain constrain) throws ToreadorOntoEditorException {
		Map<String, Parameter> parameters = new HashMap<>();
		String query = "prefix profile:<http://www.daml.org/services/owl-s/1.2/Profile.owl#>"
				+ "prefix process:<http://www.daml.org/services/owl-s/1.2/Process.owl#>" + "prefix toreador: <"
				+ baseURI + ">" + "SELECT ?param ?label ?type ?comment ?defaultValue ?mandatory FROM <" + g
				+ "> where {" + "  {?ap a profile:Profile. "
				+ "                                         ?ap profile:hasInput ?param. "
				+ "                                         ?param rdf:type process:Input. "
				+ "                                         ?param process:parameterType ?type. "
				+ "                                         ?param rdfs:label ?label."
				+ " ?param toreador:defaultValue ?defaultValue. " + " ?param toreador:mandatory ?mandatory. "
				+ "                                        ?param rdfs:comment ?comment }"
				+ "                                 }";
		log.debug(query);
		TupleQuery tupleQuery;
		try {
			tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, query);
			TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
			while (tupleQueryResult.hasNext()) {
				BindingSet result = tupleQueryResult.next();
				String label = result.getValue("label").stringValue();
				String param = result.getValue("param").stringValue();
				String type = result.getValue("type").stringValue();
				String comment = result.getValue("comment").stringValue();
				String defaultValue = result.getValue("defaultValue").stringValue();
				if (defaultValue.toLowerCase().equals("random")) {
					defaultValue = new Integer(new Random().nextInt(Integer.MAX_VALUE)).toString();
				}
				if (constrain.get(label) != null) {
					defaultValue = constrain.get(label).getValue();
				}
				// if (label.toLowerCase().equals("maxiterations")){
				// defaultValue ="100";
				// }
				boolean mandatory = ((Literal) result.getValue("mandatory")).booleanValue();
				boolean inputData = ((Literal) result.getValue("inputData")).booleanValue();
				boolean outputData = ((Literal) result.getValue("outputData")).booleanValue();
				Parameter p = new Parameter();
				p.setDescription(comment);
				p.setId(param);
				p.setType(type);
				p.setKey(label);
				// p.setValue();
				p.setDefaultValue(defaultValue);
				p.setMandatory(mandatory);
				p.setInputData(inputData);
				p.setOutputData(outputData);
				parameters.put(label, p);
			}
			return parameters;
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			log.info("Unexpected query exception", e);
			throw new ToreadorOntoEditorException(e, HttpStatus.INTERNAL_SERVER_ERROR,
					new ExceptionData("Unexpected error getting valid objects", null));
		}
	}

	private String extractRDF(String g) throws ToreadorOntoEditorException {
		String query = "construct {?s ?p ?o} where {graph <" + g + "> {?s ?p ?o}}";
		// log.debug(query);
		GraphQuery graphQuery;
		try {
			graphQuery = repositoryConnection.prepareGraphQuery(QueryLanguage.SPARQL, query);
			StringWriter sw = new StringWriter();
			RDFHandler rdfWriter = new RDFXMLPrettyWriterFactory().getWriter(sw);// Rio.createWriter(RDFFormat.RDFXML,
																					// sw);
			graphQuery.evaluate(rdfWriter);
			return sw.toString();
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			log.info("Unexpected query exception", e);
			throw new ToreadorOntoEditorException(e, HttpStatus.INTERNAL_SERVER_ERROR,
					new ExceptionData("Unexpected error getting valid objects", null));
		}
	}

	public IRI getGroundingForService(IRI graph, IRI serviceIRI) throws ToreadorOntoEditorException {
		String query = "select ?p where {  graph <" + graph.toString()
				+ "> {?grunding <http://www.daml.org/services/owl-s/1.2/Grounding.owl#hasAtomicProcessGrounding> ?p. "
				+ "?service <http://www.daml.org/services/owl-s/1.2/Service.owl#supports> ?grunding." + "}}";
		/*
		 * +
		 * "?service <http://www.daml.org/services/owl-s/1.2/Service.owl#presents> "
		 * + "<" + serviceIRI.stringValue() + ">. }}";
		 */
		// log.info("gRAPH: " + graph.toString());
		// log.info("sERVICE: " + serviceIRI);
		// log.info("QUERY: " + query);
		TupleQuery tupleQuery;
		try {
			tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, query);
			TupleQueryResult tupleQueryResult = tupleQuery.evaluate();
			while (tupleQueryResult.hasNext()) {
				BindingSet result = tupleQueryResult.next();
				String grounding = result.getValue("p").stringValue();
				return valueFactory.createIRI(grounding);
			}
		} catch (RepositoryException | MalformedQueryException | QueryEvaluationException e) {
			log.info("Unexpected query exception", e);
			throw new ToreadorOntoEditorException(e, HttpStatus.INTERNAL_SERVER_ERROR,
					new ExceptionData("Unexpected error getting valid objects", null));
		}
		return null;
	}

}
