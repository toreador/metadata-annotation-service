package com.taiger.toreadorlab.owlseditor.domain.workflow;

public enum ToreadorReferenceModeEnum {
	EXPLICIT_REFERENCES, 
	GET_REFERENCES, 
	INSERT_OWLS_TRIPLES;
	
}
