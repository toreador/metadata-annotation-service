package com.taiger.toreadorlab.owlseditor.domain.workflow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ToreadorElement implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4257342657782368897L;

	private Map<String, ToreadorNode> nodes = new HashMap<>();

	private Map<String, ToreadorEdge> edges = new HashMap<>();

	public ToreadorNode getNode(String id) {
		return this.nodes.get(id);
	}

	public ToreadorEdge getEdge(String id) {
		return this.edges.get(id);
	}

	public List<ToreadorNode> getNodes() {
		return new ArrayList<ToreadorNode>(nodes.values());
	}

	public void setNodes(List<ToreadorNode> nodeslist) {
		for (ToreadorNode node : nodeslist) {
			nodes.put(node.getData().getId(), node);
		}
	}

	public List<ToreadorEdge> getEdges() {
		return new ArrayList<ToreadorEdge>(edges.values());
	}

	public void setEdges(List<ToreadorEdge> edgeslist) {
		for (ToreadorEdge edge : edgeslist) {
			edges.put(edge.getData().getId(), edge);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((edges == null) ? 0 : edges.hashCode());
		result = prime * result + ((nodes == null) ? 0 : nodes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ToreadorElement other = (ToreadorElement) obj;
		if (edges == null) {
			if (other.edges != null)
				return false;
		} else if (!edges.equals(other.edges))
			return false;
		if (nodes == null) {
			if (other.nodes != null)
				return false;
		} else if (!nodes.equals(other.nodes))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ToreadorElement [nodes=" + nodes + ", edges=" + edges + "]";
	}
	
}