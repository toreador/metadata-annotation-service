package com.taiger.toreadorlab.owlseditor.domain.workflow;

import java.io.Serializable;

import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.annotation.JsonSerialize;
@JsonSerialize
public class ToreadorOWLSServiceResponse implements Serializable {
/**
	 * 
	 */
	private static final long serialVersionUID = 317916863647077099L;
private String data;

public String getData() {
	return data;
}

public void setData(String data) {
	this.data = data;
}

}
