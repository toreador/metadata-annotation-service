package com.taiger.toreadorlab.owlseditor.domain.workflow;

import java.io.Serializable;

public class ToreadorNode implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 2330307602642860174L;

	private ToreadorNodeData data;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ToreadorNode other = (ToreadorNode) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		return true;
	}

	public ToreadorNodeData getData() {
		return data;
	}

	public void setData(ToreadorNodeData data) {
		this.data = data;
	}

	public String getNodeType() {
		return this.data.getNodeType().getType();
	}

	public String getOperatorType() {
		return this.data.getNodeType().getOperatorType();
	}

	@Override
	public String toString() {
		return "ToreadorNode [data=" + data + "]";
	}

}
