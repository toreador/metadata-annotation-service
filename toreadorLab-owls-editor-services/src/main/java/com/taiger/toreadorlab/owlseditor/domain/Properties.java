package com.taiger.toreadorlab.owlseditor.domain;

import java.io.Serializable;
import java.util.Map;

public class Properties implements Serializable {

    private Map<String,Object> mapping;

    public Map<String, Object> getMapping() {
        return mapping;
    }

    public void setMapping(Map<String, Object> mapping) {
        this.mapping = mapping;
    }

    @Override
    public String toString() {
        return "Properties{" +
            "mapping=" + mapping +
            '}';
    }
}

