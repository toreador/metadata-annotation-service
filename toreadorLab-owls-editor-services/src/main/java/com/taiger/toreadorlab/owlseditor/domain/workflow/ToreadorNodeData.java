package com.taiger.toreadorlab.owlseditor.domain.workflow;

import java.io.Serializable;

public class ToreadorNodeData implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5976221665664276570L;

	private String id;
	//private Service service;
    private it.eng.toreador.catalogue.Service service;
	private ToreadorNodeType nodeType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/*public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}*/

    public it.eng.toreador.catalogue.Service getService() {
        return service;
    }

    public void setService(it.eng.toreador.catalogue.Service service) {
        this.service = service;
    }

    public ToreadorNodeType getNodeType() {
		return nodeType;
	}

	public void setNodeType(ToreadorNodeType nodeType) {
		this.nodeType = nodeType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nodeType == null) ? 0 : nodeType.hashCode());
		result = prime * result + ((service == null) ? 0 : service.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ToreadorNodeData other = (ToreadorNodeData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nodeType == null) {
			if (other.nodeType != null)
				return false;
		} else if (!nodeType.equals(other.nodeType))
			return false;
		if (service == null) {
			if (other.service != null)
				return false;
		} else if (!service.equals(other.service))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ToreadorNodeData [id=" + id + ", service=" + service + ", nodeType=" + nodeType + "]";
	}

}
