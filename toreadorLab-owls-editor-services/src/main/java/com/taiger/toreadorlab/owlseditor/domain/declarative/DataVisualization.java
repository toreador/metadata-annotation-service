package com.taiger.toreadorlab.owlseditor.domain.declarative;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.ObjectMapper;

import com.google.common.base.Strings;

public class DataVisualization extends Area<Objective> implements Serializable {

	public Constrain getConstrains() {
		ObjectMapper objectMaper = new ObjectMapper();
		Constrain results = new Constrain();
		if (this.incorporates != null && !incorporates.isEmpty()) {
			for (Objective indicator : incorporates) {
				try {
					if (!Strings.isNullOrEmpty(indicator.getConstraint())) {
					Constrain constraints = objectMaper.readValue(indicator.getConstraint().replaceAll("\\\"", "\""),
							Constrain.class);
					if (constraints != null)
						for (String key : constraints.keySet())
							results.put(key, constraints.get(key));
					}
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return results;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8736857603075477926L;

	public List<String> getCompactRepresentation() {
		List<String> result = new ArrayList<>();
		String entry = this.getLabel() == null ? "" : this.getLabel().replaceAll(" ", "_");
		if (this.incorporates != null && !incorporates.isEmpty()) {
			for (Objective indicator : incorporates) {

				List<String> compactIndie = indicator.getCompactRepresentation();
				for (String en : compactIndie) {
					result.add(entry + "." + en);
				}
			}
			return result;
		} else
			return Arrays.asList(entry);
	}

}
