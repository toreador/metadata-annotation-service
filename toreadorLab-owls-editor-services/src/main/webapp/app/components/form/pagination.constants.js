(function() {
    'use strict';

    angular
        .module('toreadorLabOwlsEditorServicesApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
