(function() {
    'use strict';

    angular
        .module('toreadorLabOwlsEditorServicesApp')
        .controller('ServiceDetailController', ServiceDetailController);

    ServiceDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Service', 'User_service'];

    function ServiceDetailController($scope, $rootScope, $stateParams, previousState, entity, Service, User_service) {
        var vm = this;

        vm.service = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('toreadorLabOwlsEditorServicesApp:serviceUpdate', function(event, result) {
            vm.service = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
