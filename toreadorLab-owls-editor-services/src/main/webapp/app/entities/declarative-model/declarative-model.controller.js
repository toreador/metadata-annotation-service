(function() {
    'use strict';

    angular
        .module('toreadorLabOwlsEditorServicesApp')
        .controller('DeclarativeModelController', DeclarativeModelController);

    DeclarativeModelController.$inject = ['DeclarativeModel'];

    function DeclarativeModelController(DeclarativeModel) {

        var vm = this;

        vm.declarativeModels = [];

        loadAll();

        function loadAll() {
            DeclarativeModel.query(function(result) {
                vm.declarativeModels = result;
                vm.searchQuery = null;
            });
        }
    }
})();
