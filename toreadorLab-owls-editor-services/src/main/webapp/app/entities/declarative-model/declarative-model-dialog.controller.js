(function() {
    'use strict';

    angular
        .module('toreadorLabOwlsEditorServicesApp')
        .controller('DeclarativeModelDialogController', DeclarativeModelDialogController);

    DeclarativeModelDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DeclarativeModel', 'User'];

    function DeclarativeModelDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DeclarativeModel, User) {
        var vm = this;

        vm.declarativeModel = entity;
        vm.clear = clear;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.declarativeModel.id !== null) {
                DeclarativeModel.update(vm.declarativeModel, onSaveSuccess, onSaveError);
            } else {
                DeclarativeModel.save(vm.declarativeModel, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('toreadorLabOwlsEditorServicesApp:declarativeModelUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
