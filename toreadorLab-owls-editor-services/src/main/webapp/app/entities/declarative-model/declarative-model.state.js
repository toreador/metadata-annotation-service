(function() {
    'use strict';

    angular
        .module('toreadorLabOwlsEditorServicesApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('declarative-model', {
            parent: 'entity',
            url: '/declarative-model',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'DeclarativeModels'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/declarative-model/declarative-models.html',
                    controller: 'DeclarativeModelController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('declarative-model-detail', {
            parent: 'declarative-model',
            url: '/declarative-model/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'DeclarativeModel'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/declarative-model/declarative-model-detail.html',
                    controller: 'DeclarativeModelDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'DeclarativeModel', function($stateParams, DeclarativeModel) {
                    return DeclarativeModel.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'declarative-model',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('declarative-model-detail.edit', {
            parent: 'declarative-model-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/declarative-model/declarative-model-dialog.html',
                    controller: 'DeclarativeModelDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DeclarativeModel', function(DeclarativeModel) {
                            return DeclarativeModel.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('declarative-model.new', {
            parent: 'declarative-model',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/declarative-model/declarative-model-dialog.html',
                    controller: 'DeclarativeModelDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                graphUri: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('declarative-model', null, { reload: 'declarative-model' });
                }, function() {
                    $state.go('declarative-model');
                });
            }]
        })
        .state('declarative-model.edit', {
            parent: 'declarative-model',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/declarative-model/declarative-model-dialog.html',
                    controller: 'DeclarativeModelDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DeclarativeModel', function(DeclarativeModel) {
                            return DeclarativeModel.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('declarative-model', null, { reload: 'declarative-model' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('declarative-model.delete', {
            parent: 'declarative-model',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/declarative-model/declarative-model-delete-dialog.html',
                    controller: 'DeclarativeModelDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DeclarativeModel', function(DeclarativeModel) {
                            return DeclarativeModel.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('declarative-model', null, { reload: 'declarative-model' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
