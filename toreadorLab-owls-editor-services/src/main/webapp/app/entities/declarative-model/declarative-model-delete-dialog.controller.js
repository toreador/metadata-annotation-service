(function() {
    'use strict';

    angular
        .module('toreadorLabOwlsEditorServicesApp')
        .controller('DeclarativeModelDeleteController',DeclarativeModelDeleteController);

    DeclarativeModelDeleteController.$inject = ['$uibModalInstance', 'entity', 'DeclarativeModel'];

    function DeclarativeModelDeleteController($uibModalInstance, entity, DeclarativeModel) {
        var vm = this;

        vm.declarativeModel = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DeclarativeModel.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
