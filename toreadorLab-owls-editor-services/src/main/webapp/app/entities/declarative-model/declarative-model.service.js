(function() {
    'use strict';
    angular
        .module('toreadorLabOwlsEditorServicesApp')
        .factory('DeclarativeModel', DeclarativeModel);

    DeclarativeModel.$inject = ['$resource'];

    function DeclarativeModel ($resource) {
        var resourceUrl =  'api/declarative-models/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
