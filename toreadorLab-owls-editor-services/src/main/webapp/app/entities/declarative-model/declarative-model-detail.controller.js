(function() {
    'use strict';

    angular
        .module('toreadorLabOwlsEditorServicesApp')
        .controller('DeclarativeModelDetailController', DeclarativeModelDetailController);

    DeclarativeModelDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DeclarativeModel', 'User'];

    function DeclarativeModelDetailController($scope, $rootScope, $stateParams, previousState, entity, DeclarativeModel, User) {
        var vm = this;

        vm.declarativeModel = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('toreadorLabOwlsEditorServicesApp:declarativeModelUpdate', function(event, result) {
            vm.declarativeModel = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
