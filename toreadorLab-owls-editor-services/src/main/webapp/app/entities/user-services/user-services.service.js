(function() {
    'use strict';
    angular
        .module('toreadorLabOwlsEditorServicesApp')
        .factory('User_services', User_services);

    User_services.$inject = ['$resource'];

    function User_services ($resource) {
        var resourceUrl =  'api/user-services/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
