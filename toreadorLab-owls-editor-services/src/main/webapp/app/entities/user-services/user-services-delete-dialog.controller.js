(function() {
    'use strict';

    angular
        .module('toreadorLabOwlsEditorServicesApp')
        .controller('User_servicesDeleteController',User_servicesDeleteController);

    User_servicesDeleteController.$inject = ['$uibModalInstance', 'entity', 'User_services'];

    function User_servicesDeleteController($uibModalInstance, entity, User_services) {
        var vm = this;

        vm.user_services = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            User_services.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
