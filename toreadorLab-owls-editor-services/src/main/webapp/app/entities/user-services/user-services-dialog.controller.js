(function() {
    'use strict';

    angular
        .module('toreadorLabOwlsEditorServicesApp')
        .controller('User_servicesDialogController', User_servicesDialogController);

    User_servicesDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'User_services', 'Service'];

    function User_servicesDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, User_services, Service) {
        var vm = this;

        vm.user_services = entity;
        vm.clear = clear;
        vm.save = save;
        vm.services = Service.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.user_services.id !== null) {
                User_services.update(vm.user_services, onSaveSuccess, onSaveError);
            } else {
                User_services.save(vm.user_services, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('toreadorLabOwlsEditorServicesApp:user_servicesUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
