(function() {
    'use strict';

    angular
        .module('toreadorLabOwlsEditorServicesApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-services', {
            parent: 'entity',
            url: '/user-services?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'User_services'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-services/user-services.html',
                    controller: 'User_servicesController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('user-services-detail', {
            parent: 'user-services',
            url: '/user-services/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'User_services'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-services/user-services-detail.html',
                    controller: 'User_servicesDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'User_services', function($stateParams, User_services) {
                    return User_services.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'user-services',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('user-services-detail.edit', {
            parent: 'user-services-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-services/user-services-dialog.html',
                    controller: 'User_servicesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['User_services', function(User_services) {
                            return User_services.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-services.new', {
            parent: 'user-services',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-services/user-services-dialog.html',
                    controller: 'User_servicesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                idUser: null,
                                idService: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-services', null, { reload: 'user-services' });
                }, function() {
                    $state.go('user-services');
                });
            }]
        })
        .state('user-services.edit', {
            parent: 'user-services',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-services/user-services-dialog.html',
                    controller: 'User_servicesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['User_services', function(User_services) {
                            return User_services.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-services', null, { reload: 'user-services' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-services.delete', {
            parent: 'user-services',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-services/user-services-delete-dialog.html',
                    controller: 'User_servicesDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['User_services', function(User_services) {
                            return User_services.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-services', null, { reload: 'user-services' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
