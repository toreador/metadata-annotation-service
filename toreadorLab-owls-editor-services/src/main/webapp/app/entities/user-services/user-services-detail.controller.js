(function() {
    'use strict';

    angular
        .module('toreadorLabOwlsEditorServicesApp')
        .controller('User_servicesDetailController', User_servicesDetailController);

    User_servicesDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'User_services', 'Service'];

    function User_servicesDetailController($scope, $rootScope, $stateParams, previousState, entity, User_services, Service) {
        var vm = this;

        vm.user_services = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('toreadorLabOwlsEditorServicesApp:user_servicesUpdate', function(event, result) {
            vm.user_services = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
