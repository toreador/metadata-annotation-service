(function () {
    'use strict';

    angular
        .module('toreadorLabOwlsEditorServicesApp')
        .factory('Register', Register);

    Register.$inject = ['$resource'];

    function Register ($resource) {
        return $resource('api/register', {}, {});
    }
})();
