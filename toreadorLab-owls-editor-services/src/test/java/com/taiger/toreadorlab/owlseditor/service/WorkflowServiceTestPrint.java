package com.taiger.toreadorlab.owlseditor.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.taiger.toreadorlab.owlseditor.ToreadorLabOwlsEditorServicesApp;
import com.taiger.toreadorlab.owlseditor.web.rest.errors.ToreadorOntoEditorException;
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "dev")
@SpringBootTest(classes = ToreadorLabOwlsEditorServicesApp.class)
public class WorkflowServiceTestPrint {

	@Autowired
	WorkflowService workflowService;
	
	@Test
	public void testPrintWorkflow() {
		// read file OWLS-Demo-ExplicitReferences.owl
		String projectDir = System.getProperty("user.dir");
		try {
			Path stream = Paths.get(projectDir + "/src/test/resources/models/OWLS-Demo-ExplicitReferences.owl");
			BufferedReader reader = Files.newBufferedReader(stream, StandardCharsets.UTF_8);
			String line;
			StringBuffer sb = new StringBuffer();
			while ((line = reader.readLine()) != null) {
				// process each line in some way
				sb.append(line);
			}
			try {
				System.out.println(workflowService.printWorkflow(sb.toString()));
			} catch (ToreadorOntoEditorException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
