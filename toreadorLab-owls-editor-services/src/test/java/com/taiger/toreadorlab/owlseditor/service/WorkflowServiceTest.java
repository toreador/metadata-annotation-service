//package com.taiger.toreadorlab.owlseditor.service;
//
//import java.io.IOException;
//import java.net.HttpURLConnection;
//import java.net.URL;
//
//import org.apache.commons.io.IOUtils;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import com.taiger.toreadorlab.owlseditor.ToreadorLabOwlsEditorServicesApp;
//@RunWith(SpringRunner.class)
//@ActiveProfiles(profiles = "dev")
//@SpringBootTest(classes = ToreadorLabOwlsEditorServicesApp.class)
//public class WorkflowServiceTest {
//	
//	@Autowired
//	WorkflowService workflowService;
//
//	@Test
//	public void testGetServiceRequestReference() {
//		String graph1 = "http://www.example.com/toreador/BDMOntologies/Sparkcleanser.owl";
//		String graph2 = "http://www.example.com/toreador/BDMOntologies/Sparkkmeans.owl";
//		String graph3 = "http://www.example.com/toreador/BDMOntologies/MapReducekmeans.owl";
//		String graph4 = "http://www.example.com/toreador/BDMOntologies/Sparkelbow.owl";
//
//		URL obj;
//		try {
//			obj = new URL(workflowService.getServiceRequestReference(graph1, graph1+"#SparkcleanserProcess"));
//			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
//			con.setRequestProperty("Accept","application/rdf+xml");
//			con.connect();
//			IOUtils.copy(con.getInputStream(),System.out);
//			
//			obj = new URL(workflowService.getServiceRequestReference(graph2, graph2+"#SparkkmeansProcess"));
//			con = (HttpURLConnection) obj.openConnection();
//			con.setRequestProperty("Accept","application/rdf+xml");
//			con.connect();
//			IOUtils.copy(con.getInputStream(),System.out);
//
//			obj = new URL(workflowService.getServiceRequestReference(graph3, graph3+"#MapReducekmeansProcess"));
//			con = (HttpURLConnection) obj.openConnection();
//			con.setRequestProperty("Accept","application/rdf+xml");
//			con.connect();
//			IOUtils.copy(con.getInputStream(),System.out);
//			
//			obj = new URL(workflowService.getServiceRequestReference(graph4, graph4+"#SparkelbowProcess"));
//			con = (HttpURLConnection) obj.openConnection();
//			con.setRequestProperty("Accept","application/rdf+xml");
//			con.connect();
//			IOUtils.copy(con.getInputStream(),System.out);
//			
//			} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//}
