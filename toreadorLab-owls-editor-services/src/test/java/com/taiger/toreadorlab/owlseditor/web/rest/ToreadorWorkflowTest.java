//package com.taiger.toreadorlab.owlseditor.web.rest;
//
//import static org.junit.Assert.assertTrue;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.io.FileInputStream;
//import java.io.InputStream;
//
//import org.apache.commons.io.IOUtils;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.ResultActions;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import com.taiger.toreadorlab.owlseditor.ToreadorLabOwlsEditorServicesApp;
//import com.taiger.toreadorlab.owlseditor.service.DeclarativeModelService;
//
//@RunWith(SpringRunner.class)
//@ActiveProfiles(profiles = "dev")
//@SpringBootTest(classes = ToreadorLabOwlsEditorServicesApp.class)
//
//public class ToreadorWorkflowTest {
//
//    @Autowired
//    ToreadorServicesController toreadorServiceController;
//    //
//    @Mock
//    DeclarativeModelService dmService;
//    //
//    private MockMvc mockMvc;
//
//    @Before
//    public void setUp() throws Exception {
//        MockitoAnnotations.initMocks(this);
//        mockMvc = MockMvcBuilders.standaloneSetup(toreadorServiceController).build();
//    }
//
//    @Test
//    public void testObtainServicesCatalogue() throws Exception {
//        //assertNotNull("Not yet implemented");
//        String projectDir = System.getProperty("user.dir");
//        InputStream stream = new FileInputStream(projectDir + "/src/test/resources/models/workflowTest.json");
//        ResultActions result = mockMvc
//            .perform(post("/api/services/obtainServicesCatalogue").param("userContext", "http://www.testcontext.com")
//                .content(IOUtils.toByteArray(stream)).contentType("application/json"))
//            .andExpect(status().isOk())
//            // .andExpect(content().string(is("Greetings from Spring Boot!")))
//            ;
//        assertTrue(true);
//    }
//
//
//
//}
