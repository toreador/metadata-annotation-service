//package com.taiger.toreadorlab.owlseditor.web.rest;
//
//import static org.junit.Assert.assertTrue;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.io.FileInputStream;
//import java.io.InputStream;
//import java.util.Map;
//
//import org.apache.commons.io.IOUtils;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.ResultActions;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.taiger.toreadorlab.owlseditor.ToreadorLabOwlsEditorServicesApp;
//import com.taiger.toreadorlab.owlseditor.service.DeclarativeModelService;
//
//@RunWith(SpringRunner.class)
//@ActiveProfiles(profiles = "dev")
//@SpringBootTest(classes = ToreadorLabOwlsEditorServicesApp.class)
//
//public class ToreadorServicesControllerTest {
//
//	@Autowired
//	ToreadorServicesController toreadorServiceController;
//	//
//	@Mock
//	DeclarativeModelService dmService;
//	//
//	private MockMvc mockMvc;
//
//	@Before
//	public void setUp() throws Exception {
//		MockitoAnnotations.initMocks(this);
//		mockMvc = MockMvcBuilders.standaloneSetup(toreadorServiceController).build();
//	}
//
//	@Test
//    public void testObtainServicesCatalogue() throws Exception {
//        //assertNotNull("Not yet implemented");
//        String projectDir = System.getProperty("user.dir");
//        InputStream stream = new FileInputStream(projectDir + "/src/test/resources/models/workflowTest.json");
//        ResultActions result = mockMvc
//            .perform(post("/api/services/obtainServicesCatalogue").param("userContext", "http://www.testcontext.com")
//                .content(IOUtils.toByteArray(stream)).contentType("application/ld+json"))
//            .andExpect(status().isOk())
//            // .andExpect(content().string(is("Greetings from Spring Boot!")))
//            ;
//        assertTrue(true);
//    }
//
//	@Test
//	public void testSubmitDeclarativeModel() throws Exception {
//		String projectDir = System.getProperty("user.dir");
//		InputStream stream = new FileInputStream(projectDir + "/src/test/resources/models/jotSampleDM.json");
//		ResultActions result = mockMvc
//				.perform(post("/api/services/submitDeclarativeModel").param("userContext", "http://www.testcontext.com")
//						.content(IOUtils.toByteArray(stream)).contentType("application/ld+json"))
//				.andExpect(status().isOk())
//		// .andExpect(content().string(is("Greetings from Spring Boot!")))
//		;
//		// TODO fix this, need to add id of the dm
//		mockMvc.perform(delete("/api/services/deleteDeclarativeModel").param("userContext", "http://www.testcontext.com")
//				.param("id", result.andReturn().getResponse().getContentAsString())).andExpect(status().isOk())
//		// .andExpect(content().string(is("Greetings from Spring Boot!")))
//		;
//		assertTrue(true);
//	}
//
//
//	@Test
//	public void testSubmitDeclarativeModel2() throws Exception {
//		ObjectMapper om = new ObjectMapper();
//		String projectDir = System.getProperty("user.dir");
////		InputStream stream = new FileInputStream(projectDir + "/src/test/resources/models/declarativeDemo.json");
//        InputStream stream = new FileInputStream(projectDir + "/src/test/resources/models/NewDeclarative.json");
//		Map<String,String> body = om.readValue(stream, Map.class);
//		ResultActions result = mockMvc
//				.perform(post("/api/services/submitDeclarativeModels")
//						.content(TestUtil.convertObjectToJsonBytes(body))
//						.contentType("application/json"))
//				.andExpect(status().isOk());
//		assertTrue(true);
//	}
//
//	@Test
//	public void testSubmitDeclarativeModels() throws Exception {
//		ObjectMapper om = new ObjectMapper();
//		String projectDir = System.getProperty("user.dir");
//		InputStream stream = new FileInputStream(projectDir + "/src/test/resources/models/configuration-prova.json");
//		Map<String, String> body = om.readValue(stream, Map.class);
//		ResultActions result = mockMvc
//				.perform(post("/api/services/submitDeclarativeModels")
//						.content(TestUtil.convertObjectToJsonBytes(body))
//						.contentType("application/json"))
//				.andExpect(status().isOk());
//		assertTrue(true);
//	}
//
//
//	@Test
//	public void testCreateOWLSfromWorkflow() throws Exception {
//		ObjectMapper om = new ObjectMapper();
//		String projectDir = System.getProperty("user.dir");
//		InputStream stream = new FileInputStream(projectDir + "/src/test/resources/models/workflow.json");
//		Map<String, String> body = om.readValue(stream, Map.class);
//		ResultActions result = mockMvc
//				.perform(post("/api/services/createOWLSfromWorkflow")
//						.content(TestUtil.convertObjectToJsonBytes(body))
//						.contentType("application/json"))
//				.andExpect(status().isOk());
//		assertTrue(true);
//	}
//
//	@Test
//	public void testCreateOWLSfromWorkflowDemo() throws Exception {
//		ObjectMapper om = new ObjectMapper();
//		String projectDir = System.getProperty("user.dir");
//		InputStream stream = new FileInputStream(projectDir + "/src/test/resources/models/workflowDemo.json");
//		Map<String, String> body = om.readValue(stream, Map.class);
//		ResultActions result = mockMvc
//				.perform(post("/api/services/createOWLSfromWorkflow")
//						.content(TestUtil.convertObjectToJsonBytes(body))
//						.contentType("application/json"))
//				.andExpect(status().isOk());
//		assertTrue(true);
//	}
//
//	@Test
//	public void testCreateOWLSfromWorkflowOperatorRepUnt() throws Exception {
//		ObjectMapper om = new ObjectMapper();
//		String projectDir = System.getProperty("user.dir");
//		InputStream stream = new FileInputStream(projectDir + "/src/test/resources/models/workflowOperatorRUAlt.json");
//		Map<String, String> body = om.readValue(stream, Map.class);
//		ResultActions result = mockMvc
//				.perform(post("/api/services/createOWLSfromWorkflow")
//						.content(TestUtil.convertObjectToJsonBytes(body))
//						.contentType("application/json"))
//				.andExpect(status().isOk());
//		assertTrue(true);
//	}
//
//	@Test
//	public void testCreateOWLSfromWorkflowOperatorRepUntIf() throws Exception {
//		ObjectMapper om = new ObjectMapper();
//		String projectDir = System.getProperty("user.dir");
//		InputStream stream = new FileInputStream(projectDir + "/src/test/resources/models/IterateAndConditionale.json");
//		Map<String, String> body = om.readValue(stream, Map.class);
//		ResultActions result = mockMvc
//				.perform(post("/api/services/createOWLSfromWorkflow")
//						.content(TestUtil.convertObjectToJsonBytes(body))
//						.contentType("application/json"))
//				.andExpect(status().isOk());
//		assertTrue(true);
//	}
//
//}
