/**
 * 
 */
package com.taiger.toreadorlab.owlseditor.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.taiger.toreadorlab.owlseditor.ToreadorLabOwlsEditorServicesApp;
import com.taiger.toreadorlab.owlseditor.domain.workflow.ToreadorReferenceModeEnum;
import com.taiger.toreadorlab.owlseditor.domain.workflow.ToreadorWorkflow;
import com.taiger.toreadorlab.owlseditor.web.rest.errors.ToreadorOntoEditorException;

/**
 * @author ricardo.melero@taiger.com
 *
 */
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "dev")
@SpringBootTest(classes = ToreadorLabOwlsEditorServicesApp.class)
public class CreateOWLSfromWorkflowTest {
	@Autowired
	WorkflowService workflowService;

	public static String getJSONFromResource(String path) {
		URL url = CreateOWLSfromWorkflowTest.class.getResource(path);
		File fileToRead = new File(url.getFile());
		String json = "";
		try (FileReader fileStream = new FileReader(fileToRead);
				BufferedReader bufferedReader = new BufferedReader(fileStream)) {

			String line = "";

			while ((line = bufferedReader.readLine()) != null) {
				json = json + line;
			}

		} catch (FileNotFoundException ex) {
			// exception Handling
		} catch (IOException ex) {
			// exception Handling
		}
		return json;
	}

//	@Test
//	public void testSequencePrediction() throws ToreadorOntoEditorException, JsonParseException, JsonMappingException, IOException {
//		String strworkflow = getJSONFromResource("/testWorkflow/prediction.json");
//		System.out.println("workflow:::" + strworkflow);
//		ObjectMapper mapper = new ObjectMapper();
//		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//		ToreadorWorkflow workflow = mapper.readValue(strworkflow, ToreadorWorkflow.class);
//		String transformWorkflowToOWLS = workflowService.transformWorkflowToOWLS(workflow, ToreadorReferenceModeEnum.INSERT_OWLS_TRIPLES);
//		System.out.println(transformWorkflowToOWLS);
//		FileUtils.writeStringToFile(new File("predictionGraphSequenceTransform.xml"), transformWorkflowToOWLS,Charset.defaultCharset());
//	}
	
//	@Test
//	public void testSequence() throws ToreadorOntoEditorException, JsonParseException, JsonMappingException, IOException {
//		String strworkflow = getJSONFromResource("/testWorkflow/graphSequence.json");
//		System.out.println("workflow:::" + strworkflow);
//		ObjectMapper mapper = new ObjectMapper();
//		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//		ToreadorWorkflow workflow = mapper.readValue(strworkflow, ToreadorWorkflow.class);
//		String transformWorkflowToOWLS = workflowService.transformWorkflowToOWLS(workflow, ToreadorReferenceModeEnum.INSERT_OWLS_TRIPLES);
//		System.out.println(transformWorkflowToOWLS);
//		FileUtils.writeStringToFile(new File("graphSequenceTransform.xml"), transformWorkflowToOWLS,Charset.defaultCharset());
//	}
	
//	@Test
//	public void testSplitJoin() throws ToreadorOntoEditorException, JsonParseException, JsonMappingException, IOException {
//		String strworkflow = getJSONFromResource("/testWorkflow/graphSplitJoin.json");
//		System.out.println("workflow:::" + strworkflow);
//		ObjectMapper mapper = new ObjectMapper();
//		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//		ToreadorWorkflow workflow = mapper.readValue(strworkflow, ToreadorWorkflow.class);
//		String transformWorkflowToOWLS = workflowService.transformWorkflowToOWLS(workflow, ToreadorReferenceModeEnum.INSERT_OWLS_TRIPLES);
//		System.out.println(transformWorkflowToOWLS);
//		FileUtils.writeStringToFile(new File("graphSplitJoinTransform.xml"), transformWorkflowToOWLS,Charset.defaultCharset());
//	}
	
//	@Test
//	public void testSplit() throws ToreadorOntoEditorException, JsonParseException, JsonMappingException, IOException {
//		String strworkflow = getJSONFromResource("/testWorkflow/graphSplit.json");
//		System.out.println("workflow:::" + strworkflow);
//		ObjectMapper mapper = new ObjectMapper();
//		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//		ToreadorWorkflow workflow = mapper.readValue(strworkflow, ToreadorWorkflow.class);
//		String transformWorkflowToOWLS = workflowService.transformWorkflowToOWLS(workflow, ToreadorReferenceModeEnum.INSERT_OWLS_TRIPLES);
//		System.out.println(transformWorkflowToOWLS);
//		FileUtils.writeStringToFile(new File("graphSplitTransform.xml"), transformWorkflowToOWLS,Charset.defaultCharset());
//	}
	
//	@Test
//	public void testSplitJoin() throws ToreadorOntoEditorException, JsonParseException, JsonMappingException, IOException {
//		String strworkflow = getJSONFromResource("/testWorkflow/test5.json");
//		System.out.println("workflow:::" + strworkflow);
//		ObjectMapper mapper = new ObjectMapper();
//		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//		ToreadorWorkflow workflow = mapper.readValue(strworkflow, ToreadorWorkflow.class);
//		String transformWorkflowToOWLS = workflowService.transformWorkflowToOWLS(workflow, ToreadorReferenceModeEnum.INSERT_OWLS_TRIPLES);
//		System.out.println(transformWorkflowToOWLS);
//		FileUtils.writeStringToFile(new File("graphSplitJoinTransform.xml"), transformWorkflowToOWLS,Charset.defaultCharset());
//	}
	
	@Test
	public void testSplitJoin() throws ToreadorOntoEditorException, JsonParseException, JsonMappingException, IOException {
		String strworkflow = getJSONFromResource("/testWorkflow/graphSequence.json");
		System.out.println("workflow:::" + strworkflow);
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		ToreadorWorkflow workflow = mapper.readValue(strworkflow, ToreadorWorkflow.class);
		String transformWorkflowToOWLS = workflowService.transformWorkflowToOWLS(workflow, ToreadorReferenceModeEnum.INSERT_OWLS_TRIPLES);
		System.out.println(transformWorkflowToOWLS);
		FileUtils.writeStringToFile(new File("graphSequenceTransform.xml"), transformWorkflowToOWLS,Charset.defaultCharset());
	}
	
}
