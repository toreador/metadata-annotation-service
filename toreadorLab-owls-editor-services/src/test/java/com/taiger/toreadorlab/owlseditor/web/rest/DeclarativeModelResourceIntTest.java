package com.taiger.toreadorlab.owlseditor.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.taiger.toreadorlab.owlseditor.ToreadorLabOwlsEditorServicesApp;
import com.taiger.toreadorlab.owlseditor.domain.DeclarativeModel;
import com.taiger.toreadorlab.owlseditor.repository.DeclarativeModelRepository;
import com.taiger.toreadorlab.owlseditor.service.DeclarativeModelService;
import com.taiger.toreadorlab.owlseditor.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the DeclarativeModelResource REST controller.
 *
 * @see DeclarativeModelResource
 */
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "dev")
@SpringBootTest(classes = ToreadorLabOwlsEditorServicesApp.class)
public class DeclarativeModelResourceIntTest {

    private static final String DEFAULT_GRAPH_URI = "AAAAAAAAAA";
    private static final String UPDATED_GRAPH_URI = "BBBBBBBBBB";

    @Autowired
    private DeclarativeModelRepository declarativeModelRepository;

    @Autowired
    private DeclarativeModelService declarativeModelService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDeclarativeModelMockMvc;

    private DeclarativeModel declarativeModel;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DeclarativeModelResource declarativeModelResource = new DeclarativeModelResource(declarativeModelService);
        this.restDeclarativeModelMockMvc = MockMvcBuilders.standaloneSetup(declarativeModelResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DeclarativeModel createEntity(EntityManager em) {
        DeclarativeModel declarativeModel = new DeclarativeModel()
            .graphUri(DEFAULT_GRAPH_URI);
        return declarativeModel;
    }

    @Before
    public void initTest() {
        declarativeModel = createEntity(em);
    }

    @Test
    @Transactional
    public void createDeclarativeModel() throws Exception {
        int databaseSizeBeforeCreate = declarativeModelRepository.findAll().size();

        // Create the DeclarativeModel
        restDeclarativeModelMockMvc.perform(post("/api/declarative-models")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(declarativeModel)))
            .andExpect(status().isCreated());

        // Validate the DeclarativeModel in the database
        List<DeclarativeModel> declarativeModelList = declarativeModelRepository.findAll();
        assertThat(declarativeModelList).hasSize(databaseSizeBeforeCreate + 1);
        DeclarativeModel testDeclarativeModel = declarativeModelList.get(declarativeModelList.size() - 1);
        assertThat(testDeclarativeModel.getGraphUri()).isEqualTo(DEFAULT_GRAPH_URI);
    }

    @Test
    @Transactional
    public void createDeclarativeModelWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = declarativeModelRepository.findAll().size();

        // Create the DeclarativeModel with an existing ID
        declarativeModel.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDeclarativeModelMockMvc.perform(post("/api/declarative-models")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(declarativeModel)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<DeclarativeModel> declarativeModelList = declarativeModelRepository.findAll();
        assertThat(declarativeModelList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllDeclarativeModels() throws Exception {
        // Initialize the database
        declarativeModelRepository.saveAndFlush(declarativeModel);

        // Get all the declarativeModelList
        restDeclarativeModelMockMvc.perform(get("/api/declarative-models?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(declarativeModel.getId().intValue())))
            .andExpect(jsonPath("$.[*].graphUri").value(hasItem(DEFAULT_GRAPH_URI.toString())));
    }

    @Test
    @Transactional
    public void getDeclarativeModel() throws Exception {
        // Initialize the database
        declarativeModelRepository.saveAndFlush(declarativeModel);

        // Get the declarativeModel
        restDeclarativeModelMockMvc.perform(get("/api/declarative-models/{id}", declarativeModel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(declarativeModel.getId().intValue()))
            .andExpect(jsonPath("$.graphUri").value(DEFAULT_GRAPH_URI.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDeclarativeModel() throws Exception {
        // Get the declarativeModel
        restDeclarativeModelMockMvc.perform(get("/api/declarative-models/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDeclarativeModel() throws Exception {
        // Initialize the database
        declarativeModelService.save(declarativeModel);

        int databaseSizeBeforeUpdate = declarativeModelRepository.findAll().size();

        // Update the declarativeModel
        DeclarativeModel updatedDeclarativeModel = declarativeModelRepository.findOne(declarativeModel.getId());
        updatedDeclarativeModel
            .graphUri(UPDATED_GRAPH_URI);

        restDeclarativeModelMockMvc.perform(put("/api/declarative-models")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDeclarativeModel)))
            .andExpect(status().isOk());

        // Validate the DeclarativeModel in the database
        List<DeclarativeModel> declarativeModelList = declarativeModelRepository.findAll();
        assertThat(declarativeModelList).hasSize(databaseSizeBeforeUpdate);
        DeclarativeModel testDeclarativeModel = declarativeModelList.get(declarativeModelList.size() - 1);
        assertThat(testDeclarativeModel.getGraphUri()).isEqualTo(UPDATED_GRAPH_URI);
    }

    @Test
    @Transactional
    public void updateNonExistingDeclarativeModel() throws Exception {
        int databaseSizeBeforeUpdate = declarativeModelRepository.findAll().size();

        // Create the DeclarativeModel

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDeclarativeModelMockMvc.perform(put("/api/declarative-models")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(declarativeModel)))
            .andExpect(status().isCreated());

        // Validate the DeclarativeModel in the database
        List<DeclarativeModel> declarativeModelList = declarativeModelRepository.findAll();
        assertThat(declarativeModelList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDeclarativeModel() throws Exception {
        // Initialize the database
        declarativeModelService.save(declarativeModel);

        int databaseSizeBeforeDelete = declarativeModelRepository.findAll().size();

        // Get the declarativeModel
        restDeclarativeModelMockMvc.perform(delete("/api/declarative-models/{id}", declarativeModel.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DeclarativeModel> declarativeModelList = declarativeModelRepository.findAll();
        assertThat(declarativeModelList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DeclarativeModel.class);
        DeclarativeModel declarativeModel1 = new DeclarativeModel();
        declarativeModel1.setId(1L);
        DeclarativeModel declarativeModel2 = new DeclarativeModel();
        declarativeModel2.setId(declarativeModel1.getId());
        assertThat(declarativeModel1).isEqualTo(declarativeModel2);
        declarativeModel2.setId(2L);
        assertThat(declarativeModel1).isNotEqualTo(declarativeModel2);
        declarativeModel1.setId(null);
        assertThat(declarativeModel1).isNotEqualTo(declarativeModel2);
    }
}
