package com.taiger.toreadorlab.owlseditor.cucumber.stepdefs;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import com.taiger.toreadorlab.owlseditor.ToreadorLabOwlsEditorServicesApp;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = ToreadorLabOwlsEditorServicesApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
