export class ConstantsSrv {
  constructor() {
    'ngInject';

    var data = {

           //Ontology Manager
           'getKnowledgeClasses': '/ontology/getClasses',
           'getKnowledgeIndividuals': '/ontology/getIndividuals',
           'getKnowledgeProperties': '/ontology/getPropertyValues',
           'possibleProperties': '/ontology/possibleProperties',
           'possibleObjects': '/services/possibleObjects',
           'addIndividual': '/ontology/individual',
           'updateIndividual': '/ontology/individual',
           'deleteIndividual': '/ontology/individual',
           'obtainServicesCatalogue': '/services/obtainServicesCatalogue',
           'reloadOntology': '/ontology/reloadOntology',
           'showUser':'/showUser',
           'getAllUsers':'/users',
            'account':'/account',
            'authenticate':'/authenticate',
            'isAuthenticated':'/authenticate',
            'register':'/register',
            'getServiceToUser':'/ontology/getServiceToUser',
            'getServicesNames':'/ontology/getServicesNames',
            'assignServiceToUser':'/ontology/assignServiceToUser',
            'deleteAssignedUserService':'/ontology/deleteAssignedUserService'

    };

       /*
        * Function to get the value of the constants file
        */
       function getValue(key) {
           var value;
           value = data[key];
           return value;
       }

       return {
           getValue: getValue
       };

  }

}
