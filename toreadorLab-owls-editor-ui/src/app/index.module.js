/* global  moment:false */

import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { NavbarController } from './components/navbar/navbar.controller';
import { NavbarManagerService } from './services/navbarManager.service';
import { ManagementManagerService } from './services/managementManager.service';
import { ConfigMainState } from './components/ontology/ontologyManager.state';
import { OntologyManagerController } from './components/ontology/ontologyManager.controller';
import { OntologyManagerService } from './services/ontologyManager.service';
import { ConstantsSrv } from './commons/constants.service';
import { OntologyManagerDialogController } from './components/ontology/ontologyManager-dialog.controller';
import { OntologyManagerDeleteDialogController } from './components/ontology/ontologyManager-delete-dialog.controller';
import { HomeController } from './components/home/home.controller';
//import { ManagementController } from './components/management/management.controller';
import { RegisterController } from './components/register/register.controller';
import { UserManagementController } from './components/admin/user-management/user-management.controller';
import { Principal } from './services/auth/principal.service';
import { Account } from './services/auth/account.service';
import { User } from './services/user/user.service';
import { AlertService } from './components/alert/alert.service';
import { PaginationUtil } from './components/util/pagination-util.service';
import { ParseLinks } from './components/util/parse-links.service';
import { UserManagementDialogController } from './components/admin/user-management/user-management-dialog.controller';
import { UserManagementDetailController } from './components/admin/user-management/user-management-detail.controller';
import { UserManagementDeleteController } from './components/admin/user-management/user-management-delete-dialog.controller';
import { ActivationController } from './account/activate/activate.controller';
import { PasswordController } from './account/password/password.controller';
import { SettingsController } from './account/settings/settings.controller';
import { AssignationController } from './components/assignation/assignation.controller';
import { AssignationDetailController } from './components/assignation/assignation-detail.controller';
import { AssignManagerService } from './services/assignManager.service';




angular.module('ontology', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'ui.bootstrap', 'toastr'])

  .constant('moment', moment)
  .constant('paginationConstants', {
    'itemsPerPage': 20
  })
  .config(config)
  .config(routerConfig)
  .config(ConfigMainState)
 // .run(runBlock)

  .controller('NavbarController', NavbarController)
  .controller('OntologyManagerController', OntologyManagerController)
  .controller('OntologyManagerDialogController', OntologyManagerDialogController)
  .controller('OntologyManagerDeleteDialogController', OntologyManagerDeleteDialogController)
  .controller('HomeController', HomeController)
  .controller('UserManagementController', UserManagementController)
  .controller('RegisterController', RegisterController)
  .controller('UserManagementDialogController', UserManagementDialogController)
  .controller('UserManagementDetailController', UserManagementDetailController)
  .controller('UserManagementDeleteController', UserManagementDeleteController)
  .controller('ActivationController', ActivationController)
  .controller('PasswordController', PasswordController)
  .controller('SettingsController', SettingsController)
  .controller('AssignationController', AssignationController)
  .controller('AssignationDetailController', AssignationDetailController)

  //.controller('ManagementController', ManagementController)
  .service('OntologyManagerService', OntologyManagerService)
  .service('Principal', Principal)
  .service('Account', Account)
  .service('User', User)
  .service('AlertService', AlertService)
  .service('PaginationUtil', PaginationUtil)
  .service('ParseLinks', ParseLinks)
  .service('NavbarManagerService', NavbarManagerService)
  .service('ManagementManagerService', ManagementManagerService)
  .service('ConstantsSrv', ConstantsSrv)
  .service('AssignManagerService', AssignManagerService)
