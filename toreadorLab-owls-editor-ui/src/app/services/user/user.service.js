export class User {
  constructor($resource,$cookies) {
    'ngInject';
      //http://toreador.hopto.org:8081/toreadorLab-owls-editor-service
      //http://localhost:8080
        var service = $resource('http://localhost:8080/api/users/:login', {}, {//'api/users/:login'
          'query': {
              method: 'GET',
              headers:{
                'Authorization':'Bearer ' +angular.fromJson($cookies.get('token'))
              },
              isArray: true
            },

            'get': {
                method: 'GET',
                headers:{
                  'Authorization':'Bearer ' +angular.fromJson($cookies.get('token'))
                },
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    //console.log(data);
                    return data;
                },

            },
            'save': {
              method:'POST',
              headers:{
                'Authorization':'Bearer ' +angular.fromJson($cookies.get('token'))
              }
            },
            'update': {
              method:'PUT',
              headers:{
                'Authorization':'Bearer ' +angular.fromJson($cookies.get('token'))
              }
              },
            'delete':{
              method:'DELETE',
              headers:{
                'Authorization':'Bearer ' +angular.fromJson($cookies.get('token'))
              }
          }
        });


        return service;
    }
}
