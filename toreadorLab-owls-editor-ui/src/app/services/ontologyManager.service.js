export class OntologyManagerService {
  constructor($http, ConstantsSrv,$cookies) {
    'ngInject';

    var altLabelTag = 'http://www.w3.org/2004/02/skos/core#altLabel';
    //var testLabel = 'http://www.w3.org/2004/02/skos/core#testLabel';
    var labelTag = 'http://www.w3.org/2000/01/rdf-schema#label';
    var typeTag = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type';

    var localTesting = 'http://localhost:8080/api';
    //var localTesting = 'http://toreador.hopto.org:8081/toreadorLab-owls-editor-service/api';
    //http://toreadorplatform.eng.it/api/catalogue

    var service = {
      getClasses: getClasses,
      getIndividuals: getIndividuals,
      getProperties: getProperties,
      possibleProperties: possibleProperties,
      possibleObjects: possibleObjects,
      addIndividual: addIndividual,
      updateIndividual: updateIndividual,
      deleteIndividual: deleteIndividual,
      getPropertyValuesWithProperty: getPropertyValuesWithProperty,
      containsElementId: containsElementId,
      filterArray: filterArray,
      getElementPrintableLabel: getElementPrintableLabel,
      getElementPrintableSelectedClass: getElementPrintableSelectedClass,
      createDefaultIndividual: createDefaultIndividual,
      getInitialValuePropertiesEmpty: getInitialValuePropertiesEmpty,
      validateURL: validateURL,
      requiredProperties: requiredProperties,
      getValueIdFromArray: getValueIdFromArray,
      obtainServicesCatalogue: obtainServicesCatalogue,
      reloadOntology: reloadOntology,
      getLabelOrIdFromObject: getLabelOrIdFromObject,

      getServiceToUser: getServiceToUser,
      loadServicesSelect: loadServicesSelect,


      assignServiceToUser: assignServiceToUser,
      saveServiceToUser: saveServiceToUser,
      deleteAssignedUserService:deleteAssignedUserService


    };
    return service;


    function getClasses() {
      console.log("RELOADING CLASSES 22321");
      var config = {
        // headers: {
        //   'accept': 'application/json, text/plain'
        // },
        // params: {}
      }

      //return $http.get(localTesting + ConstantsSrv.getValue('getKnowledgeClasses'), config);
      return $http.get(localTesting + ConstantsSrv.getValue('getKnowledgeClasses'), {
        headers: {'Authorization': 'Bearer ' + angular.fromJson($cookies.get('token'))}
      }).then(function (response) {
        return response;
      });


    }

    function getIndividuals(className) {
      return $http.get(localTesting + ConstantsSrv.getValue('getKnowledgeIndividuals'), {
        headers: {'Authorization': 'Bearer ' + angular.fromJson($cookies.get('token'))},
        params: {
          cls: className
        }
      });
    }

    function deleteAssignedUserService(idUser,idGraph,graph,className){

      var json = {
        "idUser": idUser,
        "idGraph": idGraph,
        "graph": graph,
        "className": className,

      }
      return $http.post(localTesting + ConstantsSrv.getValue('deleteAssignedUserService'),json,{
        headers:{'Authorization':'Bearer ' +angular.fromJson($cookies.get('token'))}
      }).then(function(response){

        return response;
      });

    }

    function reloadOntology() {

      //return $http.post(localTesting + ConstantsSrv.getValue('reloadOntology'));
      /*return $http.post(localTesting + ConstantsSrv.getValue('reloadOntology'),{
        headers:{'Authorization':'Bearer ' +JSON.parse($cookies.get('token'))}
      }).then(function(response){
        return response;
      });
*/

      var req = {
        method: 'POST',
        url: localTesting + ConstantsSrv.getValue('reloadOntology'),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Bearer ' + angular.fromJson($cookies.get('token'))
          // or  'Content-Type':'application/json'
        },
        data: {}
      }
      return $http(req).then(function (response) {
        return response;
      });

    }

    function obtainServicesCatalogue() {

      return $http.get(localTesting + ConstantsSrv.getValue('obtainServicesCatalogue'), {
        headers: {'Authorization': 'Bearer ' + angular.fromJson($cookies.get('token'))},
      });

    }


    function getProperties(individual) {
      return $http.get(localTesting + ConstantsSrv.getValue('getKnowledgeProperties'), {
        headers: {'Authorization': 'Bearer ' + angular.fromJson($cookies.get('token'))},
        params: {
          individual: individual
        }
      });
    }

    function possibleProperties(className) {
      return $http.get(localTesting + ConstantsSrv.getValue('possibleProperties'), {
        headers: {'Authorization': 'Bearer ' + angular.fromJson($cookies.get('token'))},
        params: {
          cls: className
        }
      });
    }

    function possibleObjects(property) {
      return $http.get(localTesting + ConstantsSrv.getValue('possibleObjects'), {
        headers: {'Authorization': 'Bearer ' + angular.fromJson($cookies.get('token'))},
        params: {
          property: property
        }
      });
    }

    function addIndividual(propertyValues, context) {
      return $http.post(localTesting + ConstantsSrv.getValue('addIndividual'), propertyValues, {
        headers: {'Authorization': 'Bearer ' + angular.fromJson($cookies.get('token'))},
        params: {
          context: context
        }
      });
    }

    function updateIndividual(propertyValues, id, context) {
      console.log('id: ' + id + ' context: ' + context);
      console.log("Aqui va la label");
      //console.log(altLabel);
      return $http.put(localTesting + ConstantsSrv.getValue('updateIndividual'), propertyValues, {
        headers: {'Authorization': 'Bearer ' + angular.fromJson($cookies.get('token'))},
        params: {
          id: id,
          context: context
          //altLabel:altLabel
        }
      });
    }

    function deleteIndividual(id, context) {
      console.log('id: ' + id + ' context: ' + context);
      return $http.delete(localTesting + ConstantsSrv.getValue('deleteIndividual'), {
        headers: {'Authorization': 'Bearer ' + angular.fromJson($cookies.get('token'))},
        params: {
          id: id,
          context: context
        }
      });
    }

    function getPropertyValuesWithProperty(propertyValuesList, property) {
      for (var i = 0; i < propertyValuesList.length; i++) {
        if (propertyValuesList[i].property.id == property) {
          return propertyValuesList[i];
        }
      }
      return null;
    }

    function containsElementId(elements, id) {
      for (var i = 0; i < elements.length; i++) {
        if (elements[i].id == id) {
          return true;
        }
      }
      return false;
    }

    function getLabelOrIdFromObject(element) {
      var arrayId = element.id ? (element.id).split('#') : [];
      var label = arrayId.length === 2 ? arrayId[1] : '';
      if (!label) {
        label = element.label ? element.label : '';
      }
      return label;
    }


    function checkIfContains(substring) {
      return function (element) {
        var isObject = element.hasOwnProperty('label') || element.hasOwnProperty('id');
        var label = isObject ? getLabelOrIdFromObject(element) : element;
        //console.log("label: "+label+" substring: "+substring+ " isObject: "+isObject);
        return label ? ((label.toLowerCase()).indexOf(substring.toLowerCase()) >= 0) : false;
      }
    }

    function filterArray(arrayStrings, substring) {
      //console.log('arrayStrings: '+arrayStrings);
      //console.log('substring: '+substring);
      return substring != '' ? arrayStrings.filter(checkIfContains(substring)) : arrayStrings;
      //return arrayStrings;
    }

    function getElementPrintableLabel(element) {
      return element ? element.label || (element.id ? element.id.substring(element.id.indexOf('#') + 1) : null) : null;
    }

    function getElementPrintableSelectedClass(element) {
      return element && element.indexOf('#') > -1 ? element.substring(element.indexOf('#') + 1) : element;
    }

    function getInitialProperties(propertyId) {
      return {
        'id': propertyId,
        'label': null, 'types': null, 'aliases': null,
        'typesIds': [], 'typesString': null
      }
    }

    function getInitialValuePropertiesEmpty(property, className) {
      var id = (property === typeTag) ? className : null;
      return {
        'id': id, 'label': '',
        'types': null, 'aliases': null, 'typesIds': [], 'typesString': null
      };
    }

    function getInitialValueProperties(property, data, className) {
      var valueProperties = [];
      if (property && data) {
        for (var i = 0; i < data.length; i++) {
          if (data[i].property.id === property) {
            valueProperties = data[i].values;
            break;
          }
        }
      }
      if (valueProperties.length === 0) {
        valueProperties = [getInitialValuePropertiesEmpty(property, className)];
      }
      return valueProperties;
    }

    function getPropertiesNames() {
      return [
        altLabelTag,
       // testLabel,
        labelTag,
        typeTag
      ];
    }

    function requiredProperties() {
      return [
        labelTag,
        typeTag
      ];
    }

    function getValueIdFromArray(arrayProperties) {
      var arrayPropertiesId = [];
      angular.forEach(arrayProperties, function (propertyObject) {
        arrayPropertiesId.push(propertyObject.id);
      });
      return arrayPropertiesId;
    }

    function addChoicesAttributePropertyType(property, classesList, propertyValues) {
      var typeProperty = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type';
      if (property === typeProperty && classesList) {
        propertyValues.choicesType = classesList;
      }
    }

    function addChoicesPropertyObjects(property, propertyObjects, propertyValues) {
      var arrayObjects = propertyObjects[property];
      //console.log("adding to choice: "+arrayObjects);
      if (!!arrayObjects) {
        console.log("in if!!");
        propertyValues.choicesObjects = arrayObjects;
      }
    }

    function createDefaultIndividualProperties(data, className, newProperties, classesList, propertyObjects) {
      var defaultProperties = getPropertiesNames();
      var properties = newProperties ? defaultProperties.concat(newProperties) : defaultProperties;
      var propertyValuesList = [];
      angular.forEach(properties, function (property) {
        var propertyValues = {
          'property': getInitialProperties(property),
          'values': getInitialValueProperties(property, data, className)
        };
        addChoicesAttributePropertyType(property, classesList, propertyValues);
        addChoicesPropertyObjects(property, propertyObjects, propertyValues);
        propertyValuesList.push(propertyValues);
      });
      return propertyValuesList;
    }

    function createDefaultIndividual(data, className, newProperties, propertyObjects) {
      return getClasses().then(function (classesList) {
        return createDefaultIndividualProperties(data, className, newProperties, classesList.data, propertyObjects);
      }, function (data) {
        return createDefaultIndividualProperties(data, className, newProperties, [], propertyObjects);
      });
    }

    function validateURL(textval) {
      var urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+:&%$#=~_-]+))*$/;
      return urlregex.test(textval);
    }


    function getServiceToUser(id) {

      return $http.get(localTesting + ConstantsSrv.getValue('getServiceToUser') + '/' + id, {
        headers: {'Authorization': 'Bearer ' + angular.fromJson($cookies.get('token'))}
      }).then(function (response) {
        return response;
      });
    }

    function loadServicesSelect() {

      //console.log(id);
      return $http.get(localTesting + ConstantsSrv.getValue('getServicesNames'), {
        headers: {'Authorization': 'Bearer ' + angular.fromJson($cookies.get('token'))}
      }).then(function (response) {
        return response;
      });
    }

    function assignServiceToUser() {
      //console.log(id);
      return $http.get(localTesting + ConstantsSrv.getValue('account'), {
        headers: {'Authorization': 'Bearer ' + angular.fromJson($cookies.get('token'))}
      }).then(function (response) {
        return response;
      });

    }

    function saveServiceToUser(idUser, idGraph, graph, className) {

      console.log(idUser);
      console.log(idGraph);
      console.log(graph);
      console.log(className);


      var json = {
        "idUser": idUser,
        "idGraph": idGraph,
        "graph": graph,
        "className": className,

      }


      return $http.post(localTesting + ConstantsSrv.getValue('assignServiceToUser'),json,{
        headers:{'Authorization':'Bearer ' +angular.fromJson($cookies.get('token'))}
      }).then(function(response){
        return response;
      });

    }

  }
}

/*return $http.post(localTesting + ConstantsSrv.getValue('assignServiceToUser'),{

        headers:{'Authorization':'Bearer ' +angular.fromJson($cookies.get('token')),

        },
        params: {
          'idUser':idUser,
          'graph':service,
          'graph':graph,
          'className':className
        }
      }).then(function (response) {
        console.log("entramos por aqui.");
        return response;
      });
    */
