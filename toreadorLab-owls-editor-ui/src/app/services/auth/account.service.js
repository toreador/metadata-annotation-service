export class Account {
  constructor($resource, $cookies) {
    'ngInject';
        //toreador.hopto.org:8081/toreadorLab-owls-editor-service
        //localhost:8080
        var service = $resource('http://localhost:8080/api/account', {}, {//api/account'
            'get': { method: 'GET', params: {}, isArray: false,
              headers:{
                'Authorization':'Bearer ' +angular.fromJson($cookies.get('token'))
              },
                interceptor: {
                    response: function(response) {
                        // expose response
                        return response;
                    }
                }
            }
        });

        return service;
    }
}
