export class AssignManagerService {
  constructor($http, ConstantsSrv,$cookies) {
    'ngInject';

    var altLabelTag = 'http://www.w3.org/2004/02/skos/core#altLabel';
    var labelTag = 'http://www.w3.org/2000/01/rdf-schema#label';
    var typeTag = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type';

    var localTesting = 'http://localhost:8080/api';
    //var localTesting = 'http://toreador.hopto.org:8081/toreadorLab-owls-editor-service/api';
    //http://toreadorplatform.eng.it/api/catalogue

    var service = {
      getServiceToUser: getServiceToUser,
      loadServicesSelect:loadServicesSelect,
      assignServiceToUser:assignServiceToUser,
      saveServiceToUser:saveServiceToUser
    };
    return service;




    function getServiceToUser(id) {

      //console.log(id);
      return $http.get(localTesting + ConstantsSrv.getValue('getServiceToUser')+'/'+id,{
          headers:{'Authorization':'Bearer ' +angular.fromJson($cookies.get('token'))}
      }).then(function (response) {
        return response;
      });
    }

    function loadServicesSelect() {

      //console.log(id);
      return $http.get(localTesting + ConstantsSrv.getValue('getServicesNames'),{
        headers:{'Authorization':'Bearer ' +angular.fromJson($cookies.get('token'))}
      }).then(function (response) {
        return response;
      });
    }

    function assignServiceToUser() {
      //console.log(id);
      return $http.get(localTesting + ConstantsSrv.getValue('account'),{
        headers:{'Authorization':'Bearer ' +angular.fromJson($cookies.get('token'))}
      }).then(function(response){
        return response;
      });

    }

    function saveServiceToUser(user, service){

      return $http.post(localTesting + ConstantsSrv.getValue('assignServiceToUser'),user,{

        headers:{'Authorization':'Bearer ' +angular.fromJson($cookies.get('token')),

        },
        params: {

        }
      }).then(function (response) {
        return response;
      });


    }


  /*
    function authenticate(login, pass) {
      console.log("authenticate navbarManager");
      var json = {
        username: login,
        password: pass,
        rememberMe: true
      };
      return $http.post(localTesting + ConstantsSrv.getValue('authenticate'), json).then(function(response){
        return response;
      });
    }

    function getUser() {
      console.log("account navbarManager");
      //console.log($cookies.get('token'));

      return $http.get(localTesting + ConstantsSrv.getValue('account'),{
        headers:{'Authorization':'Bearer ' +angular.fromJson($cookies.get('token'))}
      }).then(function(response){
        return response;
      });
    }
  */




  }

}
