export class NavbarManagerService {
  constructor($http, ConstantsSrv,$cookies) {
    'ngInject';

    var altLabelTag = 'http://www.w3.org/2004/02/skos/core#altLabel';
    var labelTag = 'http://www.w3.org/2000/01/rdf-schema#label';
    var typeTag = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type';

    var localTesting = 'http://localhost:8080/api';
    //var localTesting = 'http://toreador.hopto.org:8081/toreadorLab-owls-editor-service/api';
    //http://toreadorplatform.eng.it/api/catalogue

    var service = {
      showUser: showUser,
      getUser: getUser,
      authenticate: authenticate

    };
    return service;




    function showUser() {
      return $http.post(localTesting + ConstantsSrv.getValue('showUser')).then(function (response) {
        return response;
      });
    }

    function authenticate(login, pass) {
      console.log("authenticate navbarManager");
      var json = {
        username: login,
        password: pass,
        rememberMe: true
      };
      return $http.post(localTesting + ConstantsSrv.getValue('authenticate'), json).then(function(response){
        return response;
      });
    }

    function getUser() {
      console.log("account navbarManager");
      //console.log($cookies.get('token'));

      return $http.get(localTesting + ConstantsSrv.getValue('account'),{
        headers:{'Authorization':'Bearer ' +angular.fromJson($cookies.get('token'))}
      }).then(function(response){
        return response;
      });
    }





  }

}
