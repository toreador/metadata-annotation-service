export class ManagementManagerService {
  constructor($http, ConstantsSrv,$cookies) {
    'ngInject';

    var altLabelTag = 'http://www.w3.org/2004/02/skos/core#altLabel';
    var labelTag = 'http://www.w3.org/2000/01/rdf-schema#label';
    var typeTag = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type';

    var localTesting = 'http://localhost:8080/api';
    //var localTesting = 'http://toreador.hopto.org:8081/toreadorLab-owls-editor-service/api';
    //http://toreadorplatform.eng.it/api/catalogue

    var service = {
      showUser: showUser,
      createNewUser:createNewUser
    };
    return service;





    function showUser() {
      console.log("showUser ManagementManager");
      //console.log($cookies.get('token'));

      return $http.get(localTesting + ConstantsSrv.getValue('account'),{
        headers:{'Authorization':'Bearer ' +angular.fromJson($cookies.get('token'))}
      }).then(function(response){
        return response;
      });
    }


    function createNewUser(login, pass) {
      console.log("createUser ManagementManager");
      //console.log($cookies.get('token'));


      var json = {
        "activated": true,
        "authorities": [
          "ROLE_ADMIN"
        ],
        "createdBy": "string",
        "createdDate": "2018-05-22T09:08:15.297Z",
        "email": "string",
        "firstName": "string",
        "id": 5,
        "imageUrl": "string",
        "langKey": "string",
        "lastModifiedBy": "string",
        "lastModifiedDate": "2018-05-22T09:08:15.298Z",
        "lastName": "string",
        "login": login,
        "password": pass
      }

      return $http.post(localTesting + ConstantsSrv.getValue('register'),json,{
        headers:{'Authorization':'Bearer ' +angular.fromJson($cookies.get('token'))}
      }).then(function(response){
        return response;
      });
    }





  }

}
