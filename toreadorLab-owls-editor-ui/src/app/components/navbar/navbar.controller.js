export class NavbarController {
  constructor($stateParams, $scope, NavbarManagerService, $state, $cookies) {
    'ngInject';

    console.log('navController: active');
    var vm = this;


    vm.showUser = showUser;
    vm.getUser = getUser;
    vm.logout = logout;
    vm.userManagement=userManagement;
    vm.listOfServices=listOfServices;
    vm.assignServiceState=assignServiceState;
    vm.isLogged=false;
    vm.isAdmin=false;

    isLogged();
    console.log("Nuestro usuario es:");
    console.log(vm.isAdmin);

    function isLogged(){
      if($cookies.get('token')!=null){
        vm.isLogged=true;
        console.log( angular.fromJson($cookies.get('user')).authorities);
        angular.fromJson($cookies.get('user')).authorities.forEach(function (authorities) {
          if (authorities == "ROLE_ADMIN") {
            //console.log(authorities);
            vm.isAdmin = true;
          }
          //console.log(authorities);
        });
      }else{
        vm.isLogged=false;
      }



    }

    function showUser() {
      NavbarManagerService.showUser().then(function (response) {
        console.log(response);
        console.log('Show User')
      })
      //vm.getClasses();
    }

    function getUser() {
      //console.log('get User1');

      NavbarManagerService.authenticate(vm.login,vm.password).then(function (response) {
        vm.isLogged=true;
        //console.log(response.data);
        $cookies.putObject('token', response.data.id_token);

        NavbarManagerService.getUser().then(function(response){
          console.log("user-management");
          //console.log(response);
          $cookies.putObject('user', response.data);
          console.log($cookies.get('user'));
          console.log(response.data.authorities);
          response.data.authorities.forEach(function (authorities) {
            if (authorities == "ROLE_ADMIN") {
              //console.log(authorities);
              vm.isAdmin = true;
            }
            //console.log(authorities);
          });
          //$state.go('ontology-manager');
          //$state.go('management-manager');

        });

      })
      //vm.getClasses();
    }

    function listOfServices() {
      //console.log($cookies.getObject('token'));
      $state.go('ontology-manager');
      //vm.getClasses();
    }



    function assignServiceState() {
      //console.log($cookies.getObject('token'));
      $state.go('assign-manager');
      //vm.getClasses();
    }


    function userManagement() {
      //console.log($cookies.getObject('token'));
      $state.go('user-management');
      //vm.getClasses();
    }

    function logout() {
      vm.isLogged=false;
      $cookies.remove('token');
      $cookies.remove('user');
      $state.go('home').then(function () {$state.reload();});

      //console.log($cookies.getObject('token'));
      //vm.getClasses();
    }



  }

}
