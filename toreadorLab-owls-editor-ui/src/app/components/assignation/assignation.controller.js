
export class AssignationController {
  constructor(Principal, User,AlertService,$state,pagingParams, paginationConstants,ParseLinks,$cookies,AssignManagerService ) {
    'ngInject';//ParseLinks

    var vm = this;

        vm.authorities = ['ROLE_USER', 'ROLE_ADMIN','USER_TEST'];
        vm.currentAccount = null;
        vm.languages = null;
        vm.loadAll = loadAll;
        vm.setActive = setActive;
        vm.getServices=getServices;
        vm.getServices=getServices;
        vm.users = [];
        vm.page = 1;
        vm.totalItems = null;
        vm.clear = clear;
        vm.links = null;
        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.transition = transition;
        vm.isUser=true;
        vm.user;
        vm.services=[];


        vm.loadAll();

        Principal.identity().then(function(account) {
            vm.currentAccount = account;
        });

        function getServiceToUser(){

          AssignManagerService.getServiceToUser(vm.user.id).then(function (response) {
            vm.services = response.data;
            console.log(vm.services);
          });
        }

        function getServices(){
          AssignManagerService.getServiceToUser(vm.user.id).then(function (response) {
            vm.services = response.data;
            console.log(vm.services);
            $state.go('assign-manager');
          });

        }
        function setActive (user, isActivated) {
            user.activated = isActivated;
            User.update(user, function () {
                vm.loadAll();
                vm.clear();
            });
        }

        function loadAll () {
            var roles = angular.fromJson($cookies.get('user')).authorities;
            console.log(angular.fromJson($cookies.get('user')).login);
            angular.forEach(roles,function(role){
              if(role==='ROLE_ADMIN'){
                vm.isUser=false;
              }
            });

            if(!vm.isUser) {

              User.query({
                page: pagingParams.page - 1,
                size: vm.itemsPerPage,
                sort: sort()
              }, onSuccess, onError);
            }else{

              User.get({
                login: angular.fromJson($cookies.get('user')).login
              }, onSuccess2, onError);

            }
        }

        function onSuccess(data, headers) {
            vm.links = ParseLinks.parse(headers('link'));
            vm.totalItems = headers('X-Total-Count');
            vm.queryCount = vm.totalItems;
            vm.page = pagingParams.page;
            vm.users = data;
        }

        function onSuccess2(data, headers) {
            console.log(data);
            vm.user = data;
            console.log(vm.user.id);
          getServiceToUser();
        }

        function onError(error) {
            AlertService.error(error.data.message);
        }

        function clear () {
            vm.user = {
                id: null, login: null, firstName: null, lastName: null, email: null,
                activated: null, langKey: null, createdBy: null, createdDate: null,
                lastModifiedBy: null, lastModifiedDate: null, resetDate: null,
                resetKey: null, authorities: null
            };
        }

        function sort () {
            var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
            if (vm.predicate !== 'id') {
                result.push('id');
            }
            return result;
        }

        function loadPage (page) {
            vm.page = page;
            vm.transition();
        }

        function transition () {
          console.log("transition");
          console.log($state.$current);
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }
    }
}


