
export class AssignationDetailController {
  constructor(Principal, User,AlertService,$stateParams,pagingParams, paginationConstants,ParseLinks,$cookies,AssignManagerService, OntologyManagerService,$state,$q) {
    'ngInject';//ParseLinks

    var vm = this;

    vm.classes = [];
    vm.individuals = [];
    vm.individualsFiltered = [];
    vm.classesFiltered = [];
    vm.propertyValues = [];
    vm.selectedClass = null;
    vm.selectedIndividual = null;
    vm.individualSearch = '';
    vm.classesSearch = '';
    vm.success = $stateParams.message;
    vm.loadingFilters = false;



    vm.authorities = ['ROLE_USER', 'ROLE_ADMIN','USER_TEST'];
    vm.currentAccount = null;
    vm.languages = null;
    vm.loadAll = loadAll;
    vm.setActive = setActive;
    vm.getServices=getServices;
    vm.loadServicesSelect=loadServicesSelect;
    vm.assignNewService=assignNewService;
    vm.users = [];
    vm.page = 1;
    vm.totalItems = null;
    vm.clear = clear;
    vm.links = null;
    vm.loadPage = loadPage;
    vm.predicate = pagingParams.predicate;
    vm.reverse = pagingParams.ascending;
    vm.itemsPerPage = paginationConstants.itemsPerPage;
    vm.transition = transition;
    vm.isUser=true;
    vm.isAdmin;

    vm.user;
    vm.services=[];
    vm.servicesSelect=[];
    vm.selectServ;

    vm.servicios=[];



    vm.getClasses = getClasses;
    vm.getIndividuals = getIndividuals;
    vm.getProperties = getProperties;

    vm.selectClass = selectClass;
    vm.getClassCssClass = getClassCssClass;
    vm.getElementPrintableLabel = getElementPrintableLabel;
    vm.getElementPrintableHumanReadable = getElementPrintableHumanReadable;
    vm.getElementPrintableSelectedClass = getElementPrintableSelectedClass;
    vm.getClassPrintableHumanReadable=getClassPrintableHumanReadable;
    vm.selectIndividual = selectIndividual;
    vm.getIndividualCssClass = getIndividualCssClass;
    vm.selectIndividualFromValue = selectIndividualFromValue;
    vm.loadClass = loadClass;
    vm.updateServices = updateServices;
    vm.reloadOntology = reloadOntology;
    vm.doFilterIndividual = doFilterIndividual;
    vm.doFilterClasses = doFilterClasses;
    vm.deleteAssignedUserService=deleteAssignedUserService;
    vm.addServiceToUser=addServiceToUser;
    vm.isAdmin=isAdmin;

    vm.includeInDatabase=includeInDatabase;


    ///

    //vm.getClasses();

    ///

    vm.loadAll();
    vm.loadServicesSelect();

    //vm.reloadOntology();
    //vm.updateServices();
    vm.userLogged=angular.fromJson($cookies.get('user'));
    //console.log(vm.user);

    vm.isAdmin();

    function addServiceToUser(id,graph,className) {
      console.log("guardandoo...");
      console.log(vm.user.id);
      console.log(id);
      console.log(graph);
      console.log(className);
      OntologyManagerService.saveServiceToUser(vm.user.id, id,graph,className).then(function () {

        $state.reload();
        isAdmin();
      });
      /*var obj ={idi:vm.user.id,idGraph:id,graph:graph,className:className};
      vm.servicios.push(obj);
      console.log(vm.servicios);*/
    }

    function deleteAssignedUserService(idUser,idGraph,graph,className){
      console.log("borramos el grafo asignado a este usuario.");
      OntologyManagerService.deleteAssignedUserService(idUser,idGraph,graph,className).then(function () {

        $state.reload();
        });
    }

    function includeInDatabase(){
        console.log("includeindatabase");
        vm.servicios.forEach(function (servicio){
          OntologyManagerService.saveServiceToUser(vm.user.id, servicio.idGraph,servicio.graph,servicio.className);
        });

        $q.all(vm.servicios).then(function () {
          $state.reload();
        });

    }

    function isAdmin(){
      vm.isAdmin=false;
      vm.userLogged.authorities.forEach(function(authorities) {
        if(authorities=="ROLE_ADMIN"){
          //console.log(authorities);
          vm.isAdmin=true;
        }
        //console.log(authorities);
      });
    }

    function reloadOntology() {
      console.log("RELOAD ONTOLOGY");
      OntologyManagerService.reloadOntology();
      vm.getClasses();
    }

    function updateServices() {
      console.log("UPDATE SERVICES");
      OntologyManagerService.obtainServicesCatalogue();
      vm.getClasses();
    }

    function getClasses() {
      console.log("RELOADING CLASSES");
      vm.loadingFilters = true;
      OntologyManagerService.getClasses().then(function (data) {
        vm.classes = data.data;
        angular.copy(vm.classes, vm.classesFiltered);
        vm.loadingFilters = false;
        if($stateParams.className) {
          vm.selectedClass = $stateParams.className;
          vm.getIndividuals($stateParams.className);
        }
      }, function (data) {

        vm.classes = [];
        vm.loadingFilters = false;
      });
    }

    function getIndividuals(className) {
      vm.loadingFilters = true;
      vm.individualSearch = '';
      vm.noIndividuals = false;
      OntologyManagerService.getIndividuals(className).then(function (data) {
        vm.individuals = data.data;
        angular.copy(vm.individuals, vm.individualsFiltered);
        vm.loadingFilters = false;
        if(vm.individuals.length === 0) {
          vm.noIndividuals = true;
        }
      }, function (data) {
        vm.individuals = [];
        vm.individualsFiltered = [];
        vm.loadingFilters = false;
      });
    }

    function getProperties(individual) {
      vm.loadingFilters = true;
      OntologyManagerService.getProperties(individual).then(function (data) {
        vm.propertyValues = data.data;
        vm.loadingFilters = false;
      }, function (data) {
        vm.propertyValues = [];
        vm.loadingFilters = false;
      });
    }

    function selectClass(id) {
      vm.selectedIndividual = null;
      vm.propertyValues = [];

      vm.loadClass(id);
    }

    function getClassCssClass(className) {
      return vm.selectedClass == className.id ? 'active' : '';
    }

    function getElementPrintableLabel(element) {
      if(element!=null)vm.selectServ=OntologyManagerService.getLabelOrIdFromObject(element);
      return OntologyManagerService.getElementPrintableLabel(element);
    }

    function getClassPrintableHumanReadable(element){
      //http://www.daml.org/services/owl-s/1.2/Grounding.owl#AtomicProcessGrounding
      if(element!=null)vm.selectServ=OntologyManagerService.getLabelOrIdFromObject(element);
      var label = OntologyManagerService.getElementPrintableLabel(element);
      var labArray=label.split("/");
      console.log(labArray[labArray.length-1]);
      return labArray[labArray.length-1];

    }


    function getElementPrintableHumanReadable(element){
      if(element!=null)vm.selectServ=OntologyManagerService.getLabelOrIdFromObject(element);
      var label = OntologyManagerService.getElementPrintableLabel(element);
      //HadoopINGESTION60hdfs-unzipper-2.7.2ServiceCategorya
      var lab=label.match(/[0-9].*/g)[0];
      var split=lab.split("ServiceCategory")[0];
      var cad=split.replace(/[0-9]/g, '').split("-");
      label="";
      for(var i=0;i<cad.length-1;i++){
        label+=cad[i]+"-";
      }

      //console.log(label.substr(0,label.length-1));
      return label.substr(0,label.length-1);
    }

    function getElementPrintableSelectedClass(className) {

      return OntologyManagerService.getElementPrintableSelectedClass(className);
    }

    function selectIndividual(individual) {
      vm.propertyValues = [];
      vm.selectedIndividual = individual;
      vm.getProperties(individual.id);
    }

    function getIndividualCssClass(individual) {

      return (vm.selectedIndividual && vm.selectedIndividual.id == individual.id) ? 'active' : '';
    }

    function selectIndividualFromValue(individual) {

      vm.propertyValues = [];
      vm.selectedIndividual = individual;
      vm.getProperties(individual.id, function() {
        var property = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type';
        console.log(property);
        var typesPV = OntologyManagerService.getPropertyValuesWithProperty(vm.propertyValues, property);
        if (typesPV != null) {
          for (var i = 0; i < typesPV.values.length; i++) {
            var typeId = typesPV.values[i].id;
            if (OntologyManagerService.containsElementId(vm.classes, typeId)) {
              vm.loadClass(typeId);
            }
            break;
          }
        }
        var otherProperties = OntologyManagerService.getPossibleProperties(individual.id);
        console.log('otherProps: '+otherProperties);
        for (var i = 0; i < otherProperties.length; i++) {
          var otherProperty = otherProperties[i].id;
          var typesOtherPV = OntologyManagerService.getPropertyValuesWithProperty(vm.propertyValues, property);
          if (typesOtherPV != null) {
            for (var i = 0; i < typesOtherPV.values.length; i++) {
              var typeOtherId = typesOtherPV.values[i].id;
              if (OntologyManagerService.containsElementId(vm.classes, typeOtherId)) {
                vm.loadClass(typeOtherId);
              }
              break;
            }
          }
        }
      });
    }

    function loadClass(id) {
      vm.individuals = [];
      vm.selectedClass = id;
      vm.getIndividuals(id);
    }

    function doFilterIndividual() {
      vm.loadingFilters = true;
      angular.copy(vm.individuals, vm.individualsFiltered);
      vm.individualsFiltered = OntologyManagerService.filterArray(vm.individualsFiltered, vm.individualSearch);
      vm.loadingFilters = false;
    }

    function doFilterClasses() {
      vm.loadingFilters = true;
      angular.copy(vm.classes, vm.classesFiltered);
      vm.classesFiltered = OntologyManagerService.filterArray(vm.classesFiltered, vm.classesSearch);
      vm.loadingFilters = false;
    }




    Principal.identity().then(function(account) {
      vm.currentAccount = account;
    });


    function getServiceToUser(){
      OntologyManagerService.getServiceToUser(vm.user.id).then(function (response) {
        vm.services = response.data;
        console.log(vm.services);
      });
    }

    function getServices(){
      OntologyManagerService.getServiceToUser(vm.user.id).then(function (response) {
        vm.services = response.data;
        //console.log(vm.services);
        $state.go('assign-manager');
      });

    }

    function loadServicesSelect(){
      OntologyManagerService.loadServicesSelect().then(function (response) {
        vm.servicesSelect = response.data;
        //console.log(vm.servicesSelect);
      });

    }

    function assignNewService(service){
      console.log("Asignando servicio");
      OntologyManagerService.assignServiceToUser().then(function () {
        User.get({
          login: $stateParams.login
        }, onSuccess, onError);
      });

      console.log(vm.user.id);
      console.log(service);

      OntologyManagerService.saveServiceToUser(vm.user, service).then(function () {
        console.log("service assigned");
      });

    }

    function setActive (user, isActivated) {
      user.activated = isActivated;
      User.update(user, function () {
        vm.loadAll();
        vm.clear();
      });
    }

    function loadAll () {
      console.log("LOAD ALL");
      var bar = $stateParams.login;
      User.get({
        login: bar
      }, onSuccess2, onError);


    }

    function onSuccess(data, headers) {
      vm.user = data;
    }

    function onSuccess2(data, headers) {
      vm.user = data;
      getServiceToUser();
    }

    function onError(error) {
      console.log("error");
      AlertService.error(error.data.message);
    }

    function clear () {
      vm.user = {
        id: null, login: null, firstName: null, lastName: null, email: null,
        activated: null, langKey: null, createdBy: null, createdDate: null,
        lastModifiedBy: null, lastModifiedDate: null, resetDate: null,
        resetKey: null, authorities: null
      };
    }

    function sort () {
      var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
      if (vm.predicate !== 'id') {
        result.push('id');
      }
      return result;
    }

    function loadPage (page) {
      vm.page = page;
      vm.transition();
    }

    function transition () {
      console.log("transition");
      console.log($state.$current);
      $state.transitionTo($state.$current, {
        page: vm.page,
        sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
        search: vm.currentSearch
      });
    }
  }
}


