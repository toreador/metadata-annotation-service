export class UserManagementDetailController {
  constructor($stateParams, User ) {
    'ngInject';//ParseLinks

    var vm = this;


        vm.load = load;
        vm.user = {};
        vm.load($stateParams.login);

        function load(login) {
            console.log("Cargando user...");
            User.get({login: login}, function(result) {
                vm.user = result;
            });
        }
    }
}
