export class ManagementController {
  constructor(ManagementManagerService, $stateParams, $scope,$cookies) {
    'ngInject';



    var vm = this;

    vm.showUser = showUser;
    vm.showFormNewUser = showFormNewUser;
    vm.createNewUser=createNewUser;
    vm.user=[];
    vm.isAdmin = false;
    vm.isNewUser=false;

    vm.showUser();

    function showUser() {
      ManagementManagerService.showUser().then(function (response) {
        console.log('Show User');
        console.log(response);
        angular.forEach(response.data.authorities, function(role){
          if(role === "ROLE_ADMIN"){
            vm.isAdmin = true;
          }
        })
        vm.user = response.data;



      })
      //vm.getClasses();
    }


    function showFormNewUser() {
      vm.isNewUser=true;
      //vm.getClasses();
    }

    function createNewUser(username,pass) {
      vm.isNewUser=true;
      console.log("Creating new user...");
      ManagementManagerService.createNewUser(username,pass).then(function (response) {
        console.log('create User');
        console.log(response);




      })
      //vm.getClasses();
    }


  }

}
