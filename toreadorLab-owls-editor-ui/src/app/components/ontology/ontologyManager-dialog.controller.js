export class OntologyManagerDialogController {
  constructor($timeout, $scope, $stateParams, $uibModalInstance, $q, OntologyManagerService, graph,$http) {
    'ngInject';

    console.log(graph);
    var vm = this;

    vm.readOnlyValue = 'http://www.w3.org/2002/07/owl#NamedIndividual';
    vm.individualObject = {};
    vm.individuals = {};
    vm.individualsFiltered = {};
    vm.loadingFilters = {};
    vm.propertyValues = [];
    //vm.declarativeNames=[];

    vm.clear = clear;
    vm.save = save;
    vm.loadPropertyValues = loadPropertyValues;
    vm.getElementPrintableLabel = getElementPrintableLabel;
    vm.addNewValueProperty = addNewValueProperty;
    vm.removeValueProperty = removeValueProperty;
    vm.isRequiredProperty = isRequiredProperty;

    vm.doFilterChoiceObjects = doFilterChoiceObjects;
    vm.toggleChoice = toggleChoice;
    vm.getElementIndex = getElementIndex;

    vm.assignNewService = assignNewService;
    //vm.printDeclarative=printDeclarative;
    //vm.readFileDeclarative=readFileDeclarative;
    ///

    vm.loadPropertyValues();
    //vm.readFileDeclarative();
    ///

    $timeout(function() {
      angular.element('.form-group:eq(1)>input').focus();
    });



    function assignNewService(service){
      console.log("Asignando nuevo servicio");
      save();

    }

    /*function readFileDeclarative(){
      var files=["analytics.json","display.json","preparation.json","processing.json","representation.json"];
      vm.declarativeNames=[];
      angular.forEach(files,function(file){
        $http.get('/app/components/ontology/'+file).success(function (data) {
          //console.log("lectura de fichero");
          printDeclarative(data);

        });
      });
      //console.log(vm.declarativeNames);
    }

    function printDeclarative(declarative){
      var results = [];
      var childs = [];
      var area = declarative['tdm:label'].replace(/\s/g, "") ;

      for (var i=0; i < declarative['tdm:incorporates'].length; i++) {
        var child = declarative['tdm:incorporates'][i];
        var name = declarative['tdm:incorporates'][i]['tdm:label'].replace(/\s/g, "");

        for(var j=0; j < child['tdm:incorporates'].length; j++){
          var child2 = child['tdm:incorporates'][j];
          var name2 = child['tdm:incorporates'][j]['tdm:label'].replace(/\s/g, "");

          for(var k=0; k < child2['tdm:incorporates'].length; k++){
            var child3 = child2['tdm:incorporates'][k];
            var name3 = ""

            if(child2['tdm:incorporates'][k]['tdm:label'] !== angular.isUndefined){
              name3 = child2['tdm:incorporates'][k]['tdm:label'].replace(/\s/g, "");
            }


            vm.declarativeNames.push(area + '.' +name + '.' + name2 + '.' + name3);
          }
        }
      }


    }*/

    function callCreateDefaultIndividual(data, newProperties, propertyObjects) {
      console.log('newProperties '+newProperties)
      OntologyManagerService.createDefaultIndividual(data, $stateParams.className, newProperties, propertyObjects).then(function(dataIndividual) {
        console.log('dataIndividual: '+dataIndividual);
        vm.propertyValues = dataIndividual;
        if (!!newProperties) {
          console.log("hay new properties!!!");
          addValuesObjects(newProperties);
        }
      }, function(error) {
        console.log('error '+error);
        vm.propertyValues = [];
      });
    }

    function callPropertyObjectsAndCreateDefaultIndividual(data, newProperties) {
      var propertyObjects = {};
      var promises = [];
      angular.forEach(newProperties, function(property) {
        //console.log("loaded property: "+property);
        var promise = OntologyManagerService.possibleObjects(property);
        promises.push(promise);
        propertyObjects[property] = [];
      });
      $q.all(promises).then(function(values) {
        angular.forEach(newProperties, function(property, index) {
          //console.log('property: '+property);
          //console.log('values: '+values[index].data);
          propertyObjects[property] = values[index].data;
        });
        callCreateDefaultIndividual(data.data, newProperties, propertyObjects);
      });
    }

    function callPossibleProperties(data) {
      console.log('ClassName: '+$stateParams.className);
      OntologyManagerService.possibleProperties($stateParams.className).then(function(dataNewProperties) {
        var newProperties = OntologyManagerService.getValueIdFromArray(dataNewProperties.data);
        callPropertyObjectsAndCreateDefaultIndividual(data, newProperties);
      }, function(data) {
        callCreateDefaultIndividual(data.data);
      });
    }

    function getPropertyIndex(property) {
      var propertyIndex = 0;
      for (var i = 0; i < vm.propertyValues.length; i++) {
        if (vm.propertyValues[i].property.id === property) {
          propertyIndex = i;
          break;
        }
      }
      return propertyIndex;
    }

    function addValuesObjects(newProperties) {
      angular.forEach(newProperties, function(property) {
        var propertyIndex = getPropertyIndex(property);
        vm.individualObject[property] = '';
        //console.log('property: '+property+' index: '+propertyIndex);
        doFilterChoiceObjects(property, propertyIndex);
      });
    }

    function loadPropertyValues() {
      var individualId = $stateParams.id;
      console.log("LOAD PROPERTIES: "+individualId);
      if (individualId) {
        OntologyManagerService.getProperties(individualId).then(function(data) {
          callPossibleProperties(data);
        }, function(data) {
          vm.propertyValues = [];
        });
      } else {
        callPossibleProperties([]);
      }
    }

    function getElementPrintableLabel(element) {
      return OntologyManagerService.getElementPrintableLabel(element);
    }

    function addNewValueProperty(propertyIndex) {
      var valueProperties = OntologyManagerService.getInitialValuePropertiesEmpty();
      vm.propertyValues[propertyIndex].values.push(valueProperties);
    }

    function removeValueProperty(propertyIndex, valueIndex) {
      if (vm.propertyValues[propertyIndex]) {
        vm.propertyValues[propertyIndex].values.splice(valueIndex, 1);
      }
    }

    function isRequiredProperty(property) {
      vm.requiredProperties = OntologyManagerService.requiredProperties();
      return vm.requiredProperties.indexOf(property) > -1;
    }

    function doFilterChoiceObjects(property, propertyIndex) {
      //console.log("DoFilterChoicheObject: "+property);
      vm.loadingFilters[property] = true;
      vm.individualsFiltered[property] = [];
      vm.individuals[property] = vm.propertyValues[propertyIndex].choicesObjects;
      angular.copy(vm.individuals[property], vm.individualsFiltered[property]);
      if (vm.individualObject[property] == '') {
        vm.individualsFiltered[property] = OntologyManagerService.filterArray(vm.individualsFiltered[property], vm.individualObject[property]);
      }
      //console.log("property: "+property+" "+vm.individualsFiltered[property]);
      vm.loadingFilters[property] = false;
    }

    function getElementIndex(choice, propertyIndex) {
      console.log("GETTING ELEMENT INDEX: "+choice);
      var valuesId = OntologyManagerService.getValueIdFromArray(vm.propertyValues[propertyIndex].values);
      var elementIndex = valuesId.indexOf(choice.id);
      return elementIndex;
    }

    function toggleChoice(choice, propertyIndex) {
      var elementIndex = getElementIndex(choice, propertyIndex);
      if (elementIndex === -1) {
        vm.propertyValues[propertyIndex].values.push(choice);
      } else {
        vm.propertyValues[propertyIndex].values.splice(elementIndex, 1);
      }
    }

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function preprocessPropertyValues() {
      angular.forEach(vm.propertyValues, function(propertyValues) {
        angular.forEach(propertyValues.values, function(value) {
          if (OntologyManagerService.validateURL(value.label)) {
            value.id = value.label;
            value.label = null;
          } else {
            if (value.id && !OntologyManagerService.validateURL(value.id)) {
              value.label = value.id;
              value.id = null;
            }
          }
        });
      });
    }

    function save() {
      vm.isSaving = true;
      console.clear();
      console.log("PROPERTY VALUES");
      //console.log(vm.propertyValues);
      preprocessPropertyValues();

      console.log("ontologymanagerdialog");



      if ($stateParams.id) {
        OntologyManagerService.updateIndividual(vm.propertyValues, $stateParams.id, $stateParams.graph).then(function(data) {
          onSaveSuccess(data);
        }, function(data) {
          onSaveError();
        });
      } else {
        OntologyManagerService.addIndividual(vm.propertyValues).then(function(data) {
          onSaveSuccess(data);
        }, function(data) {
          onSaveError();
        });
      }
      /*console.log("guardandoo...");
      console.log($stateParams.idUser);
      console.log($stateParams.id);
      console.log($stateParams.graph);
      console.log($stateParams.className);
      */


      console.log("guardandoo...");
    }

    function onSaveSuccess(result) {
      console.log("Este es mi stateparams.id");
      console.log( $stateParams.id);
      OntologyManagerService.saveServiceToUser($stateParams.idUser, $stateParams.id,$stateParams.graph,$stateParams.className);
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

  }
}
