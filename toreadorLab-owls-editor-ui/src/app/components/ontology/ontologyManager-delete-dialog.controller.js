export class OntologyManagerDeleteDialogController {
  constructor($stateParams, $uibModalInstance, OntologyManagerService, graph) {
    'ngInject';
    console.log("GRAPHHHH "+graph);
    var vm = this;
    vm.message = {};
    vm.individual = {
      id: $stateParams.id,
      graph: $stateParams.graph
    };

    vm.clear = clear;
    vm.confirmDelete = confirmDelete;

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function confirmDelete(id, context) {
      console.log('CONTEXXXXXT ' + context)
      OntologyManagerService.deleteIndividual(id, context).then(function(data) {
        $uibModalInstance.close(true);
      }, function(data) {});
    }
  }
}
