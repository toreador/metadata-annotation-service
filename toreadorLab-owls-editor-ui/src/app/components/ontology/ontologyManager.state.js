import {AssignationDetailController} from "../assignation/assignation-detail.controller";

export function ConfigMainState($stateProvider) {
  'ngInject';


  $stateProvider
  // .state('home', {
  //   parent: 'app',
  //   url: '/',
  //   views: {
  //     'content@': {
  //       templateUrl: '/app/components/ontology/main.html',
  //       controller: 'MainController',
  //       controllerAs: 'vm'
  //     }
  //   }
  // });

  .state('home', {
    parent: 'app',
    url: '/',
    views: {
      'content@': {
        templateUrl: 'app/components/home/home.html',
        controller: 'HomeController',
        controllerAs: 'vm'
      }
    }
  })
    .state('management-manager', {
      parent: 'app',
      url: '/management',
      views: {
        'content@': {
          templateUrl: 'app/components/management/management.html',
          controller: 'ManagementController',
          controllerAs: 'vm'
        }
      }
    })
    /*.state('assign-manager', {
      parent: 'app',
      url: '/assign',
      views: {
        'content@': {
          templateUrl: 'app/components/assignation/assignation.html',
          controller: 'AssignationController',
          controllerAs: 'vm'
        }
      }
    })*/
    .state('assign-manager', {
      parent: 'app',
      url: '/assign?page&sort',
      data: {
        pageTitle: 'Users'
      },
      views: {
        'content@': {
          templateUrl: 'app/components/assignation/assignation.html',
          controller: 'AssignationController',
          controllerAs: 'vm'
        }
      },params: {
        page: {
          value: '1',
          squash: true
        },
        sort: {
          value: 'id,asc',
          squash: true
        }
      },resolve: {
        pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
          console.log($stateParams);
          return {
            page: PaginationUtil.parsePage($stateParams.page),
            sort: $stateParams.sort,
            predicate: PaginationUtil.parsePredicate($stateParams.sort),
            ascending: PaginationUtil.parseAscending($stateParams.sort)
          };
        }]
      }
    })
    .state('assign-manager.detail', {
      parent: 'assign-manager',
      url: '/:login/:idUser',
      data: {
        authorities: ['ROLE_ADMIN'],
        pageTitle: 'AssignationDetail'
      },
      views: {
        'content@': {
          templateUrl: 'app/components/assignation/assignationDetail.html',
          controller: 'AssignationDetailController',
          controllerAs: 'vm'
        }
      }
    })
  .state('ontology-manager', {
    parent: 'app',
    url: '/ontology',
    data:{
      authorities:['ROLE_ADMIN','ROLE_USER']
    },
    views: {
      'content@': {
        templateUrl: 'app/components/ontology/ontologyManager.html',
        controller: 'OntologyManagerController',
        controllerAs: 'vm'
      }
    },params: {
      page: {
        value: '1',
        squash: true
      },
      sort: {
        value: 'id,asc',
        squash: true
      }
    },resolve: {
      pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
        console.log($stateParams);
        return {
          page: PaginationUtil.parsePage($stateParams.page),
          sort: $stateParams.sort,
          predicate: PaginationUtil.parsePredicate($stateParams.sort),
          ascending: PaginationUtil.parseAscending($stateParams.sort)
        };
      }]
    }
  })
  .state('ontology-manager.new', {
    parent: 'ontology-manager',
    url: 'new/:className',
    onEnter: [
      '$stateParams',
      '$state',
      '$uibModal',
      function($stateParams, $state, $uibModal) {
        $uibModal.open({
          templateUrl: 'app/components/ontology/ontologyManager-dialog.html',
          controller: 'OntologyManagerDialogController',
          controllerAs: 'vm',
          backdrop: 'static',
          size: 'lg',
          resolve: {
                graph: function() {
                  console.log('LALALALALA 2 2 2 http://www.toreador-project.eu/BDMOntologies/')
                    return 'http://www.toreador-project.eu/BDMOntologies/';
                }
            }
        }).result.then(function() {
          $state.go('ontology-manager', {
            message: ' Success! New Individual created.',
            className: $stateParams.className,
            graph: 'http://www.toreador-project.eu/BDMOntologies/'
          }, {reload: true});
        }, function() {
          $state.go('ontology-manager');
        });
      }
    ]
  })
    .state('register', {
      parent: 'app',
      url: '/register',
      data: {
        authorities: [],
        pageTitle: 'Registration'
      },
      views: {
        'content@': {
          templateUrl: 'app/components/register/register.html',
          controller: 'RegisterController',
          controllerAs: 'vm'
        }
      }
    })
  .state('assign-manager.edit', {
    //parent: 'assign-manager.detail',
    url: 'edit/:className/:id/:graph',
    onEnter: [
      '$stateParams',
      '$state',
      '$uibModal',
      function($stateParams, $state, $uibModal) {
        $uibModal.open({
          templateUrl: 'app/components/ontology/ontologyManager-dialog.html',
          controller: 'OntologyManagerDialogController',
          controllerAs: 'vm',
          backdrop: 'static',
          size: 'lg',
          resolve: {
                graph: function() {
                  //console.log('LALALALALA '+$stateParams.graph)
                    return $stateParams.graph;
                }
            }
        }).result.then(function() {
          $state.go('assign-manager', {
            message: ' Success! Individual updated.',
            className: $stateParams.className,
            graph: $stateParams.graph
          }, {reload: true});
        }, function() {
          $state.go('assign-manager');
        });
      }
    ]
  })
    .state('ontology-manager.edit', {
      parent: 'ontology-manager',
      url: 'edit/:className/:id/:graph' +
      '',
      onEnter: [
        '$stateParams',
        '$state',
        '$uibModal',
        function($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'app/components/ontology/ontologyManager-dialog.html',
            controller: 'OntologyManagerDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              graph: function() {
                console.log('LALALALALA '+$stateParams.graph)
                return $stateParams.graph;
              }
            }
          }).result.then(function() {
            /*$state.go('assign-manager', {
              message: ' Success! Individual updated.',
              className: $stateParams.className,
              graph: $stateParams.graph
            }, {reload: true});
          }, function() {
            $state.go('^');
            */
          });
        }
      ]
    })
  .state('ontology-manager.delete', {
    parent: 'ontology-manager',
    url: 'delete/:className/:id/:graph',
    onEnter: [
      '$stateParams',
      '$state',
      '$uibModal',
      function($stateParams, $state, $uibModal) {
        $uibModal.open({
          templateUrl: 'app/components/ontology/ontologyManager-delete-dialog.html',
          controller: 'OntologyManagerDeleteDialogController',
          controllerAs: 'vm',
          size: 'md',
          resolve: {
                graph: function() {
                  console.log('LALALALALA '+$stateParams.graph)
                    return $stateParams.graph;
                }
            }
      }).result.then(function() {
          $state.go('ontology-manager', {
            message: ' Success! Individual deleted.',
            className: $stateParams.className,
            graph: $stateParams.graph
          }, {reload: true});
        }, function() {
          $state.go('^');
        });
      }
    ]
  });
}
