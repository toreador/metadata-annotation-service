# toreadorLab-owls-editor
OWL-S Editor for Toreador LAB

Interface and backend services for the Toreador Lab OWL-S ontology editor.
Requires Java 8, MySQL, Apache Tomcat 8 and Blazegraph up and running (see configuration file application.yml for settings)


# Mandatory catalog configuration

Add this information in the next configuration files

	/toreador-lab-owls-editor-services/src/test/resources/config/application-dev.yml
	/toreador-lab-owls-editor-services/src/main/resources/config/application-dev.yml
	/toreador-lab-owls-editor-services/src/main/resources/config/application-prod.yml
	
	resources: (i.e. /Users/metadata-annotation-service/toreadorLab-owls-editor-resources
	resourcesOntology: (i.e /home/ec2-user/apache-tomcat-8.5.20/webapps/ROOT/BDMOntologies/)
	
	serviceCatalogueEndpoint: (i.e. https://toreadorplatform.eng.it/ServiceCatalogue/api/catalogues)
	serviceCatalogueUsername: (user name of the catalog)
	serviceCataloguePassword: (psssword)
	
	url: jdbc:mysql://localhost:3306/toreadorLabOwlsEditorServices?useUnicode=true&characterEncoding=utf8&useSSL=false
    username: (user name of database)
    password: (pass)


# Installation:
1. Start Blazegraph
2. Create in blazegraph a new namespace called toreadorLab of type quads (not triples) leave the rest as it is. Click create and confirm. Select the name space (click "use" next to it once created)
3. In the directory toreadorLab-owls-editor-ui, the first time you build the project you must first install the bower and gulp dependencies:
4. Start MySQL server and create a database called "toreadorlabowlseditorservices" con UTF8 de charset.
5. Change in the (application-dev.yml, application-prod.yml) the username and password for the database connection.
6. in case you application will run in different port than 8018 you need to edit the file toreadorLab-owls-editor-ui/src/app/services/ontologyManager.service.js with the new port where you have this code: "    var localTesting = 'http://localhost:8080/toreadorLab-owls-editor-service/api';"

7. Local deployment:
 
 Compile:
	
	$ cd /metadata-annotation-service/
	
	Default maven profile is dev:
	$ mvn clean install
	
	Production profile -Pprod
	$ mvn clean install -Pprod
	
 Launch ui:

	cd /metadata-annotation-service/toreadorLab-owls-editor-ui
	
	bower install
	npm install
	Once the dependencies are installed you can build the project with "gulp build"
	Finally "gulp serve"
	
 Launch services:
 
	cd /metadata-annotation-service/toreadorLab-owls-editor-services
	mvn spring-boot:run

8. Tomcat deployment

- In case of problems with npm, bower, etc. ask some from the frontend team. Since the problems that may occur are very variate is better to refer to someaone from frontend in case some errors appear.

- Once compiled copy the content of the dist directory in the webapps/ROOT/ontology-ui directory of the apache tomcat or create a symlink
- Move to the toreadorLab-owls-editor directory and execute the command "mvn clean compile install"; if you have problems with the tests check if blazegraph are correctly started and namespace created.
- Copy the target/toreador-lab-owls-editor-services-0.0.1-SNAPSHOT.war.original under webapps/toreadorLab-owls-editor-service.war (you need to put the name of the war exactly as reported here)
- Start Tomcat and go to http://localhost:8080/ontology-ui for the ontology editor or to http://localhost:8080/toreadorLab-owls-editor-service for the services.
